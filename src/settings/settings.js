/** @module Settings */
import Localization from '../localization/localization';
import Setting from '../datamodels/setting';
import SettingHeader from '../datamodels/settingsheader';
import SettingSection from '../datamodels/settingsection';
import WatchSportMode from '../datamodels/watchsportmode/mode';
import WatchSportModeGroup from '../datamodels/watchsportmode/group';
import WatchSportModeSettings from '../datamodels/watchsportmode/settings';
import SettingsDefaults from '../../suunto-information-model/Specifications/CustomModes/CustomModeSettingsDefaults.json';
import Packages from '../packages/packages';

const maxNameLength = 32;

/**
 * Returns settings sections for variant
 * @param {WatchSportModeSettings} watchSportModeSettings
 * @param {String} variant
 * @param {String} version
 * @param {String} language
 * @return {SettingsSection[]}
 */
function sections(watchSportModeSettings, variant, version, language) {
  const activityID = watchSportModeSettings.getActivityID();

  const validSetting = (setting, activity) => {
    // Following setting are not yet hide in SIM
    const hiddenSettingsCommon = ['FusedLocationEnabled'];
    const hiddenSettingsVariant = {
      Helsinki: ['UsePowerPOD', 'FusedAltitudeEnabled', 'AltiBaroMode', 'TouchEnabled', 'DisplayLowColor', 'GPSInterval'],
      HelsinkiC: ['UsePowerPOD', 'FusedAltitudeEnabled', 'AltiBaroMode', 'TouchEnabled', 'DisplayLowColor', 'GPSInterval'],
    };
    if (hiddenSettingsCommon.includes(setting) ||
        (variant in SettingsDefaults.HiddenSettingsByDevice &&
         SettingsDefaults.HiddenSettingsByDevice[variant].includes(setting)) ||
        (variant in hiddenSettingsVariant && hiddenSettingsVariant[variant].includes(setting))) {
      return false;
    }
    // Filter settings based on activity ID - not all setting are applicable in
    // all activity types
    const activitySpecific = SettingsDefaults.ActivitySpecificSettings[activity];
    if (!activitySpecific) {
      return true;
    }
    return !activitySpecific.Hidden.includes(setting);
  };

  const filterFields = (id, obj) => {
    const reduced = {
      ID: id,
      Name: obj.Name,
      Description: obj.Description,
      Inputs: obj.Inputs,
      Triggers: obj.Triggers,
    };
    if (obj.DefaultValue) {
      Object.assign(reduced, { DefaultValue: obj.DefaultValue });
    }
    if (obj.Max) {
      Object.assign(reduced, { Max: obj.Max });
    }
    if (obj.Min) {
      Object.assign(reduced, { Min: obj.Min });
    }
    return reduced;
  };

  const settings = Object.keys(SettingsDefaults.DefaultSettings)
    .map(setting => filterFields(setting, SettingsDefaults.DefaultSettings[setting]))
    .filter((setting) => {
      const compatibility = SettingsDefaults.DefaultSettings[setting.ID].Compatibility;
      if (compatibility) {
        return Packages.versionCompare(version, compatibility[variant]);
      }
      return true;
    })
    .filter(setting => !SettingsDefaults.HiddenSettings.includes(setting.ID))
    .filter(setting => validSetting(setting.ID, activityID))
    .map((setting) => {
      // Translate phrase IDs
      const copy = JSON.parse(JSON.stringify(setting));
      if (copy.Name) {
        copy.Name = Localization.translate(copy.Name, language);
      }
      if (copy.Description) {
        copy.Description = Localization.translate(copy.Description, language);
      }
      return copy;
    })
    .map((setting) => {
      let settingIds;

      if (setting.Inputs && typeof setting.Inputs[0] === 'object') {
        // Single
        settingIds = [setting.ID];
      } else if (setting.Triggers) {
        // Triggers
        settingIds = [setting.ID];
      } else {
        // Multi
        settingIds = setting.Inputs;
      }
      return new SettingSection(
        setting.ID, setting.Name, setting.Description,
        settingIds.filter(id => validSetting(id, activityID))
      );
    })
    .filter(setting => setting.settings.length > 0);

  return settings;
}

/**
 * Returns setting from sportmode
 * @param {WatchSportModeGroup} watchSportModeGroup
 * @param {WatchSportModeSettings} watchSportModeSettings
 * @param {String} language
 * @param {String} variant i.e. 'Amsterdam'
 * @param {String} settingId i.e. 'Name'
 * @return {Setting}
 */
function getSetting(watchSportModeGroup, watchSportModeSettings, language, variant, settingId) {
  let ret = {};

  const getMeta = (id) => {
    const setting = SettingsDefaults.DefaultSettings[id];
    const reduced = {
      type: setting.InputType,
      inputs: setting.Inputs.map((item) => {
        const obj = {
          label: item.Label,
          labelDescription: item.LabelDescription ?
            Localization.translate(item.LabelDescription, language) :
            null,
          value: item.Value,
        };
        if (obj.label) {
          // Some labels (GPSInterval at the time of writing) have placeholder
          // for device variant. This mapping doesn't exist in SIM
          if (obj.label.includes('{0}')) {
            const variantMap = {
              Amsterdam: 'SPARTAN_ULTRA',
              Brighton: 'SPARTAN_SPORT',
              Cairo: 'SPARTAN_SPORT_OHR',
              Forssa: 'SPARTAN_TRAINER_OHR',
              // Helsinki does not have specific translations
              // for now
              Gdansk: 'SPARTAN_SPORT_WHR_BARO',
              Ibiza: 'SUUNTO_9',
            };

            const getRegionlessVariant = v => (
              v.charAt(v.length - 1) === 'C' ?
                v.substring(0, v.length - 1) :
                v
            );

            const regionlessVariant = getRegionlessVariant(variant);
            const fullVariantName = variantMap[regionlessVariant] ?
              variantMap[regionlessVariant] :
              'UNKNOWN';
            obj.label = obj.label.replace('{0}', fullVariantName);
          }
          obj.label = Localization.translate(obj.label, language);
        }
        return obj;
      }),
    };
    if (setting.Min) {
      Object.assign(reduced, { min: setting.Min });
    }
    if (setting.Max) {
      Object.assign(reduced, { max: setting.Max });
    }
    return reduced;
  };

  const getTriggerMeta = (id) => {
    const setting = SettingsDefaults.DefaultSettings[id];
    // Triggers in SIM don't have explicit 'Off' setting
    // but we need it for app
    const values = [{
      label: Localization.translate('TXT_OFF', language),
      value: 0,
    }];

    for (let i = 0; i < setting.Triggers.length; i++) {
      const triggerType = setting.Triggers[i];
      const triggerInfo = SettingsDefaults.Triggers.Types[triggerType];

      values.push({
        label: Localization.translate(triggerInfo.Name, language),
        value: i + 1,
        subValue: {
          type: triggerInfo.InputType,
          min: triggerInfo.Min,
          max: triggerInfo.Max,
          default: triggerInfo.Inputs && triggerInfo.Inputs[0] ?
            triggerInfo.Inputs[0].DefaultValue :
            null,
        },
      });
    }

    return {
      type: setting.InputType,
      inputs: values,
    };
  };

  switch (settingId) {
    case 'Name': {
      const meta = getMeta(settingId);
      // Append field limits to meta, those should be in SIM but isn't for now
      Object.assign(meta.inputs[0], { length: { min: 1, max: maxNameLength } });
      ret = new Setting(
        new SettingHeader(settingId, null),
        meta,
        watchSportModeGroup.name ? watchSportModeGroup.name : null,
        false
      );
      break;
    }
    default: {
      const simObj = SettingsDefaults.DefaultSettings[settingId];
      const name = simObj.Name;
      if (simObj.IsTrigger) {
        // Triggers are more complex than other types, they might
        // have also subvalue (i.e. AutoLap which has main "value"
        // 'Off'/'Distance'/'Duration' but also the either distance
        // or duration value.

        // Find which triggers in watch match to this setting
        const matchingTriggers = watchSportModeSettings.triggers()
          .filter(trigger => simObj.Triggers.includes(trigger.Type));

        const meta = getTriggerMeta(settingId);
        let valueObj;

        if (matchingTriggers.length) {
          const triggerInfo = SettingsDefaults.Triggers.Types[matchingTriggers[0].Type];
          let number = -1;

          for (let i = 0; i < simObj.Triggers.length; i++) {
            if (simObj.Triggers[i] === triggerInfo.DefaultValues.Type) {
              number = i + 1;
            }
          }

          valueObj = {
            value: number,
          };

          if (SettingsDefaults.Triggers.Types[matchingTriggers[0].Type].InputType !== 'None') {
            Object.assign(valueObj, {
              subvalue: {
                metric: matchingTriggers[0].LimitMetric,
                imperial: matchingTriggers[0].LimitImperial,
              },
            });
          }
        } else {
          valueObj = {
            value: 0,
          };
        }

        ret = new Setting(
          new SettingHeader(
            settingId,
            name ? Localization.translate(name, language) : null
          ),
          meta,
          valueObj,
          SettingsDefaults.OnlyShowSettings.includes(settingId)
        );
      } else {
        // Primitive types are straightforward
        ret = new Setting(
          new SettingHeader(
            settingId,
            name ? Localization.translate(name, language) : null
          ),
          getMeta(settingId),
          watchSportModeSettings.settings()[settingId],
          SettingsDefaults.OnlyShowSettings.includes(settingId)
        );
      }
      break;
    }
  }
  return ret;
}

/**
 * Changes setting from sportmode
 * @param {String} watchSportModeGroup stringified
 * @param {String} watchSportModeSettings stringified
 * @param {String} settingsId i.e. 'Name'
 * @param {String/Integer/Boolean} setting value of which type depends on setting
 * @return {WatchSportMode}
 */
function changeSetting(watchSportModeGroup, watchSportModeSettings, settingId, value) {
  const grp = new WatchSportModeGroup(watchSportModeGroup);
  const set = new WatchSportModeSettings(watchSportModeSettings);
  switch (settingId) {
    case 'Name': {
      const getLengthInBytes = input => encodeURI(input).split(/%..|./).length - 1;
      if (getLengthInBytes(value) <= maxNameLength) {
        grp.name = value;
      }
      break;
    }
    default: {
      const simObj = SettingsDefaults.DefaultSettings[settingId];
      if (simObj.IsTrigger) {
        const valueObj = JSON.parse(value);

        // Remove related triggers
        const simTriggers = simObj.Triggers;
        set.Settings.CustomModes[0].Triggers = set.triggers()
          .filter(item => !simTriggers.includes(item.Type));

        if (valueObj.value > 0) {
          // Add or modify trigger
          const multiplier = 1.609344;
          const { subvalue } = valueObj;
          let { metric } = subvalue || { metric: null };
          let { imperial } = subvalue || { imperial: null };

          if (metric && !imperial) {
            imperial = metric * multiplier;
          } else if (imperial && !metric) {
            metric = imperial / multiplier;
          }

          const triggerType = simObj.Triggers[valueObj.value - 1];
          const triggerInfo = JSON.parse(JSON.stringify(SettingsDefaults
            .Triggers.Types[triggerType].DefaultValues));

          triggerInfo.LimitMetric = metric;
          triggerInfo.LimitImperial = imperial;

          if (!triggerInfo.ActionsOnFall) {
            triggerInfo.ActionsOnFall = null;
          }

          set.Settings.CustomModes[0].Triggers.push(triggerInfo);
        }
      } else {
        set.settings()[settingId] = value;
      }
      break;
    }
  }
  return new WatchSportMode(
    grp,
    null,
    WatchSportMode.createModes(
      [null],
      [set],
      [null]
    )
  );
}

export default { sections, getSetting, changeSetting };
