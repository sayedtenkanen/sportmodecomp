export const TOP = 'Top';
export const TOP_SECOND = 'Top2nd';
export const TOP_THIRD = 'Top3rd';
export const TOP_MINI = 'TopMini';

export const CENTER = 'Center';
export const CENTER_SECOND = 'Center2nd';
export const CENTER_THIRD = 'Center3rd';

export const BOTTOM = 'Bottom';

export const CHART = 'Chart';

export const CAR_FIRST = 'Car1st';
export const CAR_SECOND = 'Car2nd';
export const CAR_THIRD = 'Car3rd';
export const CAR_FOURTH = 'Car4th';
export const CAR_FIFTH = 'Car5th';

export const LEFT_COLUMN = 'LeftColumn';
export const CENTER_COLUMN = 'CenterColumn';
export const RIGHT_COLUMN = 'RightColumn';
