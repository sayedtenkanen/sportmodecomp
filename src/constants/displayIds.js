export const DATA_3_FIELDS = 'Data3Fields';
export const DATA_4_FIELDS = 'Data4Fields';
export const DATA_5_FIELDS = 'Data5Fields';
export const DATA_7_FIELDS = 'Data7Fields';
export const DATA_3_FIELDS_WEAR_ROUND = 'Data3FieldsWearRound';
export const DATA_4_FIELDS_WEAR_ROUND = 'Data4FieldsWearRound';
export const DATA_5_FIELDS_WEAR_ROUND = 'Data5FieldsWearRound';
export const DATA_7_FIELDS_WEAR_ROUND = 'Data7FieldsWearRound';
export const INTERVAL_37 = 'Interval37';
export const SIMPLE_LINE_CHART = 'SimpleLineChart';
export const SIMPLE_LINE_CHART_WEAR_ROUND = 'SimpleLineChartWearRound';
export const ZONES_8_FIELDS = 'Zones8Fields';
export const LAP_TABLE = 'LapTable';
export const LAP_TABLE_WEAR_ROUND = 'LapTableWearRound';
export const DATA_3_CAROUSEL = 'Data3Carousel';

// Hidden displays
export const MAP_WEAR_ROUND = 'MapWearRound';
export const DISTANCE_AUTO_LAP = 'DistanceAutoLap';
export const DURATION_AUTO_LAP = 'DurationAutoLap';
export const MANUAL_LAP = 'ManualLap';
