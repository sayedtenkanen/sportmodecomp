/** @module SettingHeader */
export default class SettingHeader {
  /**
   * @param {String} id Identification value.
   * @param {String} name Localized UI name.
  */
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}
