/** @module Setting */
export default class Setting {
  /**
   * @param {SettingHeader} header
   * @param {Object} meta
   * @param {String} value Current value of the setting, return values are
   * according to 'meta'
   * @param {Boolean} readOnly true if read-only
  */
  constructor(header, meta, value, readOnly) {
    this.header = header;
    this.meta = meta;
    this.value = value;
    this.readOnly = readOnly;
  }
  /**
   * Identification value.
   * @return {Number} id
  */
  get id() {
    return this.header.id;
  }
  /**
   * Localized UI name.
   * @return {String} name
  */
  get name() {
    return this.header.name;
  }
}
