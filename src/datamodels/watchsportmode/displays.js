import * as Fields from '../../constants/fieldIds';

export default class WatchSportModeDisplays {
  constructor(json) {
    if (json !== undefined) {
      Object.assign(this, JSON.parse(json));
    }
  }
  initFromSim(displays) {
    // Make a copy from displays data and remove unnecessary fields
    const reducedDisplays = JSON.parse(JSON.stringify(displays))
      .map(display => WatchSportModeDisplays.filterDisplay(display));
    // Assign data
    const sportModeDisplays = {
      Settings: {
        CustomModes: [
          {
            Displays: reducedDisplays,
          },
        ],
      },
    };
    Object.assign(this, sportModeDisplays);
  }
  templates() {
    return this.displays().map(display => display.TemplateType);
  }
  displays() {
    return this.Settings.CustomModes[0].Displays;
  }
  static filterDisplay(display) {
    const reducedDisplay = JSON.parse(JSON.stringify(display));
    delete reducedDisplay.ShowTimeout;
    delete reducedDisplay.Name;
    reducedDisplay.Fields.forEach((field) => {
      const reducedField = field;
      if (field.ID !== Fields.CHART) {
        delete reducedField.ValueScaleMin;
        delete reducedField.ValueScaleMax;
        delete reducedField.ValueScaleImperialMin;
        delete reducedField.ValueScaleImperialMax;
        delete reducedField.SecondDimensionScaleUnit;
        delete reducedField.SecondDimensionScale;
        delete reducedField.SecondDimensionScaleToggle;
      }
      Object.keys(reducedField).forEach(key =>
        ((reducedField[key] == null) && delete reducedField[key]));
    });
    Object.keys(reducedDisplay).forEach(key =>
      ((reducedDisplay[key] == null) && delete reducedDisplay[key]));
    return reducedDisplay;
  }
}
