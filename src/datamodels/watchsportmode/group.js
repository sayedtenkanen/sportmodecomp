export default class WatchSportModeGroup {
  constructor(json) {
    if (json !== undefined) {
      Object.assign(this, JSON.parse(json));
    }
  }
  createNew(activityId) {
    const sportModeGroup = {
      Settings: {
        CustomModeGroups: [
          {
            ActivityID: activityId,
            AutoTransition: false,
            CustomModeIDs: [0],
            CustomModePackageID: 0,
            CustomModePackageVersion: 0,
            Name: '',
            MoveType: 0,
            TransitionPhase: false,
          },
        ],
      },
    };
    Object.assign(this, sportModeGroup);
  }
  get id() {
    return this.Settings.CustomModeGroups[0].ActivityID;
  }
  get name() {
    return this.Settings.CustomModeGroups[0].Name;
  }
  set name(name) {
    this.Settings.CustomModeGroups[0].Name = name;
  }
}
