export default class WatchSportModeSettings {
  constructor(json) {
    if (json !== undefined) {
      Object.assign(this, JSON.parse(json));
    }
  }
  initFromSim(settings, activityID) {
    // Make a copy from settings data
    const reducedSettings = JSON.parse(JSON.stringify(settings));
    reducedSettings.ActivityID = activityID;

    // Remove display fields from settings
    delete reducedSettings.Displays;

    Object.keys(reducedSettings).forEach((key) => {
      if (reducedSettings[key] == null) {
        delete reducedSettings[key];
      }
    });
    delete reducedSettings.AutoStart;
    delete reducedSettings.AutoScrolling;
    delete reducedSettings.CustomModeID;
    delete reducedSettings.TagSelectionEnabled;
    delete reducedSettings.TargetAltitudeMetric;
    delete reducedSettings.TargetAltitudeImperial;
    delete reducedSettings.TargetAscentMetric;
    delete reducedSettings.TargetAscentImperial;
    delete reducedSettings.TargetDistanceMetric;
    delete reducedSettings.TargetDistanceImperial;
    delete reducedSettings.TargetSpeedMetric;
    delete reducedSettings.TargetSpeedImperial;
    delete reducedSettings.TargetDuration;
    delete reducedSettings.TargetPower;
    delete reducedSettings.TargetCalories;
    delete reducedSettings.WorkoutEnabled;

    delete reducedSettings.__type;

    const sportModeSettings = {
      Settings: {
        CustomModes: [
          reducedSettings,
        ],
      },
    };
    Object.assign(this, sportModeSettings);
  }
  setHasIntervalDisplay(value) {
    this.Settings.CustomModes[0].HasIntervalDisplay = value;
  }
  getHasIntervalDisplay() {
    return this.Settings.CustomModes[0].HasIntervalDisplay;
  }
  setGpsInterval(value) {
    this.Settings.CustomModes[0].GPSInterval = value;
  }
  getGpsInterval() {
    return this.Settings.CustomModes[0].GPSInterval;
  }
  getActivityID() {
    return this.Settings.CustomModes[0].ActivityID;
  }
  settings() {
    return this.Settings.CustomModes[0];
  }
  triggers() {
    return this.Settings.CustomModes[0].Triggers;
  }
}
