export default class WatchSportMode {
  /**
   * @param {WatchSportModeGroup} group
   * @param {Number} groupId
   * @param {Object[]} modes in array of objects in format
   * {
   *     id: {Number}
   *     settings: {WatchSportModeSettings},
   *     displays: {WatchSportModeDisplays}
   * }
  */
  constructor(group, groupId, modes) {
    this.group = {
      id: groupId,
      data: group,
    };
    this.modes = modes;
  }
  /**
   * @param {Number[]} ids
   * @param {WatchSportModeSettings[]} settings
   * @param {WatchSportModeDisplays[]} displays
   * @return {Object[]} modes in array of objects in format
   * {
   *     id: {Number}
   *     settings: {WatchSportModeSettings},
   *     displays: {WatchSportModeDisplays}
   * }
  */
  static createModes(ids, settings, displays) {
    const modes = [];
    ids.forEach((modeId, index) =>
      modes.push({
        id: modeId,
        settings: settings[index],
        displays: displays[index],
      }));
    return modes;
  }
}
