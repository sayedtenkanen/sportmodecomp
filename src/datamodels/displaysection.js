/** @module DisplaySection */
export default class DisplaySection {
  /**
   * @param {String} id Identification value.
   * @param {String} name Localized UI name.
   * @param {String} description Localized description. Can be null.
   * @param {Display[]} displays
  */
  constructor(id, name, description, displays) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.displays = displays;
  }
}
