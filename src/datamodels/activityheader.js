/** @module ActivityHeader */
export default class ActivityHeader {
  /**
   * @param {Number} id Activity ID as in SIM, i.e. Running is 3
   * @param {String} type Localized UI type, i.e. 'Basic', 'Indoor' or 'Intervall' can
   * be null if not applicable as in getSportModes
  */
  constructor(id, type) {
    this.id = id;
    this.type = type;
  }
}
