/** @module Field */
export default class Field {
  /**
   * @param {String} id Identification value.
   * @param {String} name Localized UI name.
  */
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}
