/** @module SportModeHeader */
export default class SportModeHeader {
  /**
   * @param {Number} id Device specific ID for sport mode. Also
   * known as "GroupId".
   * @param {Number[]} modeIds Array of ModeId.
   * @param {String} name User inputted name for custom mode. Null
   * for factory modes.
   * @param {Boolean} factorymode true if factory mode, false for custom modes.
   * @param {ActivityHeader} activity ActivityHeader object
  */
  constructor(id, modeIds, name, factorymode, activity) {
    this.id = id;
    this.modeIds = modeIds;
    this.name = name;
    this.factorymode = factorymode;
    this.activity = activity;
  }
}
