/** @module Display */
export default class Display {
  /**
   * @param {String} id Identification value.
   * @param {String} name Localized UI name.
   * @param {Boolean} readOnly true if fields are read-only
   * @param {String} iconValues path to icon with real example values
   * @param {String} iconPlaceholder path to icon with placeholder values. Can be null
   * @param {Boolean} deletable true if can be deleted
   * @param {Boolean} replaceable true if can be replaced with another display
  */
  constructor(id, name, readOnly, iconValues, iconPlaceholder, deletable, replaceable) {
    this.id = id;
    this.name = name;
    this.readOnly = readOnly;
    this.icon = {
      values: iconValues,
      placeholder: iconPlaceholder,
    };
    this.deletable = deletable;
    this.replaceable = replaceable;
  }
}
