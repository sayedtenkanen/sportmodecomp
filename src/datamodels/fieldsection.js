/** @module FieldSection */
export default class FieldSection {
  /**
   * @param {String} id Identification value.
   * @param {String} name Localized UI name.
   * @param {Field[]} fields
  */
  constructor(id, name, fields) {
    this.id = id;
    this.name = name;
    this.fields = fields;
  }
}
