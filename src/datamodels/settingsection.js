/** @module SettingsSection */
export default class SettingSection {
  /**
   * @param {String} id Identification value
   * @param {String} title Localized UI title. Can be null (i.e. for Name)
   * @param {String} description Localized UI description. Can be null
   * @param {String[]} settings Array of setting IDs
  */
  constructor(id, title, description, settings) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.settings = settings;
  }
}
