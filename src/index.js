/** @module Component */
import './polyfill-config';
import SportModes from './sportmodes/sportmodes';
import Field from './datamodels/field';
import Display from './datamodels/display';
import WatchSportModeGroup from './datamodels/watchsportmode/group';
import WatchSportModeSettings from './datamodels/watchsportmode/settings';
import WatchSportModeDisplays from './datamodels/watchsportmode/displays';
import WatchSportMode from './datamodels/watchsportmode/mode';
import Settings from './settings/settings';
import Packages from './packages/packages';
import PackageJson from '../package.json';
import DisplayIconMap from './displayiconmap';
import Localization from './localization/localization';
import SimInfo from '../sim-tools/sim-info/sim_info.json';
import * as Displays from './constants/displayIds';
import * as Fields from './constants/fieldIds';

// To keep the entry point for users to be only Component object,
// we wont use 'static' functions since they need to be called
// differently than class methods. Ignore warning for unused 'this'
// for these functions.

// Two APIs are offered. One using a class and another with a wrapper
// on top of with plain function.

/* eslint class-methods-use-this: ["error", { "exceptMethods": [
   "getVersion",
   "getSimHash",
   "getSupportedLanguages",
   "deleteDisplay",
   "changeSetting",
   "getSupportedExtensions",
   "isListFull"] }] */

// class API
export default class Component {
  /**
   * @param {String} variant device variant, i.e. 'Amsterdam'
   * @param {String} version device SW version, i.e. '1.0.0'
   * @param {String} language ISO639 code, i.e. 'en'
  */
  constructor(variant, version, language) {
    this._variant = variant;
    this._version = version;
    this._language = this.getSupportedLanguages().includes(language) ? language : 'en';
    this._packages = new Packages(variant, version, language);
  }
  /**
   * Returns Component version
   * @return {String} semantic version, i.e. '1.0.0'
  */
  getVersion() {
    return PackageJson.version;
  }
  /**
   * Returns SIM hash
   * @return {String} first 12 characters of SIM hash
  */
  getSimHash() {
    return SimInfo.hash;
  }
  /**
   * Returns supported languages using ISO639 codes
   * @return {String} stringified String[], i.e. '['en', 'fi']'
  */
  getSupportedLanguages() {
    return JSON.stringify(Localization.supportedLanguages());
  }
  /**
   * Tells whether or not device supports sport mode customization.
   * @return {Integer} 0 if not supported, 1 if supported but too old version,
   * 2 if supported and 3 if supported but changing name is not supported.
  */
  isDeviceSupported() {
    const supportedVariants = [
      'Amsterdam',
      'Brighton',
      'Cairo',
      'Forssa',
      'Gdansk',
      'Helsinki',
      'Ibiza',
      'Kyoto',
      'Lima',
      'Monza',
      'Oulu',
      'Nagano',
    ];

    const regionlessVariant = this._getRegionlessVariant();

    if (!supportedVariants.includes(regionlessVariant)) {
      return 0;
    }

    // Generally new models are added to this switch and in the
    // supportedVariants list above
    switch (regionlessVariant) {
      case 'Helsinki':
        // Support for carousel template was added to 2.3.16
        return Packages.versionCompare(this._version, '2.3.16') ? 2 : 1;
      case 'Monza':
        // Support for Monza and its LapTable was added in 2.7.10
        return Packages.versionCompare(this._version, '2.7.10') ? 2 : 1;
      case 'Oulu':
        return Packages.versionCompare(this._version, '2.10.2') ? 2 : 1;
      case 'Kyoto':
        return Packages.versionCompare(this._version, '1.122.0') ? 2 : 1;
      case 'Nagano':
        return Packages.versionCompare(this._version, '2.13.12') ? 2 : 1;
      default:
        break;
    }

    const split = this._version.split('.');
    const major = parseInt(split[0], 10);
    const minor = parseInt(split[1], 10);

    if (major === 2) {
      switch (minor) {
        case 0:
          // 2.0.42 is a production version which has a bug which causes the
          // name of the sport mode not to update correctly if changing.
          return !Packages.versionCompare(this._version, '2.0.42') ? 1 : 3;
        case 1:
          return !Packages.versionCompare(this._version, '2.1.42') ? 1 : 2;
        default:
          break;
      }
    }

    if (!Packages.versionCompare(this._version, '2.2.10')) {
      return 1;
    }

    return 2;
  }
  /**
   * Returns all available sportmodes for customization
   * @return {String} stringified ActivityHeader[]
  */
  getSportModes() {
    return JSON.stringify(this._packages.sportModes());
  }
  /**
   * Returns currently active sportmodes
   * @param {String} watchSportModes stringified
   * @return {String} stringified SportModeHeader[]
  */
  getCurrentSportModes(watchSportModes) {
    return JSON.stringify(SportModes.list(watchSportModes, this._language));
  }
  /**
   * Returns sport mode template for specific activity type
   * @param {Number} activityId
   * @param {String} watchSportModes stringified
   * @return {String} stringified WatchSportMode
  */
  getSportModeTemplate(activityId, watchSportModes) {
    return JSON.stringify(this._packages.sportModeTemplate(
      activityId,
      SportModes.newModeId(watchSportModes)
    ));
  }
  /**
   * Returns displays for sportmode. Settings are needed in
   * order to check if navigation view is visible or not.
   * @param {String} watchSportModeDisplays stringified
   * @param {String} watchSportModeSettings stringified
   * @return {String} stringified Display[]
  */
  getCurrentDisplays(watchSportModeDisplays, watchSportModeSettings) {
    const currentDisplays = new WatchSportModeDisplays(watchSportModeDisplays).displays();

    // Make sure user doesn't be able to delete last non-interval display as sport mode
    // is not operational without one
    const numberOfNonIntervalDisplays =
      currentDisplays.filter(display => (!Packages.isIntervalDisplay(display.TemplateType))).length;

    const lastNonIntervalDisplayIndex = numberOfNonIntervalDisplays === 1 ?
      currentDisplays.findIndex(display => (!Packages.isIntervalDisplay(display.TemplateType))) :
      -1;

    const displays = currentDisplays
      .filter(display => !Packages.getHiddenDisplays().includes(display.TemplateType))
      .map((display, index) => {
        // Type i.e. 'Data3Fields' or 'Interval37'
        const type = display.TemplateType;
        const intervalType = Packages.isIntervalDisplay(type);
        // 'Data3Fields' is unchanged, but interval displays are expanded
        // to Pace, Power etc.
        const expandedType = intervalType ?
          Packages.getIntervalDisplayName(type, display.Fields) :
          type;
        return new Display(
          type,
          this._packages.displayName(type, display.Fields),
          intervalType,
          DisplayIconMap[expandedType].values,
          DisplayIconMap[expandedType].placeholder,
          lastNonIntervalDisplayIndex !== index,
          true
        );
      });

    const addMapDisplay = () => {
      displays.push(new Display(
        'Breadcrumb',
        Localization.translate('TXT_BREADCRUMB_VIEW', this._language),
        true,
        this._isWearRoundVariant() ?
          DisplayIconMap.MapWearRound.values :
          DisplayIconMap.Navigation.values,
        undefined,
        false,
        false
      ));
    };

    if (this._isWearRoundVariant()) {
      // Wear variant(s) have actual Map display within current displays
      if (currentDisplays.some(display => display.TemplateType === Displays.MAP_WEAR_ROUND)) {
        addMapDisplay();
      }
    } else if (this._isNavigationSupported() && watchSportModeSettings) {
      // Navigation/Breadcrumb is handled separately based on settings
      // on NG1 devices i.e. there is no Map display within displays
      if (new WatchSportModeSettings(watchSportModeSettings).getGpsInterval() > 0) {
        addMapDisplay();
      }
    }

    return JSON.stringify(displays);
  }
  /**
   * @deprecated Read dynamic "deletable" field in getCurrentDisplays instead.
   *
   * Returns the min number of displays for given activity. Not applicable for
   * factory modes. Includes read-only displays. Settings are needed to determine
   * whether or not navigation view is shown.
   * @param {String} watchSportModeSettings
   * @return {Number} number of displays
  */
  getMinNumberOfDisplays(watchSportModeSettings) {
    return this._packages.minNumberOfDisplays() +
      ((this._isNavigationSupported() && watchSportModeSettings &&
       new WatchSportModeSettings(watchSportModeSettings).getGpsInterval() > 0) ?
        1 : 0);
  }
  /**
   * Returns the max number of displays for given activity. Not applicable for
   * factory modes. Includes read-only displays. Settings are needed to determine
   * whether or not navigation view is shown.
   * @param {String} watchSportModeSettings
   * @return {Number} number of displays
  */
  getMaxNumberOfDisplays(watchSportModeSettings) {
    return this._packages.maxNumberOfDisplays() +
      ((this._isNavigationSupported() && watchSportModeSettings &&
       new WatchSportModeSettings(watchSportModeSettings).getGpsInterval() > 0) ?
        1 : 0);
  }
  /**
   * Returns all current active fields
   * @param {String} watchSportModeDisplays stringified
   * @param {Number} displayIndex
   * @return {String} stringified Field[]
  */
  getCurrentFields(watchSportModeDisplays, displayIndex) {
    const obj = new WatchSportModeDisplays(watchSportModeDisplays);
    const displays = obj.displays();
    if (displayIndex >= displays.length || !displays[displayIndex].Fields) {
      return JSON.stringify([]);
    }

    const template = obj.templates()[displayIndex];
    if (Packages.getHiddenDisplays().includes(template)) {
      return JSON.stringify([]);
    }

    const fields = displays[displayIndex].Fields
      .filter(field => Packages.isFieldModifiable(field.ID, template)).map((field) => {
        const phraseId = this._packages.fieldPhraseId(field, template, field.ID);
        return new Field(phraseId, this._packages.translatePhraseId(phraseId));
      });

    // Interval displays have fields which are not visible
    // for user in main view
    if (Packages.isIntervalDisplay(template)) {
      return JSON.stringify(fields.slice(
        0,
        Packages.intervalDisplayNumberOfFields(template)
      ));
    }

    return JSON.stringify(fields);
  }
  /**
   * Returns all available displays
   * @param {String} watchSportModeDisplays stringified
   * @param {Number} activityId
   * @param {Number} displayIndex
   * @return {String} stringified DisplaySection[]
  */
  getDisplays(watchSportModeDisplays, activityId, displayIndex) {
    const obj = JSON.parse(watchSportModeDisplays);
    const currentDisplays = obj.Settings.CustomModes[0].Displays.map(watchDisplay =>
      new Display(watchDisplay.TemplateType));
    return JSON.stringify(this._packages.displayList(activityId, currentDisplays, displayIndex));
  }
  /**
   * Changes display in sportmode
   * @param {String} watchSportModeDisplays stringified
   * @param {String} watchSportModeSettings stringified
   * @param {Number} displayIndex display to replace, starting from 0
   * @param {String} displayId new display, i.e. 'Data4fields'
   * @return {String} Stringified WatchSportMode
  */
  changeDisplay(watchSportModeDisplays, watchSportModeSettings, displayIndex, displayId) {
    const displaysObj = new WatchSportModeDisplays(watchSportModeDisplays);
    const settingsObj = new WatchSportModeSettings(watchSportModeSettings);

    const currentFields = displaysObj.displays()[displayIndex].Fields;
    const currentTemplate = displaysObj.templates()[displayIndex];

    if (Packages.isIntervalDisplaySubType(displayId)) {
      const display = this._packages.intervalDisplay(displayId);
      // Interval displays are using phrase ID's since they don't
      // have proper names besides 'Interval<X>7' which doesn't
      // identify the flavor (like 'Speed and pace')
      displaysObj.displays()[displayIndex] = WatchSportModeDisplays.filterDisplay({
        Fields: display.Fields,
        TemplateType: display.TemplateType,
      });
    } else {
      const activityId = settingsObj.getActivityID();
      const formatters = Packages.displayFormatStyles(displayId);
      const newDisplay = JSON.parse(JSON.stringify(Object.keys(formatters)
        .map(item => ({ ID: item }))));

      const getField = (phraseId, id) => this._packages.field(phraseId, displayId, id);

      if (Packages.isColumnsDisplay(displayId)) {
        // No merge is done for columns since those need special default values
        newDisplay.forEach((newField) => {
          Object.assign(newField, this._getColumnField(activityId, newField.ID, displayId));
        });
      } else if (Packages.isZoneDisplay(displayId)) {
        newDisplay.forEach((newField) => {
          Object.assign(newField, this._getZoneField(newField.ID, displayId));
        });
      } else if (Packages.isGraphDisplay(displayId)) {
        // No merge is done for graph since it needs special default values
        newDisplay.forEach((newField) => {
          Object.assign(newField, this._getGraphField(activityId, newField.ID, displayId));
        });
      } else {
        const fieldProperties = Packages.displayFieldProperties(displayId);

        newDisplay.forEach((newField, index) => {
          // SIM has default field value for some of the newer displays
          const defaultField =
            Component.getDefaultFieldForDisplay(activityId, fieldProperties, newField.ID);

          // If field is not customizable by the user (i.e is "Static"),
          // use default field value from SIM
          if (defaultField && Packages.isStaticFieldForDisplay(fieldProperties, newField.ID)) {
            Object.assign(newField, getField(defaultField, newField.ID));

            // Fields from Graph or Interval (etc.) displays are not merged to
            // new display
          } else if (!Packages.isGraphDisplay(currentTemplate) &&
              !Packages.isIntervalDisplay(currentTemplate) &&
              !Packages.isZoneDisplay(currentTemplate) &&
              !Packages.isColumnsDisplay(currentTemplate) &&
              currentFields.length > index) {
            // ID is i.e. 'Top' so we don't want to copy that as field positions
            // might have changed, use ID from new field instead
            delete currentFields[index].ID;
            Object.assign(newField, currentFields[index]);
          } else {
            // Fill rest of the fields with default (duration)
            Object.assign(newField, getField('TXT_DURATION', newField.ID));
          }

          // Update formatter
          Object.assign(newField, { ValueFormatStyle: formatters[newField.ID] });
        });
      }

      displaysObj.displays()[displayIndex] = {
        Fields: newDisplay,
        TemplateType: displayId,
      };
    }

    const updatedSettings = Component.updateIntervalSetting(settingsObj, displaysObj);

    return JSON.stringify(new WatchSportMode(
      null,
      null,
      WatchSportMode.createModes([null], [updatedSettings], [displaysObj])
    ));
  }
  /**
   * Get all fields from display
   * @param {Number} activityId
   * @param {String} displayId i.e. 'SimpleLineChart'
   * @param {Number} fieldIndex starting from 0
   * @return {String} stringified FieldSection[]
  */
  getFields(activityId, displayId, fieldIndex) {
    return JSON.stringify(this._packages.fields(activityId, displayId, fieldIndex));
  }
  /**
   * Change field in display
   * @param {String} watchSportModeDisplays stringified
   * @param {Number} displayIndex display to modify, starting from 0
   * @param {Number} fieldIndex field to be replaced, starting from 0
   * @param {String} fieldId new field, i.e. 'TXT_DURATION'
   * @return {String} stringified WatchSportModeDisplays
  */
  changeField(watchSportModeDisplays, displayIndex, fieldIndex, fieldId) {
    const obj = new WatchSportModeDisplays(watchSportModeDisplays);
    const display = obj.displays()[displayIndex];

    // Update field index. Given field index only contains modifiable fields.
    let fieldDataIndex = fieldIndex;
    display.Fields.forEach((field, index) => {
      if (!Packages.isFieldModifiable(field.ID, display.TemplateType) && index <= fieldDataIndex) {
        fieldDataIndex += 1;
      }
    });

    const getNewField = (position) => {
      // Preserve ID (position, i.e. 'Top') from current field
      const newField = {
        ID: position,
      };
      // Append data to new object to preserve order (ID first)
      Object.assign(newField, this._packages.field(fieldId, display.TemplateType, newField.ID));
      return newField;
    };

    const updatedPosition = display.Fields[fieldDataIndex].ID;
    display.Fields[fieldDataIndex] = getNewField(updatedPosition);

    if (display.TemplateType === Displays.ZONES_8_FIELDS && updatedPosition === Fields.TOP) {
      // 'Zones8Fields' display has also 'Top2nd' and 'Top3rd' fields which
      // should be synced with 'Top' so that the field selected by user is shown
      // regardless of which zones are active.
      const syncedFields = [
        Fields.TOP_SECOND,
        Fields.TOP_THIRD,
      ];

      syncedFields.forEach((position) => {
        const syncedFieldIndex = display.Fields.findIndex(x => (x.ID === position));
        if (syncedFieldIndex >= 0) {
          display.Fields[syncedFieldIndex] = getNewField(position);
        }
      });
    }

    return JSON.stringify(obj);
  }
  /**
   * Add display to sportmode. Display cannot be added after
   * non-deletable displays, i.e. Breadcrumb.
   * @param {String} watchSportModeDisplays stringified
   * @param {String} watchSportModeSettings stringified
   * @param {Number} displayIndex
   * @param {String} displayId
   * @return {String} stringified WatchSportMode
  */
  addDisplay(watchSportModeDisplays, watchSportModeSettings, displayIndex, displayId) {
    const displaysObj = new WatchSportModeDisplays(watchSportModeDisplays);
    const settingsObj = new WatchSportModeSettings(watchSportModeSettings);

    let newDisplay;
    if (Packages.isIntervalDisplaySubType(displayId)) {
      if (displaysObj.templates().filter(template =>
        Packages.isIntervalDisplay(template)).length !== 0) {
        // Only one interval display is allowed
        return '';
      }
      const display = this._packages.intervalDisplay(displayId);
      // Interval displays are using phrase ID's since they don't
      // have proper names besides 'Interval<X>7' which doesn't
      // identify the flavor (like 'Speed and pace')
      newDisplay = {
        Fields: display.Fields,
        TemplateType: display.TemplateType,
      };
    } else {
      const activityId = settingsObj.getActivityID();
      const formatters = Packages.displayFormatStyles(displayId);
      const display = JSON.parse(JSON.stringify(Object.keys(formatters)
        .map(item => ({ ID: item }))));

      if (Packages.isColumnsDisplay(displayId)) {
        display.forEach((newField) => {
          Object.assign(newField, this._getColumnField(activityId, newField.ID, displayId));
        });
      } else if (Packages.isZoneDisplay(displayId)) {
        display.forEach((newField) => {
          Object.assign(newField, this._getZoneField(newField.ID, displayId));
        });
      } else if (Packages.isGraphDisplay(displayId)) {
        display.forEach((newField) => {
          Object.assign(newField, this._getGraphField(activityId, newField.ID, displayId));
        });
      } else {
        const fieldProperties = Packages.displayFieldProperties(displayId);

        display.forEach((newField) => {
          // SIM has default field value for some of the newer displays
          const defaultField =
            Component.getDefaultFieldForDisplay(activityId, fieldProperties, newField.ID);

          // Newer displays have properties telling which field to use,
          // default to Duration if not knowing better
          Object.assign(newField, this._packages.field(defaultField || 'TXT_DURATION', displayId, newField.ID));

          // Update formatter
          Object.assign(newField, { ValueFormatStyle: formatters[newField.ID] });
        });
      }

      newDisplay = {
        Fields: display,
        TemplateType: displayId,
      };
    }

    // Don't allow adding new displays after hidden displays
    const adjustedDisplayIndex = displaysObj.displays().reduce((adjusted, current, index) => {
      const isHiddenDisplay = Packages.getHiddenDisplays().some(hiddenDisplay =>
        hiddenDisplay === current.TemplateType);

      if (isHiddenDisplay && adjusted > index) {
        // reduce is not handled in lint, there is no way to avoid reassign
        adjusted = index; // eslint-disable-line no-param-reassign
      }

      return adjusted;
    }, displayIndex);

    displaysObj.displays().splice(adjustedDisplayIndex, 0, newDisplay);

    // Update interval setting
    const updatedSettings = Component.updateIntervalSetting(settingsObj, displaysObj);
    return JSON.stringify(new WatchSportMode(
      null,
      null,
      WatchSportMode.createModes([null], [updatedSettings], [displaysObj])
    ));
  }
  /**
   * Delete display
   * @param {String} watchSportModeDisplays stringified
   * @param {String} watchSportModeSettings stringified
   * @param {Number} displayIndex
   * @return {String} stringified WatchSportMode
  */
  deleteDisplay(watchSportModeDisplays, watchSportModeSettings, displayIndex) {
    const displaysObj = new WatchSportModeDisplays(watchSportModeDisplays);
    const settingsObj = new WatchSportModeSettings(watchSportModeSettings);

    displaysObj.displays().splice(displayIndex, 1);

    const updatedSettings = Component.updateIntervalSetting(settingsObj, displaysObj);

    return JSON.stringify(new WatchSportMode(
      null,
      null,
      WatchSportMode.createModes([null], [updatedSettings], [displaysObj])
    ));
  }
  /**
   * Returns setting sections from sportmode
   * @param {String} watchSportModeSettings stringified
   * @return {String} stringified SettingSection[]
  */
  getSettingSections(watchSportModeSettings) {
    return JSON.stringify(Settings.sections(
      new WatchSportModeSettings(watchSportModeSettings),
      this._variant,
      this._version,
      this._language
    ));
  }
  /**
   * Returns setting from sportmode
   * @param {String} watchSportModeGroup stringified
   * @param {String} watchSportModeSettings stringified
   * @param {Number} settingId
   * @return {String} stringified Setting
  */
  getSetting(watchSportModeGroup, watchSportModeSettings, settingId) {
    return JSON.stringify(Settings.getSetting(
      new WatchSportModeGroup(watchSportModeGroup),
      new WatchSportModeSettings(watchSportModeSettings),
      this._language,
      this._variant,
      settingId
    ));
  }
  /**
   * Changes setting from sportmode
   * @param {String} watchSportModeGroup stringified
   * @param {String} watchSportModeSettings stringified
   * @param {String} settingsId i.e. 'Name'
   * @param {String/Number/Boolean} setting value of which type depends on setting
   * @return {String} stringified WatchSportMode
  */
  changeSetting(watchSportModeGroup, watchSportModeSettings, settingId, value) {
    return JSON.stringify(Settings.changeSetting(
      watchSportModeGroup,
      watchSportModeSettings,
      settingId,
      value
    ));
  }
  /**
   * Returns array of supported extensions since 1.0.0 version.
   * @return {String[]}
  */
  getSupportedExtensions() {
    return JSON.stringify([]);
  }
  /**
   * Returns true if user's sport mode list is full and mode needs to
   * be removed before new mode can be added.
   * @param {String} watchSportModes stringified
   * @param {Boolean} factory set to true if adding factory mode, false if
   * custom mode
   * @return {Boolean} true if list is full
  */
  isListFull(watchSportModes, factory) {
    return SportModes.isListFull(watchSportModes, factory);
  }
  /**
   * Updates interval setting based on displays
   * @param {WatchSportModeSettings} watchSportModeSettings
   * @param {WatchSportModeDisplays} watchSportModeDisplays
   * @return {WatchSportModeSettings}
  */
  static updateIntervalSetting(watchSportModeSettings, watchSportModeDisplays) {
    const remainingTemplates = watchSportModeDisplays.templates()
      .filter(template => Packages.isIntervalDisplay(template));
    const stillHasIntervalDisplay = remainingTemplates.length > 0;

    if (watchSportModeSettings.getHasIntervalDisplay() !== stillHasIntervalDisplay) {
      const copy = new WatchSportModeSettings(JSON.stringify(watchSportModeSettings));
      copy.setHasIntervalDisplay(stillHasIntervalDisplay);
      return copy;
    }

    return watchSportModeSettings;
  }
  /**
   * Returns default field for column display based on the position.
   * @param {Integer} activityId Activity ID, e.g. 3 for Running
   * @param {String} fieldId I.e. 'Top'
   * @param {String} displayId I.e. 'Table3Columns'
   * @return {Object} field object
  */
  _getColumnField(activityId, fieldId, displayId) {
    // Newer templates have default values defined in SIM
    const fieldProperties = Packages.displayFieldProperties(displayId);

    const defaultFieldFromSim =
      Component.getDefaultFieldForDisplay(activityId, fieldProperties, fieldId);

    if (defaultFieldFromSim) {
      return this._packages.field(defaultFieldFromSim, displayId, fieldId);
    }

    switch (fieldId) {
      case 'IndexColumn':
        // Index column is non-modifiable lap number
        return this._packages.field('TXT_LAP_NUMBER', displayId, fieldId);
      case 'RightColumn':
        return this._packages.field('TXT_LAP_AVG_HEART_RATE', displayId, fieldId);
      case 'Top':
      case 'Bottom':
        return this._packages.field('TXT_DURATION', displayId, fieldId);
      default:
        // Fill rest of the fields with default (lap duration)
        return this._packages.field('TXT_LAP_DURATION', displayId, fieldId);
    }
  }
  /**
   * Returns default field for zone display based on the position.
   * @param {String} fieldId I.e. 'Top'
   * @param {String} displayId I.e. 'Zones8Fields'
   * @return {Object} field object
  */
  _getZoneField(fieldId, displayId) {
    switch (fieldId) {
      // Shown field depends on which zones user has active. Since
      // Top is customized by the user, Component will take care that they
      // are kept in sync
      case Fields.TOP:
      case Fields.TOP_SECOND:
      case Fields.TOP_THIRD:
        return this._packages.field('TXT_AVERAGE_HEART_RATE', displayId, fieldId);
      case Fields.CENTER:
        // Shown value in Center depends on which zones user has as active,
        // it is not customizable by user
        return this._packages.field('TXT_HEART_RATE', displayId, fieldId);
      case Fields.CENTER_SECOND:
        return this._packages.field('TXT_POWER_3S', displayId, fieldId);
      case Fields.CENTER_THIRD:
        return this._packages.field('TXT_PACE', displayId, fieldId);
      default:
        return this._packages.field('TXT_DURATION', displayId, fieldId);
    }
  }
  /**
   * Returns default field for graph display based on the position.
   * @param {Integer} activityId Activity ID, e.g. 3 for Running
   * @param {String} fieldId I.e. 'Top'
   * @param {String} displayId I.e. 'SimpleLineChart'
   * @return {Object} field object
  */
  _getGraphField(activityId, fieldId, displayId) {
    // Newer templates have default values defined in SIM
    const fieldProperties = Packages.displayFieldProperties(displayId);

    const defaultFieldFromSim =
      Component.getDefaultFieldForDisplay(activityId, fieldProperties, fieldId);

    if (defaultFieldFromSim) {
      return this._packages.field(defaultFieldFromSim, displayId, fieldId);
    }

    // Default hard-coded behaviour for old displays templates
    switch (fieldId) {
      case 'Top':
      case 'Chart':
        return this._packages.field('TXT_HEART_RATE', displayId, fieldId);
      default:
        return this._packages.field('TXT_DURATION', displayId, fieldId);
    }
  }
  /**
   * Returns true if device supports navigation.
   * @return {Boolean} true if supported
  */
  _isNavigationSupported() {
    return this._getRegionlessVariant(this._variant) !== 'Helsinki';
  }
  /**
   * Returns variant name without region character at the end.
   * For example 'Amsterdam' and 'AmsterdamC' both return 'Amsterdam'.
   * @return {String} regionless variant name
  */
  _getRegionlessVariant() {
    const variant = this._variant;
    return variant.charAt(variant.length - 1) === 'C' ?
      variant.substring(0, variant.length - 1) :
      variant;
  }
  /**
   * Check if device is Wear Round variant.
   * @return {Boolean} true if device is Wear Round variant
  */
  _isWearRoundVariant() {
    return this._getRegionlessVariant(this._variant) === 'Kyoto';
  }
  /**
   * Returns default field value (like Duration) for given field
   * ID. Only newer displays have it and caller must prepare for NOT
   * receiving one.
   * @param {Integer} activityId Activity ID, e.g. 3 for Running
   * @param {Object} fieldProperties Field properties for display
   * @param {String} fieldId e.g. 'Top'
   * @return {String} Field ID e.g. 'TXT_DURATION' or null if does
   * not exist for this display/field.
  */
  static getDefaultFieldForDisplay(activityId, fieldProperties, fieldId) {
    if (fieldProperties) {
      const fieldProperty = fieldProperties[fieldId];
      if (fieldProperty) {
        if (!fieldProperty.DefaultFieldId) {
          return null;
        } else if ((typeof fieldProperty.DefaultFieldId) === 'string') {
          // In simple case DefaultFieldId is a string
          //
          // "DefaultFieldId": "TXT_DURATION",
          return fieldProperty.DefaultFieldId;
        }

        // DefaultFieldId can also be an object which has specific
        // values for certain activity types
        //
        // "DefaultFieldId": {
        //     "Default": "TXT_LAP_DURATION",
        //     "Specific": [
        //         {
        //             "Id": "TXT_INTERVAL_SWIMDISTANCE",
        //             "UsedInActivities": [6]
        //         }
        //     ]
        // },
        const defaultFieldIdObj = fieldProperty.DefaultFieldId;

        if (defaultFieldIdObj.Specific) {
          const specific = defaultFieldIdObj.Specific.filter(item =>
            item.UsedInActivities.includes(activityId));

          // Return first match, any activity ID should not be defined
          // in two different items
          if (specific && specific.length > 0) {
            return specific[0].Id;
          }
        }

        return defaultFieldIdObj.Default;
      }
    }
    return null;
  }
}

// Plain function API uses global variable where Component instance
// is initialized
let instance;

export function initialize(...args) {
  instance = new Component(...args);
}
export function getVersion(...args) {
  return instance.getVersion(...args);
}
export function getSimHash(...args) {
  return instance.getSimHash(...args);
}
export function getSupportedLanguages(...args) {
  return instance.getSupportedLanguages(...args);
}
export function isDeviceSupported(...args) {
  return instance.isDeviceSupported(...args);
}
export function getSportModes(...args) {
  return instance.getSportModes(...args);
}
export function getCurrentSportModes(...args) {
  return instance.getCurrentSportModes(...args);
}
export function getSportModeTemplate(...args) {
  return instance.getSportModeTemplate(...args);
}
export function getCurrentDisplays(...args) {
  return instance.getCurrentDisplays(...args);
}
export function getMinNumberOfDisplays(...args) {
  return instance.getMinNumberOfDisplays(...args);
}
export function getMaxNumberOfDisplays(...args) {
  return instance.getMaxNumberOfDisplays(...args);
}
export function getCurrentFields(...args) {
  return instance.getCurrentFields(...args);
}
export function getDisplays(...args) {
  return instance.getDisplays(...args);
}
export function changeDisplay(...args) {
  return instance.changeDisplay(...args);
}
export function getFields(...args) {
  return instance.getFields(...args);
}
export function changeField(...args) {
  return instance.changeField(...args);
}
export function addDisplay(...args) {
  return instance.addDisplay(...args);
}
export function deleteDisplay(...args) {
  return instance.deleteDisplay(...args);
}
export function getSettingSections(...args) {
  return instance.getSettingSections(...args);
}
export function getSetting(...args) {
  return instance.getSetting(...args);
}
export function changeSetting(...args) {
  return instance.changeSetting(...args);
}
export function getSupportedExtensions(...args) {
  return instance.getSupportedExtensions(...args);
}
export function isListFull(...args) {
  return instance.isListFull(...args);
}
