/** @module Packages */
import ActivityHeader from '../datamodels/activityheader';
import WatchSportMode from '../datamodels/watchsportmode/mode';
import WatchSportModeGroup from '../datamodels/watchsportmode/group';
import WatchSportModeSettings from '../datamodels/watchsportmode/settings';
import WatchSportModeDisplays from '../datamodels/watchsportmode/displays';
import Display from '../datamodels/display';
import DisplaySection from '../datamodels/displaysection';
import Field from '../datamodels/field';
import FieldSection from '../datamodels/fieldsection';
import BaseModes from '../../suunto-information-model/Specifications/CustomModes/BaseModes.json';
import DisplayTemplates from '../../suunto-information-model/Specifications/CustomModes/DisplayTemplates.json';
import IntervalTemplates from '../../suunto-information-model/Specifications/CustomModes/IntervalTrainingDisplayDefaults.json';
import DisplayFields from '../../suunto-information-model/Specifications/CustomModes/DisplayFieldList.json';
import FieldDefaultFormatStyles from '../../suunto-information-model/Specifications/CustomModes/DisplayTemplateDefaultFormatStyles.json';
import Localization from '../localization/localization';
import DisplayIconMap from '../displayiconmap';
import ActivityIdCompatibility from '../../sim-tools/activityids/activityIds.json';
import * as Displays from '../constants/displayIds';
import * as Fields from '../constants/fieldIds';

/* eslint class-methods-use-this: ["error", { "exceptMethods": [
   "minNumberOfDisplays",
   "maxNumberOfDisplays",
   "fieldPhraseId"] }] */

export default class Packages {
  constructor(variant, version, language) {
    this._variant = variant;
    this._version = version;
    this._language = language;
  }
  /**
   * Returns all available sportmodes for customization
   * @return {ActivityHeader[]} activityHeaders
  */
  sportModes() {
    // Make sure there actually exists a sport mode template for given
    // activity ID
    const availableTemplates = this._getAvailableTemplates();

    // ActivityIdCompatibility contains every sport mode this particular
    // variant supports out-of-the-box
    return ActivityIdCompatibility.reduce((total, activityIdItem) => {
      const compatibility = activityIdItem.compatibility
        .filter(deviceItem =>
          deviceItem.DeviceName === this._variant &&
          Packages.versionCompare(this._version, deviceItem.FirmwareVersion));

      const id = activityIdItem.activityId;

      // Component does not yet support multisports
      const isTriathlon = i => i === 19;

      if (!isTriathlon(id) && compatibility.length && !total.includes(id)) {
        total.push(id);
      }

      return total;
    }, [])
      .filter(item => availableTemplates.includes(item))
      .map(item => new ActivityHeader(item));
  }
  /**
   * Returns sport mode template for specific activitytype
   * @param {Number} activityId
   * @param {Number} modeId
   * @return {WatchSportMode} watchSportMode
  */
  sportModeTemplate(activityId, modeId) {
    // Search modes for given activity
    const modes = Object.keys(BaseModes).filter(key =>
      (BaseModes[key].UsedInActivities.find(x => x === activityId) !== undefined &&
       BaseModes[key].Modes.filter(mode =>
         mode.Compatibility[this._variant] !== undefined).length > 0));
    if (modes.length === 0) {
      // Base mode not found for given activity
      return undefined;
    }

    // Search latest supported version
    const mode = BaseModes[modes[0]].Modes.reduce((selectedMode, currentMode) => {
      if (Packages.versionCompare(this._version, currentMode.Compatibility[this._variant])) {
        if (selectedMode === undefined ||
            Packages.versionCompare(
              currentMode.Compatibility[this._variant],
              selectedMode.Compatibility[this._variant]
            )) {
          return currentMode;
        }
      }
      return selectedMode;
    }, undefined);

    if (mode === undefined) {
      // Template not found for given activity id
      return undefined;
    }

    // Create group, set name and Id (int32) for new sportmode
    /* eslint no-bitwise: ["error", { "allow": ["|"] }] */
    const groupId = 0xC0000000 | modeId; // Single sport and user owned
    const group = new WatchSportModeGroup();
    group.createNew(activityId);
    group.Settings.CustomModeGroups[0].Name = Localization.translate('TXT_NEW_SPORT_MODE', this._language);
    group.Settings.CustomModeGroups[0].CustomModeIDs = [modeId];

    const settings = new WatchSportModeSettings();
    settings.initFromSim(mode.Mode, activityId);
    const displays = new WatchSportModeDisplays();
    displays.initFromSim(mode.Mode.Displays);
    return new WatchSportMode(
      group,
      groupId,
      WatchSportMode.createModes([modeId], [settings], [displays])
    );
  }
  /**
   * Returns all available displays for given sportmode for given display
   * @param {Number} activityId
   * @param {Display[]} currentDisplays
   * @param {Number} displayIndex
   * @return {DisplaySection[]}
  */
  displayList(activityId, currentDisplays, displayIndex) {
    const fieldDisplays = [];
    const columnDisplays = [];
    const zoneDisplays = [];

    Object.keys(DisplayTemplates).forEach((key) => {
      const compatibility =
            DisplayTemplates[key].Compatibility.filter(elem =>
              (elem.DeviceName === this._variant &&
               Packages.versionCompare(this._version, elem.FirmwareVersion)));
      const template = DisplayTemplates[key];
      if (compatibility.length &&
          (!template.MaxInstancesInSportMode ||
           currentDisplays.filter((display, index) =>
             (index !== displayIndex && display.id === key)).length <
           template.MaxInstancesInSportMode)) {
        let displaySection;
        if (template.GroupName === 'TXT_COLUMNS') {
          displaySection = columnDisplays;
        } else if (template.GroupName === 'TXT_INTENSITY_ZONES') {
          displaySection = zoneDisplays;
        } else {
          // Default to 'Fields'
          displaySection = fieldDisplays;
        }

        if (!Packages.getHiddenDisplays().includes(key)) {
          displaySection.push(new Display(
            key,
            this.displayName(key),
            false,
            DisplayIconMap[key].values,
            DisplayIconMap[key].placeholder,
            true,
            true
          ));
        }
      }
    });

    // Check if interval display can be added. It is possible if there already isn't
    // interval display and there will remain one non-interval display after the change
    const nonIntervalDisplayIndex =
      currentDisplays.findIndex(display => (!Packages.isIntervalDisplay(display.id)));
    const numberOfNonIntervalDisplays =
      currentDisplays.filter(display => (!Packages.isIntervalDisplay(display.id))).length;

    const intervalDisplays = [];
    const intervalDisplayIndex =
          currentDisplays.findIndex(display => (Packages.isIntervalDisplay(display.id)));

    if ((intervalDisplayIndex === -1 || intervalDisplayIndex === displayIndex) &&
        (numberOfNonIntervalDisplays > 1 ||
        (nonIntervalDisplayIndex !== -1 && nonIntervalDisplayIndex !== displayIndex))) {
      // Filter unsupported templates
      IntervalTemplates.filter((display) => {
        if (display.HiddenInActivities.find(x => x === activityId) !== undefined) {
          // Interval display is hidden for current activity
          return false;
        }
        // Check if version is supported
        return Packages.versionCompare(this._version, display.Compatibility[this._variant]);
      }).forEach((display) => {
        if (intervalDisplays.findIndex(x => (x.id === display.PhraseId)) === -1) {
          intervalDisplays.push(new Display(
            display.PhraseId,
            Localization.translate(display.PhraseId, this._language),
            true,
            DisplayIconMap[display.PhraseId].values,
            DisplayIconMap[display.PhraseId].placeholder,
            true,
            true
          ));
        }
      });
    }

    const displaySections = [];

    if (fieldDisplays.length) {
      displaySections.push(new DisplaySection(
        'Fields',
        Localization.translate('TXT_FIELDS', this._language),
        null,
        fieldDisplays
      ));
    }

    if (columnDisplays.length) {
      displaySections.push(new DisplaySection(
        'Columns',
        Localization.translate('TXT_COLUMNS', this._language),
        null,
        columnDisplays
      ));
    }

    if (zoneDisplays.length) {
      displaySections.push(new DisplaySection(
        'Zones',
        Localization.translate('TXT_INTENSITY_ZONES', this._language),
        null,
        zoneDisplays
      ));
    }

    if (intervalDisplays.length) {
      displaySections.push(new DisplaySection(
        'Intervals',
        Localization.translate('TXT_INT_MODE', this._language),
        Localization.translate('TXT_INTERVAL_DISPLAY_DESCRIPTION_SHORT', this._language),
        intervalDisplays
      ));
    }

    // Sort alphabetically
    return displaySections.map((section) => {
      section.displays.sort(Packages.displaySorter);
      return section;
    });
  }
  /**
   * Returns all available display fields for given sportmode for given field in
   * given display
   * @param {Number} activityId
   * @param {String} displayId i.e. 'SimpleLineChart'
   * @param {Number} fieldIndex starting from 0
  */
  fields(activityId, displayId, fieldIndex = 0) {
    const sections = [];
    if (!Packages.isIntervalDisplaySubType(displayId)) {
      let fieldDataIndex = fieldIndex;

      // Map "UI index" to "Data index" i.e. there might be display
      // 0: TOP (hidden from user, but exists in the data)
      // 1: CENTER
      // 2: BOTTOM
      // so when application asks for index "0", it actually means CENTER because
      // it doesn't know that TOP exists
      Object.keys(Packages.displayFormatStyles(displayId)).forEach((field, index) => {
        if (!Packages.isFieldModifiable(field, displayId) && index <= fieldDataIndex) {
          fieldDataIndex += 1;
        }
      });

      const fieldPosition =
        Object.keys(DisplayTemplates[displayId].DefaultFormatStyles)[fieldDataIndex];

      // Field properties only exist for some of the newer displays -
      // for Spartans etc. it is null
      const fieldProperties = Packages.displayFieldProperties(displayId);
      const fieldPropertiesPosition = fieldProperties ? fieldProperties[fieldPosition] : null;

      // Parsing following (optional) structure to figure out if this
      // display has specific available fields defined in SIM. If not
      // then all fields are considered as available
      //
      // "AvailableFieldIds": [
      //     "Default": [
      //         "TXT_LAP_DISTANCE",
      //         "TXT_LAP_DURATION"
      //     ],
      //     "Specific": [
      //         {
      //             "FieldIds": [
      //                 "TXT_INTERVAL_SWIMDISTANCE"
      //             ],
      //             "UsedInActivities": [6]
      //         }
      //     ]
      // ]
      const availableFieldIds =
          fieldPropertiesPosition && fieldPropertiesPosition.AvailableFieldIds ?
            fieldPropertiesPosition.AvailableFieldIds :
            null;

      // Look for specific available fields section for given Activity ID
      const specificAvailableFields = availableFieldIds &&
          availableFieldIds.Specific &&
            availableFieldIds.Specific.filter(specific =>
              specific.UsedInActivities.includes(activityId)).map(specific => specific.FieldIds);

      // Either use specific fields if found some...
      let availableFields =
        specificAvailableFields && specificAvailableFields.length > 0 ?
          specificAvailableFields[0] : null;

      // ...or default to Default fields if available. In other cases
      // fields are not filtered at all but all are available
      if (!availableFields && availableFieldIds) {
        availableFields = availableFieldIds.Default;
      }

      // Interval displays don't have Fields
      DisplayFields.forEach((field) => {
        if (field.HiddenInActivities.find(x => x === activityId) === undefined &&
            Packages.versionCompare(this._version, field.Compatibility[this._variant]) &&
            (!availableFields || availableFields.includes(field.ValuePhrase))) {
          // 'Chart' supports only selected fields, skip if not one of them
          if (fieldPosition !== Fields.CHART ||
            (field.Plot && field.Plot[displayId])) {
            const groupPhrases = field.GroupPhrases ? field.GroupPhrases : ['TXT_FIELDS'];

            groupPhrases.forEach((groupPhrase) => {
              const sectionIndex = sections.findIndex(section => section.id === groupPhrase);
              if (sectionIndex === -1) {
                sections.push(new FieldSection(
                  groupPhrase,
                  Localization.translate(groupPhrase, this._language),
                  [new Field(field.ValuePhrase, this.translatePhraseId(field.ValuePhrase))]
                ));
              } else if (sections[sectionIndex].fields.find(x => x.id === field.ValuePhrase) ===
                         undefined) {
                // Add to list if the field is not there already
                sections[sectionIndex].fields.push(new Field(
                  field.ValuePhrase,
                  this.translatePhraseId(field.ValuePhrase)
                ));
              }
            });
          }
        }
      });
      const sortSettingNames = (lhs, rhs) => {
        const l = lhs.name.toLowerCase();
        const r = rhs.name.toLowerCase();
        if (l === r) {
          return 0;
        }
        return l > r ? 1 : -1;
      };
      // Sort fields alphabetically
      sections.sort(sortSettingNames);
      sections.forEach((section) => {
        section.fields.sort(sortSettingNames);
      });
    }
    return sections;
  }
  /**
   * Get field template with phrase ID
   * @param {String} phraseID i.e. 'TXT_HEART_RATE'
   * @param {String} displayId i.e. 'Data3Fields', needed for formatter
   * @param {String} position i.e. 'Top', needed for formatter
   * @return {Object} field object
  */
  field(phraseId, displayId, position) {
    const fields = DisplayFields
      .filter(field => field.ValuePhrase === phraseId &&
              Packages.versionCompare(this._version, field.Compatibility[this._variant]));

    if (!fields.length) {
      // Couldn't find requested field
      return undefined;
    }

    // Same field can look different depending on ESW version, sort
    // to decending order so that only the newest is used
    fields.sort(Packages.fieldSorter(this._variant));

    // Some fields have displayId specific "extension"
    const newField =
      JSON.parse(JSON.stringify(fields[0].FieldExt &&
         fields[0].FieldExt[displayId] && fields[0].FieldExt[displayId][position] ?
        fields[0].FieldExt[displayId][position] :
        fields[0].Field));

    // Remove nulls, ESW doesn't like null in all fields
    Object.keys(newField).forEach(key =>
      ((newField[key] == null) && delete newField[key]));

    // Get the formatter
    if (FieldDefaultFormatStyles[displayId] &&
        FieldDefaultFormatStyles[displayId][position]) {
      newField.ValueFormatStyle = FieldDefaultFormatStyles[displayId][position];
    }

    // Append graph default values (scale, unit etc) if needed
    if (position === Fields.CHART && fields[0].Plot[displayId]) {
      Object.assign(newField, fields[0].Plot[displayId]);
    }

    return newField;
  }
  /**
   * Returns the max number of displays. Not applicable for
   * factory modes. Includes read-only displays.
   * @return {Number}
  */
  maxNumberOfDisplays() {
    return 3;
  }
  /**
   * Returns the min number of displays. Not applicable for
   * factory modes. Includes read-only displays.
   * @return {Number}
  */
  minNumberOfDisplays() {
    return 1;
  }
  /**
   * Returns localized name for display type
   * example: displayName('TXT_DISPLAY_3_FIELDS')
   *
   * Since interval displays are named as 'Interval37' and
   * 'Interval77' and it doesn't alone tell which variant
   * of those it is (i.e. 'TXT_POWER_NO_DISTANCE_INTERVAL'),
   * we need to determine it from the fields it contains.
   *
   * @param {String} displayType i.e. 'Data4Fields'
   * @param {Object} fields needed for figuring out interval display names.
   * Can be null for non-interval displays.
   * @return {String} translated string for UI purposes
  */
  displayName(displayType, fields) {
    const name = DisplayTemplates[displayType] ?
      DisplayTemplates[displayType].Name :
      Packages.getIntervalDisplayName(displayType, fields);
    return Localization.translate(name, this._language);
  }
  /**
   * Returns phrase ID for field
   * @param {Object} field
   * @param {String} displayType i.e. 'Data4Fields'
   * @param {String} fieldLocationId i.e. 'Top' or 'Bottom'
   * @return {String} Phrase ID
  */
  fieldPhraseId(field, displayType, fieldLocationId) {
    // Make a copy since the field needs to be possibly manipulated before matching
    const f = JSON.parse(JSON.stringify(field));

    // 'AutoLap' fields does not exist in SIM's Display Fields. Instead we map them
    // to manual lap fields. ESW internally handles whether it shows 'AutoLap' or
    // (manual) 'Lap' fields depending on lap settings.
    if (f.ValueWindow === 'AutoLap') {
      f.ValueWindow = 'Lap';
    }

    // ESW software cannot handle periods in its internal enums, so make sure
    // Component recognizes also all underscore ValueResources, e.g.
    // 'com_google_heart_rate_Bpm_bpm' and 'com.google.heart_rate.Bpm.bpm'
    // are considered as same value
    const specialCharacterHandler = value => value.replace(/\./g, '_');

    // Match from FieldExt[displayType][fieldLocationId]
    const fieldExtMatcher = (simItem, value) => {
      if (simItem.FieldExt && simItem.FieldExt[displayType]) {
        const simDisplayTypeExt = simItem.FieldExt[displayType];
        if (simDisplayTypeExt[fieldLocationId]) {
          const simExtItem = simDisplayTypeExt[fieldLocationId];
          return !simExtItem[value] ||
            specialCharacterHandler(simExtItem[value]) === specialCharacterHandler(f[value]);
        }
      }
      return false;
    };

    // Match from Field AND FieldExt[displayType][fieldLocationId]
    const fieldMatcher = (simItem, value) =>
      (!simItem.Field[value] || simItem.Field[value] === f[value]) ||
        fieldExtMatcher(simItem, value);

    const matchingFields = DisplayFields.filter(simItem =>
      Object.keys(simItem.Field).filter(value => !fieldMatcher(simItem, value)).length === 0)
      .filter((item) => {
        if (!item.Compatibility[this._variant]) {
          return false;
        }

        return Packages.versionCompare(
          this._version,
          item.Compatibility[this._variant]
        );
      });

    if (matchingFields.length < 1) {
      // Couldn't match field to any from the templates
      return 'TXT_INVALID_FIELD';
    }

    // Sort fields so that the latest supported version is first,
    // older versions shouldn't be used anymore
    matchingFields.sort(Packages.fieldSorter(this._variant));

    return matchingFields[0].ValuePhrase;
  }
  /**
   * Translate phrase ID
   * @param {String} phraseId
   * @return {String} translated phrase for UI purposes
  */
  translatePhraseId(phraseId) {
    return Localization.translate(phraseId, this._language);
  }
  /**
   * Sort function for fields for sorting to descending order based on
   * compatibility version.
   * @param {String} lhs
   * @param {String} rhs
   * @return {Number} 0 if equal, 1 if rhs newer, -1 if lhs newer
  */
  static fieldSorter(variant) {
    return (lhs, rhs) => {
      const lhsVersion = lhs.Compatibility[variant];
      const rhsVersion = rhs.Compatibility[variant];
      return Packages.versionCompare(rhsVersion, lhsVersion) ?
        1 :
        -1;
    };
  }
  /**
   * Sort function for displays for sorting to alphabetical order
   * @param {String} lhs
   * @param {String} rhs
   * @return {Number} 0 if equal, 1 if lhs is first, -1 if rhs is first
  */
  static displaySorter(lhs, rhs) {
    const lhsName = lhs.name.toLowerCase();
    const rhsName = rhs.name.toLowerCase();
    if (lhsName === rhsName) {
      return 0;
    }
    return lhsName < rhsName ? -1 : 1;
  }
  /**
   * Get formatters for display
   * @param {String} displayId i.e. 'Data4Fields'
   * @return {Object} formatters for each field
  */
  static displayFormatStyles(displayId) {
    return FieldDefaultFormatStyles[displayId];
  }
  /**
   * Get display field properties. Only available for
   * new or updated displays so can be null.
   * @param {String} displayId i.e. 'Data4Fields'
   * @return {Object} Properties for seleted display, e.g.
   * {
   *   "Top": {
   *     "DefaultFieldId": "TXT_TIMEOFDAY",
   *     "Static": true
   *   }
   * }
   * All fields are optional.
  */
  static displayFieldProperties(displayId) {
    return DisplayTemplates[displayId] ?
      DisplayTemplates[displayId].FieldProperties :
      null;
  }
  /**
   * Get interval display
   * @param {String} displayId i.e. 'TXT_SPEED'
   * @return {Object} display
  */
  intervalDisplay(displayId) {
    // Search latest supported version
    const display = IntervalTemplates.reduce((selectedDisplay, currentDisplay) => {
      if (currentDisplay.PhraseId === displayId &&
          Packages.versionCompare(this._version, currentDisplay.Compatibility[this._variant])) {
        if (selectedDisplay === undefined ||
            Packages.versionCompare(
              currentDisplay.Compatibility[this._variant],
              selectedDisplay.Compatibility[this._variant]
            )) {
          return currentDisplay;
        }
      }
      return selectedDisplay;
    }, undefined);
    if (display === undefined) {
      // Display not found for given Id
      return undefined;
    }
    return WatchSportModeDisplays.filterDisplay(display.Display);
  }
  static versionCompare(version, minimumVersion) {
    if (version === undefined || minimumVersion === undefined) {
      return false;
    }
    const v1 = version.split('.');
    const v2 = minimumVersion.split('.');
    const minLength = Math.min(v1.length, v2.length);
    for (let i = 0; i < minLength; i++) {
      if (Number(v1[i]) < Number(v2[i])) {
        return false;
      } else if (Number(v1[i]) > Number(v2[i])) {
        return true;
      }
    }
    return (v1.length >= v2.length);
  }
  /**
   * Check if displayId is column type
   * @param {String} displayId i.e. 'Interval77' or 'Table2Columns'
   * @return {Boolean} true if column type
  */
  static isColumnsDisplay(displayId) {
    return displayId.includes('Columns') || displayId.includes('LapTable');
  }
  /**
   * Check if displayId is interval type
   * @param {String} displayId i.e. 'Interval77' or 'Data4Fields'
   * @return {Boolean} true if interval type
  */
  static isIntervalDisplay(displayId) {
    return (displayId === 'Interval77' || displayId === 'Interval37');
  }
  /**
   * Check if displayId is graph type
   * @param {String} displayId i.e. 'SimpleLineChart' or 'Data4Fields'
   * @return {Boolean} true if graph type
  */
  static isGraphDisplay(displayId) {
    return displayId === Displays.SIMPLE_LINE_CHART ||
      displayId === Displays.SIMPLE_LINE_CHART_WEAR_ROUND;
  }
  /**
   * Check if displayId is zone type
   * @param {String} displayId i.e. 'Zones8Fields' or 'Data4Fields'
   * @return {Boolean} true if zone type
  */
  static isZoneDisplay(displayId) {
    return displayId === Displays.ZONES_8_FIELDS;
  }
  /**
   * Check if zone field is visible
   * @param {String} fieldLocationId i.e. 'Top' or 'Bottom'
   * @return {Boolean} true if visible, false if hidden
  */
  static zoneFieldVisible(fieldLocationId) {
    switch (fieldLocationId) {
      case Fields.TOP:
      case Fields.BOTTOM:
        return true;
      default:
        return false;
    }
  }
  /**
   * Check if displayId is interval sub type i.e. 'TXT_SPEED_PACE_INTERVAL'
   * @param {String} displayId i.e. 'TXT_SPEED_PACE_INTERVAL' or 'Data4Fields'
   * @return {Boolean} true if interval type
  */
  static isIntervalDisplaySubType(displayId) {
    return IntervalTemplates.filter(template =>
      template.PhraseId === displayId).length > 0;
  }
  static intervalDisplayNumberOfFields(displayId) {
    switch (displayId) {
      case 'Interval77':
        return 7;
      case 'Interval37':
        return 3;
      default:
        return undefined;
    }
  }
  static getIntervalDisplayName(displayType, fields) {
    const simTemplates = IntervalTemplates.filter((template) => {
      const templateFields = template.Display.Fields;
      if (templateFields.length === fields.length) {
        // Need to compare all fields with same index,
        // classic index based loop works best. We cannot
        // deep match them since SIM has nulls and watch uses
        // zeros, so instead we only partial match.
        for (let i = 0; i < templateFields.length; i++) {
          const templateField = templateFields[i];
          const field = fields[i];

          // At the time of writing, ValueFormatStyle is 'Fourdigits'
          // in SIM RunningInterval but in SIM templates
          // IntervalTrainingDisplayDefaults it's 'Fivedigits' for
          // 'TXT_SPEED_PACE_INTERVAL'? Therefore it cannot be used
          // in the mapping.
          if (templateField.ID !== field.ID ||
              templateField.PathRoot !== field.PathRoot) {
            return false;
          }

          // Following checks are skipped if those are null in SIM
          // Watch uses 0 so comparing those unconditionally doesn't
          // yield correct result.
          if ((templateField.ValueAggregate) &&
              (templateField.ValueAggregate !== field.ValueAggregate)) {
            return false;
          } else if ((templateField.ValueResource) &&
              (templateField.ValueResource !== field.ValueResource)) {
            return false;
          } else if ((templateField.ValueWindow) &&
              (templateField.ValueWindow !== field.ValueWindow)) {
            return false;
          } else if ((templateField.ValueFormat) &&
              (templateField.ValueFormat !== field.ValueFormat)) {
            return false;
          }
        }

        // All checked fields matched, this template is a keeper.
        return true;
      }

      return false;
    });

    if (simTemplates.length !== 1) {
      // Something is really broken if this condition ever happens
      // but still prepare for it
      return 'TXT_INVALID_FIELD';
    }

    return simTemplates[0].PhraseId;
  }
  /**
   * Check if field location is modifiable
   * @param {String} fieldLocationId i.e. 'Top' or 'IndexColumn'
   * @param {String} displayId i.e. 'Zones8Fields' or 'Data4Fields'
   * @return {Boolean} true if modifiable
  */
  static isFieldModifiable(fieldLocationId, displayId) {
    // Newer templates have "Static" field, check that first
    const fieldProperties = Packages.displayFieldProperties(displayId);

    if (fieldProperties) {
      const isStatic = Packages.isStaticFieldForDisplay(fieldProperties, fieldLocationId);
      if (isStatic !== undefined) {
        return !isStatic;
      }
    }
    if (Packages.isZoneDisplay(displayId)) {
      return Packages.zoneFieldVisible(fieldLocationId);
    }
    if (displayId === 'Data3Carousel') {
      return (fieldLocationId !== Fields.TOP && fieldLocationId !== Fields.BOTTOM);
    }

    return (fieldLocationId !== 'IndexColumn'); // Resource for index column cannot be changed
  }
  /**
   * Check if field sections are supported in SIM.
   * @return {Boolean} true if field section are set in SIM
  */
  static isFieldSectionsSupported() {
    return (DisplayFields[0].GroupPhrases !== undefined);
  }
  /**
   * Check if basemodes are defined in SIM for given variant.
   * @param {String} variant device variant, i.e. 'Amsterdam'
   * @return {Boolean} true if basemodes are defined in SIM
  */
  static isCustomizationSupported(variant) {
    const modes = Object.keys(BaseModes).filter(key =>
      (BaseModes[key].Modes.filter(mode =>
        mode.Compatibility[variant] !== undefined).length > 0));
    return (modes.length > 0);
  }
  /**
   * Some displays are not visible for the user but still need to
   * exist in the display data for device to function properly
   * @return {Array} array of hidden displays
  */
  static getHiddenDisplays() {
    return [
      'DistanceAutoLap',
      'DurationAutoLap',
      'MapWearRound', // Isn't actually hidden, but it must be at the end of the visible displays
      'ManualLap',
    ];
  }
  /**
   * Check if selected field in display is "Static", i.e not
   * modifiable by the user.
   * @param {Object} fieldProperties Field properties for display
   * @param {String} fieldId e.g. 'Top'
   * @return {Boolean} True/False or undefined
  */
  static isStaticFieldForDisplay(fieldProperties, fieldId) {
    if (fieldProperties) {
      const fieldProperty = fieldProperties[fieldId];
      if (fieldProperty) {
        return fieldProperty.Static;
      }
    }
    return undefined;
  }
  /**
   * Get list of activity IDs which have sport mode template implemented
   * @return {Array} Array of activity IDs
  */
  _getAvailableTemplates() {
    return Object.keys(BaseModes)
      .map(templateType => BaseModes[templateType])
      .filter(templates =>
        templates.Modes.filter((mode) => {
          if (mode.Compatibility[this._variant] !== undefined) {
            if (Packages.versionCompare(this._version, mode.Compatibility[this._variant])) {
              return true;
            }
          }
          return false;
        }).length > 0)
      .reduce((idList, mode) => {
        // reduce is not handled in lint, there is no way to avoid reassign
        idList = idList.concat(mode.UsedInActivities); // eslint-disable-line no-param-reassign
        return idList;
      }, []);
  }
}
