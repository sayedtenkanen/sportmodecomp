// This file is imported from MC Widgets repo with some adaptation
import translations from './translations';

/**
 * interface to the translation object
 * @param  {string} phraseId phraseId of the requested string
 * @param  {string} language languageCode ('en')
 * @return {string}          translated string or english term if translation not found,
 * if term not found returns phraseId
 */
function _getTranslation(phraseId, language) {
  const checkThatTranslationIsOk = str => (str && str.toLowerCase() !== 'new term');
  const returnSimilarLanguageTranslation = (phrase, inputLanguage) => {
    const lang = inputLanguage && inputLanguage.indexOf('-') !== -1 ? inputLanguage.split('-')[0].toLowerCase() : inputLanguage.toString().toLowerCase();
    const closestToOwnlang = Object.keys(translations[phrase])
      .filter(translationLanguage => translationLanguage.toLowerCase().indexOf(lang) !== -1)[0];
    if (translations[phrase][closestToOwnlang]) {
      return translations[phrase][closestToOwnlang];
    }
    return null;
  };

  if (translations && language && phraseId && phraseId !== '' && translations[phraseId]) {
    if (translations[phraseId][language]) {
      if (checkThatTranslationIsOk(translations[phraseId][language])) {
        return translations[phraseId][language];
      } else if (checkThatTranslationIsOk(translations[phraseId].en)) {
        return translations[phraseId].en;
      }
    } else {
      const similarLanguageTranslation = returnSimilarLanguageTranslation(phraseId, language);
      if (checkThatTranslationIsOk(similarLanguageTranslation)) {
        return similarLanguageTranslation;
      } else if (checkThatTranslationIsOk(translations[phraseId].en)) {
        return translations[phraseId].en;
      }
    }
  } else if (translations && !language && phraseId !== '' && translations[phraseId] && checkThatTranslationIsOk(translations[phraseId].en)) {
    return translations[phraseId].en;
  }
  return phraseId;
}

/**
* fetches the translated string with given language
* example: translate('TXT_CRICKET')
* example: translate('TXT_CRICKET', 'fi')
* @param  {string} phraseId phraseId of the requested string
* @param  {string} language language to get the translation in
* @param  {number} optional parameter to help get the correct pluralization.
* @param  {object} translationVariables an object with the shape { 0: '123', 1:'233' } for
* adding values into translations eg. TXT_FAST  = This was fast {0},
* translate(...,...,{0: 1}) -> This was fast 1
* @return {string}          translated string or phraseId if not found
*/

function translate(phraseId, language, translationVariables = {}, amount = 0) {
  let translation = _getTranslation(phraseId, language);
  if (amount === 1) {
    const singularId = `${phraseId}_SINGULAR`;
    const singularTranslation = _getTranslation(singularId, language);

    translation = singularTranslation !== singularId ? singularTranslation : translation;
  }
  if (Object.keys(translationVariables).length > 0) {
    Object.keys(translationVariables).forEach((key) => {
      translation = translation.replace(`{${key}}`, translationVariables[key]);
    });
  }
  return translation;
}

/**
* Returns supported languages using ISO639 codes
* @return {string[]} i.e. ['en', 'fi']
*/
function supportedLanguages() {
  return Object.keys(translations.TXT_DISTANCE);
}

export default { translate, supportedLanguages };
