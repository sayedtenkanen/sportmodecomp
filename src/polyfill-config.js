// Embed functionality which is not natively supported by runtime

// Duktape 2.2.1 (latest version at the time of writing) and older need following
import 'core-js/fn/array/find';
import 'core-js/fn/array/find-index';
import 'core-js/fn/array/includes';
// In addition at least Duktape 1.8.0/1.5.0 need following
import 'core-js/es6/string';
import 'core-js/fn/object/assign';
