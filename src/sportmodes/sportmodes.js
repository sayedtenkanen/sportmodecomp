/** @module SportModes */
import SportModeHeader from '../datamodels/sportmodeheader';
import ActivityHeader from '../datamodels/activityheader';
import PackageIds from '../../sim-tools/packageids/packageIds.json';
import Localization from '../localization/localization';

function isCustomMode(mode) {
  return typeof mode.CustomModePackageID === 'undefined';
}
/**
 * Returns currently active sportmodes
 * @param {String} watchSportModes
 * @param {String} language
 * @return {SportModeHeader[]} sportModeHeaders
 */
function list(watchSportModes, language) {
  const obj = JSON.parse(watchSportModes);
  return obj.map((watchMode) => {
    const isCustom = isCustomMode(watchMode);
    const modeIds = watchMode.Modes.map(mode => mode.ID);
    const packageId = watchMode.CustomModePackageID;

    // Only factory modes have packageId and activity type, i.e. 'Basic' or 'Interval'
    const activityTypes = packageId > 0 ?
      PackageIds.filter(item => item.customModePackageId === watchMode.CustomModePackageID) :
      [];

    const activityTypeName = activityTypes.length === 1 ?
      Localization.translate(activityTypes[0].nameId, language) :
      null;

    return new SportModeHeader(
      watchMode.ID,
      modeIds,
      isCustom ? watchMode.Name : null,
      !isCustom,
      new ActivityHeader(watchMode.ActivityID, activityTypeName)
    );
  });
}
/**
 * Returns true if user's sport mode list is full and mode needs to
 * be removed before new mode can be added.
 * @param {String} watchSportModes stringified
 * @param {Boolean} factory set to true if adding factory mode, false if
 * custom mode
 * @return {Boolean} true if list is full
 */
function isListFull(watchSportModes, factory) {
  const obj = JSON.parse(watchSportModes);

  // Factory mode can be always added
  if (factory) {
    return false;
  }

  // Custom mode can be added as long as there are no more
  // than 18 custom modes
  return obj.filter(mode => isCustomMode(mode)).length > 18;
}

/**
 * Returns unique mode ID.
 * @return {Integer} Available ID for new mode
 */
function newModeId(watchSportModes) {
  const obj = JSON.parse(watchSportModes);
  const currentIds = obj.reduce((modeList, watchMode) => {
    Array.prototype.push.apply(modeList, watchMode.Modes.map(mode => mode.ID));
    return modeList;
  }, []);

  // Start from first possible ID and find unique. Set Single Sport (b31) and
  // User Owned (b30) bits to avoid collision with factory modes
  const singleSportBit = 0x80000000;
  const userOwnedBit = 0x40000000;
  /* eslint no-bitwise: ["error", { "allow": ["|"] }] */
  let newId = singleSportBit | userOwnedBit | 0;
  while (currentIds.includes(newId)) {
    newId += 1;
  }
  return newId;
}

export default { list, isListFull, newModeId };
