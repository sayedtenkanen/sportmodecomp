const map =
{
  Data3Fields: {
    placeholder: 'displays/data/data_3fields_placeholder.png',
    values: 'displays/data/data_3fields_value.png',
  },
  Data3FieldsWearRound: {
    placeholder: 'displays/data/data_3_fields_wearround_placeholder.png',
    values: 'displays/data/data_3_fields_wearround_value.png',
  },
  Data4Fields: {
    placeholder: 'displays/data/data_4fields_placeholder.png',
    values: 'displays/data/data_4fields_value.png',
  },
  Data4FieldsWearRound: {
    placeholder: 'displays/data/data_4_fields_wearround_placeholder.png',
    values: 'displays/data/data_4_fields_wearround_value.png',
  },
  Data5Fields: {
    placeholder: 'displays/data/data_5fields_placeholder.png',
    values: 'displays/data/data_5fields_value.png',
  },
  Data5FieldsWearRound: {
    placeholder: 'displays/data/data_5_fields_wearround_placeholder.png',
    values: 'displays/data/data_5_fields_wearround_value.png',
  },
  Data7Fields: {
    placeholder: 'displays/data/data_7fields_placeholder.png',
    values: 'displays/data/data_7fields_value.png',
  },
  Data7FieldsWearRound: {
    placeholder: 'displays/data/data_7_fields_wearround_placeholder.png',
    values: 'displays/data/data_7_fields_wearround_value.png',
  },
  Table2Columns: {
    placeholder: 'displays/table/table_2columns_placeholder.png',
    values: 'displays/table/table_2columns_value.png',
  },
  Table3Columns: {
    placeholder: 'displays/table/table_3columns_placeholder.png',
    values: 'displays/table/table_3columns_value.png',
  },
  LapTable: {
    placeholder: 'displays/table/table_lap_placeholder.png',
    values: 'displays/table/table_lap_value.png',
  },
  LapTableWearRound: {
    placeholder: 'displays/table/table_lap_wearround_placeholder.png',
    values: 'displays/table/table_lap_wearround_value.png',
  },
  Data3Carousel: {
    placeholder: 'displays/data/data_3carousel_placeholder.png',
    values: 'displays/data/data_3carousel_value.png',
  },
  SimpleLineChart: {
    placeholder: 'displays/data/data_simplelinechart_placeholder.png',
    values: 'displays/data/data_simplelinechart_value.png',
  },
  SimpleLineChartWearRound: {
    placeholder: 'displays/data/data_simplelinechart_wearround_placeholder.png',
    values: 'displays/data/data_simplelinechart_wearround_value.png',
  },
  Zones8Fields: {
    placeholder: 'displays/zones8fields/zones8fields_placeholder.png',
    values: 'displays/zones8fields/zones8fields_value.png',
  },
  TXT_POWER_NO_DISTANCE_INTERVAL: {
    values: 'displays/interval/interval_power_no_distance.png',
  },
  TXT_SPEED: {
    values: 'displays/interval/interval_speed.png',
  },
  TXT_POWER: {
    values: 'displays/interval/interval_power.png',
  },
  TXT_DURATION_HR_INTERVAL: {
    values: 'displays/interval/interval_heartRate.png',
  },
  TXT_SPEED_PACE_INTERVAL: {
    values: 'displays/interval/interval_pace.png',
  },
  Navigation: {
    values: 'displays/navigation.png',
  },
  MapWearRound: {
    values: 'displays/navigation_wearround.png',
  },
};
export default map;
