# sportmodecomp

## General

Sport Mode Component (later referenced as "Component") that is used in
Suunto app (iOS, Android) to lessen platform specific logic. It's
written in JavaScript. Component takes care of parsing watch, variant
and version specific data and implements as straightforward as
possible API for UI. It shares the data formats with MDS.

Architecture with the Component is such that it takes the data blobs
received from MDS as-is and returns new modified data blob after app
calls some modification method on it. App should not attempt to read
or modify the blob directly but always use Component to modify or read
it. Application is responsible for the logic and implementation of
reading and writing blobs to/from watch using MDS.

## Making a release

Create release branch from master.

```
$ git checkout -b release/<major.minor, i.e. 1.2>
```

Update and commit release notes.

Tags should not be created manually but use `npm version` so that the
version number in package.json is automatically kept in sync. `npm
version` also creates git tag.

```
$ npm version <full three-part version number, i.e. 1.2.0>
```

The last digit of the version number should always be 0 for the first
release from a new release branch. It should only be incremented if
hotfix releases are made from existing release branches.

Push the release branch.

```
$ git push origin <branch name>
```

Create Pull Request and get approvals. When ready, push tag.

```
$ git push origin <tag name, i.e. 1.2.0>
```

Merge Pull Request.

## Usage

### Prerequisites

Following softwares(s) must be installed.

- [nodejs](https://nodejs.org/en/), v. 8.10.0 or newer

Component has Suunto Information Model as a dependency which needs to
be downloaded

```
$ git submodule update --init --recursive
```

*Optional:* If you want to test some custom SIM version, change it now

```
$ cd suunto-information-model/`
$ git checkout <your SIM  version>
```

Install node package dependencies

```
$ npm install
$ npm run init
```

Init runs necessary tools to generate needed files from SIM

### Running tests

```
$ npm run test
```

### Running lint

```
$ npm run lint
```

### Packaging bundle

Component is distributed as self-contained bundle `bundle.js`. Bundle
contains all the Component code and related SIM configuration and
related translations.

```
$ npm run package
```

Output bundle is located in `dist/bundle.js`

### Documentation  ###

APIs in code are documented with jsdoc compatible syntax.

```
$ npm run doc
```

Entry point for documentation is located in `out/index.html`

