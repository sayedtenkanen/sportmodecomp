import { expect } from 'chai';
import fs from 'fs';
import Localization from '../src/localization/localization';
import DisplayTemplates from '../suunto-information-model/Specifications/CustomModes/DisplayTemplates.json';
import IntervalTemplates from '../suunto-information-model/Specifications/CustomModes/IntervalTrainingDisplayDefaults.json';
import DisplayFields from '../suunto-information-model/Specifications/CustomModes/DisplayFieldList.json';

describe('Localization', () => {
  it('DisplayFields', () => {
    DisplayFields.forEach((field) => {
      const phraseId = field.ValuePhrase;
      const translation = Localization.translate(phraseId, 'en');
      expect(translation.length).to.be.a('number');
      expect(translation.length).to.be.above(0);
      expect(translation).to.not.deep.equal(phraseId);
    });
  });
  it('DisplayField GroupNames', () => {
    DisplayFields.forEach((field) => {
      if (field.GroupPhrases) {
        field.GroupPhrases.forEach((phraseId) => {
          const translation = Localization.translate(phraseId, 'en');
          expect(translation.length).to.be.a('number');
          expect(translation.length).to.be.above(0);
          expect(translation).to.not.deep.equal(phraseId);
        });
      }
    });
  });
  it('IntervalTemplates', () => {
    IntervalTemplates.forEach((template) => {
      const phraseId = template.PhraseId;
      const translation = Localization.translate(phraseId, 'en');
      expect(translation.length).to.be.a('number');
      expect(translation.length).to.be.above(0);
      expect(translation).to.not.deep.equal(phraseId);
    });
  });
  it('DisplayTemplates', () => {
    Object.keys(DisplayTemplates).forEach((type) => {
      const phraseId = DisplayTemplates[type].Name;
      const translation = Localization.translate(phraseId, 'en');
      expect(translation.length).to.be.a('number');
      expect(translation.length).to.be.above(0);
      // Some templates are not customizable (lap related and map) for
      // now so skip those in unittest to avoid need of having them
      // translated because the concepts or groupings are not yet
      // clear
      const excludes = [
        'TXT_MAP',
        'TXT_LAPS',
      ];
      if (!excludes.includes(phraseId)) {
        expect(translation).to.not.deep.equal(phraseId);
      }
    });
  });
  it('ModeTypes', (done) => {
    const simPath = 'suunto-information-model/Specifications/CustomModes/CustomModePackages';

    // Loop through factory modes and harvest all found activity types
    // like TXT_BASIC_MODE, TXT_FITNESS_MODE etc
    fs.readdir(simPath, (error, files) => {
      expect(error).to.be.null;

      function readModeType(file) {
        return new Promise((resolve, reject) => {
          fs.readFile(`${simPath}/${file}`, 'utf8', (fileError, content) => {
            if (fileError) {
              reject(fileError);
            } else {
              const obj = JSON.parse(content);
              // NameID is for example TXT_BASIC_MODE
              resolve(obj.CustomModeGroups[0].NameID);
            }
          });
        });
      }

      // There are two types files, i.e. "AerobicBasic.json" and
      // "AerobicBasicPackage.json". Filter latter out since former
      // has the required information.
      const promises = files
        .filter(file => !file.includes('Package'))
        .map(file => readModeType(file));

      Promise.all(promises).then((values) => {
        values.forEach((phraseId) => {
          const translation = Localization.translate(phraseId, 'en');
          expect(translation.length).to.be.a('number');
          expect(translation.length).to.be.above(0);
          expect(translation).to.not.deep.equal(phraseId);
        });
        done();
      });
    });
  });
});
