/* eslint-disable no-plusplus */
/* eslint no-bitwise: ["error", { "allow": ["|"] }] */
import { exec } from 'child_process';
import { expect } from 'chai';
import fs from 'fs';
import * as index from '../src/index';
import PackageJson from '../package.json';
import WatchSportModeGroup from '../src/datamodels/watchsportmode/group';
import WatchSportModeSettings from '../src/datamodels/watchsportmode/settings';
import WatchSportModeDisplays from '../src/datamodels/watchsportmode/displays';
import DisplayIconMap from '../src/displayiconmap';
import Packages from '../src/packages/packages';
import Setting from '../src/datamodels/setting';
import * as Displays from '../src/constants/displayIds';
import * as Fields from '../src/constants/fieldIds';
import * as SupportHelpers from './supportHelpers';

const Component = index.default;

describe('Component', () => {
  const testDataPath = 'test/data';
  describe('constructor', () => {
    describe('language', () => {
      it('en', () => {
        const component = new Component('', '', 'en');
        expect(component._language).to.be.eql('en');
      });
      it('fi', () => {
        const component = new Component('', '', 'fi');
        expect(component._language).to.be.eql('fi');
      });
      it('tr', () => {
        const component = new Component('', '', 'tr');
        expect(component._language).to.be.eql('tr');
      });
      it('unknown', () => {
        const component = new Component('', '', 'unknown');
        // Unknown language defaults to English
        expect(component._language).to.be.eql('en');
      });
    });
  });
  it('version', () => {
    // Class API
    const component = new Component('', '', '');
    expect(component.getVersion()).to.be.eql(PackageJson.version);

    // plain function API
    index.initialize();
    expect(index.getVersion()).to.be.eql(PackageJson.version);
  });
  it('getSimHash', (done) => {
    const component = new Component('', '', '');
    exec('cd ./suunto-information-model && git rev-parse --verify HEAD --short=12', (error, stdout, stderr) => {
      expect(error).to.be.eql(null);
      expect(stderr).to.be.eql('');
      expect(component.getSimHash()).to.be.eql(stdout.trim());
      done();
    });
  });
  describe('isDeviceSupported', () => {
    const supportedVariants = [
      'Amsterdam',
      'AmsterdamC',
      'Brighton',
      'BrightonC',
      'Cairo',
      'CairoC',
      'Forssa',
      'ForssaC',
      'Gdansk',
      'GdanskC',
      'Ibiza',
      'IbizaC',
      'Lima',
      'LimaC',
    ];
    describe('supported', () => {
      it('2.2.10', () => {
        supportedVariants.forEach((variant) => {
          const component = new Component(variant, '2.2.10', 'en');
          expect(component.isDeviceSupported()).to.be.eql(2);
        });
      });
      it('2.1.42', () => {
        supportedVariants.forEach((variant) => {
          const component = new Component(variant, '2.1.42', 'en');
          expect(component.isDeviceSupported()).to.be.eql(2);
        });
      });
    });
    describe('Helsinki', () => {
      const helsinkiVariants = [
        'Helsinki',
        'HelsinkiC',
      ];
      it('supported', () => {
        helsinkiVariants.forEach((variant) => {
          const component = new Component(variant, '2.3.16', 'en');
          expect(component.isDeviceSupported()).to.be.eql(2);
        });
      });
      it('supported but too old', () => {
        helsinkiVariants.forEach((variant) => {
          const component = new Component(variant, '2.2.10', 'en');
          expect(component.isDeviceSupported()).to.be.eql(1);
        });
      });
    });
    describe('Monza', () => {
      const monzaVariants = [
        'Monza',
        'MonzaC',
      ];
      it('supported', () => {
        monzaVariants.forEach((variant) => {
          const component = new Component(variant, '2.7.10', 'en');
          expect(component.isDeviceSupported()).to.be.eql(2);
        });
      });
      it('supported but too old', () => {
        monzaVariants.forEach((variant) => {
          const component = new Component(variant, '2.7.0', 'en');
          expect(component.isDeviceSupported()).to.be.eql(1);
        });
      });
    });
    describe('Oulu', () => {
      const ouluVariants = [
        'Oulu',
        'OuluC',
      ];
      it('supported', () => {
        ouluVariants.forEach((variant) => {
          const component = new Component(variant, '2.10.2', 'en');
          expect(component.isDeviceSupported()).to.be.eql(2);
        });
      });
      it('supported but too old', () => {
        ouluVariants.forEach((variant) => {
          const component = new Component(variant, '2.10.1', 'en');
          expect(component.isDeviceSupported()).to.be.eql(1);
        });
      });
    });
    describe('Kyoto', () => {
      const variants = [
        'Kyoto',
        'KyotoC',
      ];
      it('supported', () => {
        if (!SupportHelpers.simSupportsKyoto()) {
          return;
        }

        variants.forEach((variant) => {
          const component = new Component(variant, '1.122.0', 'en');
          expect(component.isDeviceSupported()).to.be.eql(2);
        });
      });
      it('supported but too old', () => {
        if (!SupportHelpers.simSupportsKyoto()) {
          return;
        }

        variants.forEach((variant) => {
          const component = new Component(variant, '1.121.0', 'en');
          expect(component.isDeviceSupported()).to.be.eql(1);
        });
      });
    });
    it('not supported', () => {
      const component = new Component('Ambit', '2.2.10', 'en');
      expect(component.isDeviceSupported()).to.be.eql(0);
    });
    it('supported but no name change', () => {
      supportedVariants.forEach((variant) => {
        const component = new Component(variant, '2.0.42', 'en');
        expect(component.isDeviceSupported()).to.be.eql(3);
      });
    });
    describe('too old', () => {
      it('2.2-series', () => {
        supportedVariants.forEach((variant) => {
          const component = new Component(variant, '2.2.8', 'en');
          expect(component.isDeviceSupported()).to.be.eql(1);
        });
      });
      it('2.1-series', () => {
        supportedVariants.forEach((variant) => {
          const component = new Component(variant, '2.1.40', 'en');
          expect(component.isDeviceSupported()).to.be.eql(1);
        });
      });
      it('2.0-series', () => {
        supportedVariants.forEach((variant) => {
          const component = new Component(variant, '2.0.40', 'en');
          expect(component.isDeviceSupported()).to.be.eql(1);
        });
      });
      it('1.0-series', () => {
        supportedVariants.forEach((variant) => {
          const component = new Component(variant, '1.0.0', 'en');
          expect(component.isDeviceSupported()).to.be.eql(1);
        });
      });
    });
  });
  it('getSportModes', () => {
    const component = new Component('Amsterdam', '1.9.22', 'fi');
    const modes = JSON.parse(component.getSportModes());

    // More thorough tests is in packages.test.js
    expect(modes.length).to.be.eql(33);
  });
  it('getCurrentSportModes', () => {
    const component = new Component('Amsterdam', '1.9.0', 'fi');
    const groups = fs.readFileSync(`${testDataPath}/groups.json`, 'utf8');
    const modes = JSON.parse(component.getCurrentSportModes(groups));

    // More thorough test is in sportmodes.test.js
    expect(modes.length).to.be.eql(2);
  });
  describe('getSportModeTemplate', () => {
    it('Running', () => {
      const component = new Component('Amsterdam', '1.9.0', 'fi');
      const groups = fs.readFileSync(`${testDataPath}/groups.json`, 'utf8');
      const template = JSON.parse(component.getSportModeTemplate(3, groups));
      expect(template.group.data.Settings.CustomModeGroups[0].ActivityID).to.be.eql(3);
      expect(template.group.id).to.be.eql(0xC0000000 | 1);
      expect(template.group.data.Settings.CustomModeGroups[0].CustomModeIDs[0])
        .to.be.eql(0xC0000000 | 1);
    });
    it('Invalid', () => {
      const component = new Component('Amsterdam', '1.9.0', 'fi');
      const groups = fs.readFileSync(`${testDataPath}/groups.json`, 'utf8');
      const template = component.getSportModeTemplate(99, groups);
      expect(template).to.be.eql(undefined);
    });
    it('Empty list', () => {
      // Special case which isn't normally possible, but in some error cases it might
      // happen that there isn't any modes in watch
      const component = new Component('Amsterdam', '1.9.0', 'fi');
      const template = JSON.parse(component.getSportModeTemplate(3, '[]'));
      expect(template.group.data.Settings.CustomModeGroups[0].ActivityID).to.be.eql(3);
      expect(template.group.id).to.be.eql(0xC0000000 | 0);
      expect(template.group.data.Settings.CustomModeGroups[0].CustomModeIDs[0])
        .to.be.eql(0xC0000000 | 0);
    });
    it('Kyoto', () => {
      // Test every template so that the fields can be mapped
      if (!SupportHelpers.simSupportsKyoto()) {
        return;
      }

      const component = new Component('Kyoto', '1.122.0', 'en');
      const sportModes = JSON.parse(component.getSportModes());

      sportModes.forEach((activity) => {
        const mode = JSON.parse(component.getSportModeTemplate(activity.id, '[]'));
        const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));

        for (let i = 0; i < displays.displays().length; ++i) {
          JSON.parse(component.getCurrentFields(JSON.stringify(displays), i))
            .forEach(field => expect(field.name).to.not.equal('(Invalid)'));
        }
      });
    });
  });
  describe('getCurrentDisplays', () => {
    it('With GPS', () => {
      const component = new Component('Amsterdam', '1.9.0', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const displays = JSON.parse(component.getCurrentDisplays(displayJson, settingsJson));

      expect(displays.length).to.be.eql(3);

      expect(displays[0].id).to.be.eql('Data4Fields');
      expect(displays[0].name).to.be.eql('4 Fields');
      expect(displays[0].readOnly).to.be.false;
      expect(displays[0].icon.values).to.be.equal('displays/data/data_4fields_value.png');
      expect(displays[0].icon.placeholder).to.be.equal('displays/data/data_4fields_placeholder.png');
      // Last non-interval display, cannot be removed
      expect(displays[0].deletable).to.be.false;
      expect(displays[0].replaceable).to.be.true;

      expect(displays[1].id).to.be.eql('Interval37');
      expect(displays[1].name).to.be.eql('Speed and pace');
      expect(displays[1].readOnly).to.be.true;
      expect(displays[1].icon.values).to.be.equal('displays/interval/interval_pace.png');
      expect(displays[1].icon.placeholder).to.be.undefined;
      expect(displays[1].deletable).to.be.true;
      expect(displays[1].replaceable).to.be.true;

      expect(displays[2].id).to.be.eql('Breadcrumb');
      expect(displays[2].name).to.be.eql('Breadcrumb');
      expect(displays[2].readOnly).to.be.true;
      expect(displays[2].icon.values).to.be.equal('displays/navigation.png');
      expect(displays[2].icon.placeholder).to.be.undefined;
      expect(displays[2].deletable).to.be.false;
      expect(displays[2].replaceable).to.be.false;
    });
    it('Helsinki', () => {
      const component = new Component('Helsinki', '1.9.0', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const displays = JSON.parse(component.getCurrentDisplays(displayJson, settingsJson));

      // Helsinki does not support navigation and therefore doesn't have
      // Breadcrumb display
      expect(displays.length).to.be.eql(2);

      expect(displays[0].id).to.be.eql('Data4Fields');
      expect(displays[1].id).to.be.eql('Interval37');
    });
    it('Without GPS', () => {
      const component = new Component('Amsterdam', '1.9.0', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const settingsObj = new WatchSportModeSettings(settingsJson);
      settingsObj.setGpsInterval(0);
      const displays = JSON
        .parse(component.getCurrentDisplays(displayJson, JSON.stringify(settingsObj)));

      expect(displays.length).to.be.eql(2);

      expect(displays[0].id).to.be.eql('Data4Fields');
      expect(displays[0].deletable).to.be.false;
      expect(displays[0].replaceable).to.be.true;
      expect(displays[1].id).to.be.eql('Interval37');
      expect(displays[1].deletable).to.be.true;
      expect(displays[1].replaceable).to.be.true;
    });
    it('With two non-interval displays', () => {
      const component = new Component('Amsterdam', '1.9.0', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displaysColumns.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const displays = JSON.parse(component.getCurrentDisplays(displayJson, settingsJson));

      expect(displays.length).to.be.eql(3);

      expect(displays[0].id).to.be.eql('Table2Columns');
      expect(displays[0].name).to.be.eql('2 Columns');
      expect(displays[0].readOnly).to.be.false;
      expect(displays[0].icon.values).to.be.equal('displays/table/table_2columns_value.png');
      expect(displays[0].icon.placeholder).to.be.equal('displays/table/table_2columns_placeholder.png');
      expect(displays[0].deletable).to.be.true;
      expect(displays[0].replaceable).to.be.true;

      expect(displays[1].id).to.be.eql('Table3Columns');
      expect(displays[1].name).to.be.eql('3 Columns');
      expect(displays[1].readOnly).to.be.false;
      expect(displays[1].icon.values).to.be.equal('displays/table/table_3columns_value.png');
      expect(displays[1].icon.placeholder).to.be.equal('displays/table/table_3columns_placeholder.png');
      expect(displays[1].deletable).to.be.true;
      expect(displays[1].replaceable).to.be.true;

      expect(displays[2].id).to.be.eql('Breadcrumb');
      expect(displays[2].name).to.be.eql('Breadcrumb');
      expect(displays[2].readOnly).to.be.true;
      expect(displays[2].icon.values).to.be.equal('displays/navigation.png');
      expect(displays[2].icon.placeholder).to.be.undefined;
      expect(displays[2].deletable).to.be.false;
      expect(displays[2].replaceable).to.be.false;
    });
    it('Last display', () => {
      const component = new Component('Amsterdam', '1.9.0', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays-without-intervals.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const displays = JSON.parse(component.getCurrentDisplays(displayJson, settingsJson));

      expect(displays.length).to.be.eql(2);

      expect(displays[0].id).to.be.eql('Data4Fields');
      expect(displays[0].name).to.be.eql('4 Fields');
      expect(displays[0].readOnly).to.be.false;
      expect(displays[0].icon.values).to.be.equal('displays/data/data_4fields_value.png');
      expect(displays[0].icon.placeholder).to.be.equal('displays/data/data_4fields_placeholder.png');
      // Last non-interval display, cannot be removed
      expect(displays[0].deletable).to.be.false;
      expect(displays[0].replaceable).to.be.true;

      expect(displays[1].id).to.be.eql('Breadcrumb');
    });
    it('SimpleLineChart', () => {
      if (!SupportHelpers.simSupportsGraphs()) {
        return;
      }

      const component = new Component('Amsterdam', '1.9.0', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays-graph.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const displays = JSON.parse(component.getCurrentDisplays(displayJson, settingsJson));

      expect(displays.length).to.be.eql(2);

      expect(displays[0].id).to.be.eql(Displays.SIMPLE_LINE_CHART);
      expect(displays[0].name).to.be.eql('Graph');
      expect(displays[0].readOnly).to.be.false;
      expect(displays[0].icon.values).to.be.equal('displays/data/data_simplelinechart_value.png');
      expect(displays[0].icon.placeholder).to.be.equal('displays/data/data_simplelinechart_placeholder.png');
      expect(displays[0].deletable).to.be.false;
      expect(displays[0].replaceable).to.be.true;

      expect(displays[1].id).to.be.eql('Breadcrumb');
    });
    describe('Wear', () => {
      it('With map', () => {
        if (!SupportHelpers.simSupportsKyoto()) {
          return;
        }

        const component = new Component('Kyoto', '1.122', 'en');
        const displayJson = fs.readFileSync(`${testDataPath}/displays-kyoto.json`, 'utf8');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
        const displays = JSON.parse(component.getCurrentDisplays(displayJson, settingsJson));

        expect(displays.length).to.be.eql(2);

        expect(displays[0].id).to.be.eql(Displays.DATA_4_FIELDS_WEAR_ROUND);
        expect(displays[1].id).to.be.eql('Breadcrumb');
        expect(displays[1].icon.values).to.be.equal('displays/navigation_wearround.png');
      });
      it('Without map', () => {
        if (!SupportHelpers.simSupportsKyoto()) {
          return;
        }

        const component = new Component('Kyoto', '1.122', 'en');
        const displayJson = fs.readFileSync(`${testDataPath}/displays-kyoto-without-map.json`, 'utf8');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
        const displays = JSON.parse(component.getCurrentDisplays(displayJson, settingsJson));

        expect(displays.length).to.be.eql(1);

        expect(displays[0].id).to.be.eql(Displays.DATA_4_FIELDS_WEAR_ROUND);
      });
    });
  });
  it('LapTable', () => {
    if (!SupportHelpers.simSupportsLapTable()) {
      return;
    }

    const component = new Component('Monza', '2.7.10', 'en');
    const displayJson = fs.readFileSync(`${testDataPath}/displays-laptable.json`, 'utf8');
    const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
    const displays = JSON.parse(component.getCurrentDisplays(displayJson, settingsJson));

    expect(displays.length).to.be.eql(2);

    expect(displays[0].id).to.be.eql(Displays.LAP_TABLE);
    expect(displays[0].name).to.be.eql('Lap table');
    expect(displays[0].readOnly).to.be.false;
    expect(displays[0].icon.values).to.be.equal('displays/table/table_lap_value.png');
    expect(displays[0].icon.placeholder).to.be.equal('displays/table/table_lap_placeholder.png');
    // Only display besides Breadcrumb, cannot be removed
    expect(displays[0].deletable).to.be.false;
    expect(displays[0].replaceable).to.be.true;

    expect(displays[1].id).to.be.eql('Breadcrumb');
  });
  describe('getCurrentFields', () => {
    function checkField(field, id, name) {
      expect(field.id).to.be.eql(id);
      expect(field.name).to.be.eql(name);
    }
    describe('Fields', () => {
      it('Amsterdam', () => {
        const component = new Component('Amsterdam', '2.0.0', 'en');
        const fieldsJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
        const fields = JSON.parse(component.getCurrentFields(fieldsJson, 0));

        expect(fields.length).to.be.eql(4);

        checkField(fields[0], 'TXT_ALTITUDE', 'Altitude');
        checkField(fields[1], 'TXT_HEART_RATE', 'Heart rate');
        checkField(fields[2], 'TXT_PACE', 'Pace');
        checkField(fields[3], 'TXT_DURATION', 'Duration');
      });
      describe('Kyoto', () => {
        it('Visible', () => {
          if (!SupportHelpers.simSupportsKyoto()) {
            return;
          }

          const component = new Component('Kyoto', '1.122', 'en');
          const fieldsJson = fs.readFileSync(`${testDataPath}/displays-kyoto.json`, 'utf8');
          const fields = JSON.parse(component.getCurrentFields(fieldsJson, 0));

          expect(fields.length).to.be.eql(3);

          checkField(fields[0], 'TXT_DISTANCE', 'Distance');
          checkField(fields[1], 'TXT_HEART_RATE', 'Heart rate');
          checkField(fields[2], 'TXT_PACE', 'Pace');
        });
        it('Hidden', () => {
          if (!SupportHelpers.simSupportsKyoto()) {
            return;
          }

          const component = new Component('Kyoto', '1.122', 'en');
          const fieldsJson = fs.readFileSync(`${testDataPath}/displays-kyoto.json`, 'utf8');
          const fields = JSON.parse(component.getCurrentFields(fieldsJson, 3));

          // Make sure component doesn't return data for hidden
          // display (UI index out-of-bounds, but data index is valid)
          expect(fields.length).to.be.eql(0);
        });
        it('No Fields', () => {
          if (!SupportHelpers.simSupportsKyoto()) {
            return;
          }

          const component = new Component('Kyoto', '1.122', 'en');
          const displays =
            JSON.parse(fs.readFileSync(`${testDataPath}/displays-kyoto.json`, 'utf8'));

          // Make sure component survives missing Field array for added robustness
          displays.Settings.CustomModes[0].Displays[0].Fields = undefined;

          const fields = JSON.parse(component.getCurrentFields(JSON.stringify(displays), 0));
          expect(fields.length).to.be.eql(0);
        });
      });
    });
    it('Columns', () => {
      const component = new Component('Amsterdam', '2.0.0', 'en');
      const fieldsJson = fs.readFileSync(`${testDataPath}/displaysColumns.json`, 'utf8');
      const fields2Columns = JSON.parse(component.getCurrentFields(fieldsJson, 0));

      expect(fields2Columns.length).to.be.eql(3);
      checkField(fields2Columns[0], 'TXT_DURATION', 'Duration');
      checkField(fields2Columns[1], 'TXT_LAP_AVG_SPEED', 'Lap avg. speed');
      checkField(fields2Columns[2], 'TXT_LAP_AVG_HEART_RATE', 'Lap avg. heart rate');

      const fields3Columns = JSON.parse(component.getCurrentFields(fieldsJson, 1));
      expect(fields3Columns.length).to.be.eql(4);
      checkField(fields3Columns[0], 'TXT_DURATION', 'Duration');
      checkField(fields3Columns[1], 'TXT_LAP_AVERAGE_PACE', 'Lap avg. pace');
      checkField(fields3Columns[2], 'TXT_LAP_AVG_HEART_RATE', 'Lap avg. heart rate');
      checkField(fields3Columns[3], 'TXT_LAP_AVG_CADENCE', 'Lap avg. cadence');
    });
    it('LapTable', () => {
      if (!SupportHelpers.simSupportsLapTable()) {
        return;
      }

      const component = new Component('MonzaC', '2.7.10', 'en');
      const fieldsJson = fs.readFileSync(`${testDataPath}/displays-laptable.json`, 'utf8');
      const laptable = JSON.parse(component.getCurrentFields(fieldsJson, 0));

      expect(laptable.length).to.be.eql(4);
      checkField(laptable[0], 'TXT_LAP_DISTANCE', 'Lap distance');
      checkField(laptable[1], 'TXT_LAP_DURATION', 'Lap duration');
      checkField(laptable[2], 'TXT_LAP_AVG_HEART_RATE', 'Lap avg. heart rate');
      checkField(laptable[3], 'TXT_DURATION', 'Duration');
    });
    it('SimpleLineChart', () => {
      if (!SupportHelpers.simSupportsGraphs()) {
        return;
      }

      const component = new Component('Amsterdam', '2.0.0', 'en');
      const fieldsJson = fs.readFileSync(`${testDataPath}/displays-graph.json`, 'utf8');
      const fields = JSON.parse(component.getCurrentFields(fieldsJson, 0));

      expect(fields.length).to.be.eql(3);

      checkField(fields[0], 'TXT_HEART_RATE', 'Heart rate');
      checkField(fields[1], 'TXT_HEART_RATE', 'Heart rate');
      checkField(fields[2], 'TXT_DURATION', 'Duration');
    });
    it('Zones8Fields', () => {
      if (!SupportHelpers.simSupportsZones()) {
        return;
      }

      const component = new Component('Amsterdam', '2.4.0', 'en');
      const fieldsJson = fs.readFileSync(`${testDataPath}/displays-zones.json`, 'utf8');
      const fields = JSON.parse(component.getCurrentFields(fieldsJson, 0));

      expect(fields.length).to.be.eql(2);

      checkField(fields[0], 'TXT_PACE', 'Pace');
      checkField(fields[1], 'TXT_DURATION', 'Duration');
    });
    it('Carousel', () => {
      if (!Packages.isCustomizationSupported('Helsinki')) {
        return;
      }
      const component = new Component('Helsinki', '2.4.0', 'en');
      const fieldsJson = fs.readFileSync(`${testDataPath}/displays-carousel.json`, 'utf8');
      const fields = JSON.parse(component.getCurrentFields(fieldsJson, 0));

      expect(fields.length).to.be.eql(5);

      checkField(fields[0], 'TXT_HEART_RATE', 'Heart rate');
      checkField(fields[1], 'TXT_DISTANCE', 'Distance');
      checkField(fields[2], 'TXT_PACE', 'Pace');
      checkField(fields[3], 'TXT_EMPTY', 'Empty');
      checkField(fields[4], 'TXT_EMPTY', 'Empty');
    });
    it('Out of bounds', () => {
      const component = new Component('Amsterdam', '2.0.0', 'en');
      const fieldsJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const fields = JSON.parse(component.getCurrentFields(fieldsJson, 2));

      expect(fields.length).to.be.eql(0);
    });
    describe('Interval', () => {
      const getOldBottomField = () => (
        {
          ID: 'Bottom',
          PathRoot: 'Activity',
          ValueWindow: 'Interval',
          ValueWindowIndex: -1,
          ValueResource: 'Duration',
          ValueAggregate: 'Current',
          ValueFormat: 'Duration',
          ValueFormatStyle: 'Training',
        }
      );
      it('Too old version', () => {
        const component = new Component('Amsterdam', '1.0.0', 'en');
        const displays = new WatchSportModeDisplays(fs
          .readFileSync(`${testDataPath}/displays.json`, 'utf8'));

        // Old SW uses different field with different phrase ID and translation,
        // make sure that the component can handle compatibility correctly
        displays.displays()[1].Fields[2] = getOldBottomField();
        const fields = JSON.parse(component.getCurrentFields(JSON.stringify(displays), 1));

        expect(fields.length).to.be.eql(3);

        // None of the fields can be matched since this version does not support
        // them according to SIM - make sure we return correctly in such error
        // case
        fields.forEach(field => checkField(field, 'TXT_INVALID_FIELD', '(Invalid)'));
      });
      it('<1.8.6', () => {
        const component = new Component('Amsterdam', '1.8.5', 'en');
        const displays = new WatchSportModeDisplays(fs
          .readFileSync(`${testDataPath}/displays.json`, 'utf8'));

        // Old SW uses different field with different phrase ID and translation,
        // make sure that the component can handle compatibility correctly
        displays.displays()[1].Fields[2] = getOldBottomField();
        const fields = JSON.parse(component.getCurrentFields(JSON.stringify(displays), 1));

        expect(fields.length).to.be.eql(3);

        // ESWs older than 1.8.6 use 'TXT_INTERVAL_DURATION'
        checkField(fields[2], 'TXT_INTERVAL_DURATION', 'Interval duration');
      });
      it('<1.9.22', () => {
        const component = new Component('Amsterdam', '1.9.21', 'en');
        const displays = new WatchSportModeDisplays(fs
          .readFileSync(`${testDataPath}/displays.json`, 'utf8'));

        displays.displays()[1].Fields[2] = getOldBottomField();
        const fields = JSON.parse(component.getCurrentFields(JSON.stringify(displays), 1));

        expect(fields.length).to.be.eql(3);

        // ESWs newer than 1.8.6 use 'TXT_CURRENT_INTERVAL_DURATION'
        checkField(fields[2], 'TXT_CURRENT_INTERVAL_DURATION', 'Current interval duration');
      });
      it('>= 1.9.22', () => {
        const component = new Component('Amsterdam', '1.9.22', 'en');
        const fieldsJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
        const fields = JSON.parse(component.getCurrentFields(fieldsJson, 1));

        expect(fields.length).to.be.eql(3);

        checkField(fields[0], 'TXT_CURRENT_INTERVAL_DISTANCE', 'Current interval distance');
        checkField(fields[1], 'TXT_PACE', 'Pace');
        checkField(fields[2], 'TXT_CURRENT_INTERVAL_DURATION', 'Current interval duration');
      });
      it('Nonsupported variant', () => {
        const component = new Component('Helsinki', '1.9.22', 'en');
        const fieldsJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
        const fields = JSON.parse(component.getCurrentFields(fieldsJson, 1));

        expect(fields.length).to.be.eql(3);

        // None of the fields can be matched since this variant does not support
        // them according to SIM - make sure we return correctly in such error
        // case
        fields.forEach(field => checkField(field, 'TXT_INVALID_FIELD', '(Invalid)'));
      });
    });
    it('Autolap', () => {
      const component = new Component('Amsterdam', '1.9.0', 'en');
      const fieldsJson = fs.readFileSync(`${testDataPath}/displays-laps.json`, 'utf8');
      const fieldsOne = JSON.parse(component.getCurrentFields(fieldsJson, 0));

      expect(fieldsOne.length).to.be.eql(4);

      checkField(fieldsOne[0], 'TXT_DURATION', 'Duration');
      checkField(fieldsOne[1], 'TXT_LAP_AVERAGE_PACE', 'Lap avg. pace');
      checkField(fieldsOne[2], 'TXT_LAP_AVG_CADENCE', 'Lap avg. cadence');
      checkField(fieldsOne[3], 'TXT_LAP_AVG_HEART_RATE', 'Lap avg. heart rate');
    });
  });
  describe('changeField', () => {
    describe('HeartRate', () => {
      it('Amsterdam', () => {
        const component = new Component('Amsterdam', '1.9.0', 'en');
        const fieldsJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
        const displays = new WatchSportModeDisplays(component.changeField(fieldsJson, 0, 0, 'TXT_HEART_RATE'));

        expect(displays.displays().length).to.be.eql(2);

        const fields = displays.displays()[0].Fields;

        expect(fields.length).to.be.eql(4);

        const modifiedField = fields[0];

        expect(Object.keys(modifiedField).length).to.be.eql(8);
        expect(modifiedField.ID).to.be.eql('TopLeft');
        expect(modifiedField.PathRoot).to.be.eql('Activity');
        expect(modifiedField.ValueWindow).to.be.eql('Move');
        expect(modifiedField.ValueWindowIndex).to.be.eql(-1);
        expect(modifiedField.ValueResource).to.be.eql('HeartRate');
        expect(modifiedField.ValueAggregate).to.be.eql('Current');
        expect(modifiedField.ValueFormatStyle).to.be.eql('Fourdigits');
        expect(modifiedField.ValueFormat).to.be.eql('HeartRate');
      });
      it('Kyoto', () => {
        if (!SupportHelpers.simSupportsKyoto()) {
          return;
        }

        const component = new Component('Kyoto', '1.122', 'en');
        const fieldsJson = fs.readFileSync(`${testDataPath}/displays-kyoto.json`, 'utf8');
        // Change Center field from Pace to Heart Rate. Note the
        // index. It's 2 instead of 3 because first field TopMini
        // isn't modifiable and is hence invisible from application
        // PoV
        const displays = new WatchSportModeDisplays(component.changeField(fieldsJson, 0, 2, 'TXT_HEART_RATE'));

        // 5 includes hidden displays
        expect(displays.displays().length).to.be.eql(5);

        const fields = displays.displays()[0].Fields;

        expect(fields.length).to.be.eql(5);

        const modifiedField = fields[3];

        expect(Object.keys(modifiedField).length).to.be.eql(8);
        expect(modifiedField.ID).to.be.eql('Center');
        expect(modifiedField.PathRoot).to.be.eql('Activity');
        expect(modifiedField.ValueWindow).to.be.eql('Move');
        expect(modifiedField.ValueWindowIndex).to.be.eql(-1);
        expect(modifiedField.ValueResource).to.be.eql('HeartRate');
        expect(modifiedField.ValueAggregate).to.be.eql('Current');
        expect(modifiedField.ValueFormatStyle).to.be.eql('Fivedigits');
        expect(modifiedField.ValueFormat).to.be.eql('HeartRate');
      });
    });
    it('Sunset', () => {
      const component = new Component('Amsterdam', '2.0.0', 'en');
      const fieldsJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const displays = new WatchSportModeDisplays(component.changeField(fieldsJson, 0, 0, 'TXT_SUNSET'));

      expect(displays.displays().length).to.be.eql(2);

      const fields = displays.displays()[0].Fields;

      expect(fields.length).to.be.eql(4);

      const modifiedField = fields[0];

      expect(Object.keys(modifiedField).length).to.be.eql(5);
      expect(modifiedField.ID).to.be.eql('TopLeft');
      expect(modifiedField.PathRoot).to.be.eql('Outdoor/Sunset');
      expect(modifiedField.ValueResource).to.be.eql('Next');
      expect(modifiedField.ValueFormatStyle).to.be.eql('Fourdigits');
      expect(modifiedField.ValueFormat).to.be.eql('Sunset');
    });
    describe('SimpleLineChart', () => {
      it('Cadence', () => {
        if (!SupportHelpers.simSupportsGraphs()) {
          return;
        }

        const component = new Component('Amsterdam', '2.0.0', 'en');
        const fieldsJson = fs.readFileSync(`${testDataPath}/displays-graph.json`, 'utf8');
        const displays = new WatchSportModeDisplays(component.changeField(fieldsJson, 0, 1, 'TXT_CADENCE'));

        expect(displays.displays().length).to.be.eql(1);

        const fields = displays.displays()[0].Fields;

        expect(fields.length).to.be.eql(3);

        const modifiedField = fields[1];

        expect(Object.keys(modifiedField).length).to.be.eql(15);
        expect(modifiedField.ID).to.be.eql(Fields.CHART);
        expect(modifiedField.PathRoot).to.be.eql('Activity');
        expect(modifiedField.SecondDimensionScale).to.be.eql(300);
        expect(modifiedField.SecondDimensionScaleToggle).to.be.eql(1800);
        expect(modifiedField.SecondDimensionScaleUnit).to.be.eql('Duration');
        expect(modifiedField.ValueAggregate).to.be.eql('Current');
        expect(modifiedField.ValueFormat).to.be.eql('Cadence');
        expect(modifiedField.ValueFormatStyle).to.be.eql('Fourdigits');
        expect(modifiedField.ValueResource).to.be.eql('Cadence');
        expect(modifiedField.ValueScaleImperialMax).to.be.closeTo(0.3333333333, 0.001);
        expect(modifiedField.ValueScaleImperialMin).to.be.eql(0);
        expect(modifiedField.ValueScaleMax).to.be.closeTo(0.3333333333, 0.001);
        expect(modifiedField.ValueScaleMin).to.be.eql(0);
        expect(modifiedField.ValueWindow).to.be.eql('Move');
        expect(modifiedField.ValueWindowIndex).to.be.eql(-1);
      });
    });
    describe('Zones8Fields', () => {
      it('Top', () => {
        if (!SupportHelpers.simSupportsZones()) {
          return;
        }

        const component = new Component('Amsterdam', '2.4.0', 'en');
        const fieldsJson = fs.readFileSync(`${testDataPath}/displays-zones.json`, 'utf8');
        const displays = new WatchSportModeDisplays(component.changeField(fieldsJson, 0, 0, 'TXT_TIMEOFDAY'));

        expect(displays.displays().length).to.be.eql(1);

        const fields = displays.displays()[0].Fields;
        expect(fields.length).to.be.eql(7);

        const find = (position => fields.find(field => field.ID === position));

        // Value which was changed
        const top = find(Fields.TOP);

        expect(Object.keys(top).length).to.be.eql(5);
        expect(top.PathRoot).to.be.eql('Dev/Time');
        expect(top.ValueFormat).to.be.eql('TimeOfDay');
        expect(top.ValueFormatStyle).to.be.eql('Fourdigits');
        expect(top.ValueResource).to.be.eql('LocalTime');

        // All "Top" values are kept in sync that the user always
        // sees the selected field regardless of which zones are
        // active
        const topSecond = find(Fields.TOP_SECOND);
        expect(Object.keys(topSecond).length).to.be.eql(5);
        expect(topSecond.PathRoot).to.be.eql('Dev/Time');
        expect(topSecond.ValueFormat).to.be.eql('TimeOfDay');
        expect(topSecond.ValueFormatStyle).to.be.eql('Fourdigits');
        expect(topSecond.ValueResource).to.be.eql('LocalTime');

        const topThird = find(Fields.TOP_THIRD);
        expect(Object.keys(topThird).length).to.be.eql(5);
        expect(topThird.PathRoot).to.be.eql('Dev/Time');
        expect(topThird.ValueFormat).to.be.eql('TimeOfDay');
        expect(topThird.ValueFormatStyle).to.be.eql('Fourdigits');
        expect(topThird.ValueResource).to.be.eql('LocalTime');
      });
      it('Bottom', () => {
        if (!SupportHelpers.simSupportsZones()) {
          return;
        }

        const component = new Component('Amsterdam', '2.4.0', 'en');
        const fieldsJson = fs.readFileSync(`${testDataPath}/displays-zones.json`, 'utf8');
        const displays = new WatchSportModeDisplays(component.changeField(fieldsJson, 0, 1, 'TXT_TIMEOFDAY'));

        expect(displays.displays().length).to.be.eql(1);

        const fields = displays.displays()[0].Fields;

        expect(fields.length).to.be.eql(7);

        const modifiedField = fields[6];

        expect(Object.keys(modifiedField).length).to.be.eql(5);
        expect(modifiedField.ID).to.be.eql(Fields.BOTTOM);
        expect(modifiedField.PathRoot).to.be.eql('Dev/Time');
        expect(modifiedField.ValueFormat).to.be.eql('TimeOfDay');
        expect(modifiedField.ValueFormatStyle).to.contains('digits');
        expect(modifiedField.ValueResource).to.be.eql('LocalTime');
      });
    });
    it('Columns', () => {
      const component = new Component('Amsterdam', '1.9.0', 'en');
      const fieldsJson = fs.readFileSync(`${testDataPath}/displaysColumns.json`, 'utf8');
      const displays = new WatchSportModeDisplays(component.changeField(fieldsJson, 0, 1, 'TXT_LAP_AVG_CADENCE'));
      expect(displays.displays().length).to.be.eql(2);

      const fields = displays.displays()[0].Fields;

      // Display contains unmodifiable field that is not visible to application.
      // Modified field is at index 2.
      expect(fields.length).to.be.eql(4);
      const modifiedField = fields[2];

      expect(Object.keys(modifiedField).length).to.be.eql(8);
      expect(modifiedField.ID).to.be.eql('LeftColumn');
      expect(modifiedField.PathRoot).to.be.eql('Activity');
      expect(modifiedField.ValueWindow).to.be.eql('Lap');
      expect(modifiedField.ValueWindowIndex).to.be.eql(-1);
      expect(modifiedField.ValueResource).to.be.eql('Cadence');
      expect(modifiedField.ValueAggregate).to.be.eql('Avg');
      expect(modifiedField.ValueFormatStyle).to.be.eql('Fourdigits');
      expect(modifiedField.ValueFormat).to.be.eql('Cadence');
    });
    it('LapTable', () => {
      if (!SupportHelpers.simSupportsLapTable()) {
        return;
      }

      const component = new Component('Monza', '2.7.10', 'en');
      const fieldsJson = fs.readFileSync(`${testDataPath}/displays-laptable.json`, 'utf8');
      const displays = new WatchSportModeDisplays(component.changeField(fieldsJson, 0, 0, 'TXT_LAP_AVG_CADENCE'));
      expect(displays.displays().length).to.be.eql(1);

      const fields = displays.displays()[0].Fields;

      expect(fields.length).to.be.eql(4);
      const modifiedField = fields[0];

      expect(Object.keys(modifiedField).length).to.be.eql(8);
      expect(modifiedField.ID).to.be.eql(Fields.LEFT_COLUMN);
      expect(modifiedField.PathRoot).to.be.eql('Activity');
      expect(modifiedField.ValueWindow).to.be.eql('Lap');
      expect(modifiedField.ValueWindowIndex).to.be.eql(-1);
      expect(modifiedField.ValueResource).to.be.eql('Cadence');
      expect(modifiedField.ValueAggregate).to.be.eql('Avg');
      expect(modifiedField.ValueFormatStyle).to.be.eql('Fourdigits');
      expect(modifiedField.ValueFormat).to.be.eql('Cadence');
    });
    describe('Interval', () => {
      it('< 1.9.22', () => {
        const component = new Component('Amsterdam', '1.9.21', 'en');
        const fieldsJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
        const displays = new WatchSportModeDisplays(component.changeField(
          fieldsJson,
          1,
          1,
          'TXT_CURRENT_INTERVAL_DURATION'
        ));

        expect(displays.displays().length).to.be.eql(2);

        const fields = displays.displays()[1].Fields;

        expect(fields.length).to.be.eql(10);

        const modifiedField = fields[1];

        expect(modifiedField.ID).to.be.eql('Center');
        expect(modifiedField.PathRoot).to.be.eql('Activity');
        expect(modifiedField.ValueWindow).to.be.eql('Interval');
        expect(modifiedField.ValueWindowIndex).to.be.eql(-1);
        expect(modifiedField.ValueResource).to.be.eql('Duration');
        expect(modifiedField.ValueAggregate).to.be.eql('Current');
        expect(modifiedField.ValueFormat).to.be.eql('Duration');
      });
      it('>= 1.9.22', () => {
        const component = new Component('Amsterdam', '1.9.22', 'en');
        const fieldsJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
        const displays = new WatchSportModeDisplays(component.changeField(
          fieldsJson,
          1,
          1,
          'TXT_CURRENT_INTERVAL_DURATION'
        ));

        expect(displays.displays().length).to.be.eql(2);

        const fields = displays.displays()[1].Fields;

        expect(fields.length).to.be.eql(10);

        const modifiedField = fields[1];

        expect(modifiedField.ID).to.be.eql('Center');
        expect(modifiedField.PathRoot).to.be.eql('Activity/Exercise/Interval/CountDown/Duration');
        expect(modifiedField.ValueFormat).to.be.eql('Duration');
      });
    });
    it('Data3Carousel', () => {
      if (!Packages.isCustomizationSupported('Helsinki')) {
        return;
      }

      const component = new Component('Helsinki', '2.4.0', 'en');
      const fieldsJson = fs.readFileSync(`${testDataPath}/displays-carousel.json`, 'utf8');
      const displays = new WatchSportModeDisplays(component.changeField(fieldsJson, 0, 0, 'TXT_SPEED'));

      expect(displays.displays().length).to.be.eql(2);
      const fields = displays.displays()[0].Fields;

      // Display contains unmodifiable fields (top and bottom) which are not visible to application.
      // Modified field is at index 1.
      expect(fields.length).to.be.eql(7);

      expect(Object.keys(fields[1]).length).to.be.eql(8);
      expect(fields[1].ID).to.be.eql(Fields.CAR_FIRST);
      expect(fields[1].PathRoot).to.be.eql('Activity');
      expect(fields[1].ValueWindow).to.be.eql('Move');
      expect(fields[1].ValueWindowIndex).to.be.eql(-1);
      expect(fields[1].ValueResource).to.be.eql('Speed');
      expect(fields[1].ValueAggregate).to.be.eql('Current');
      expect(fields[1].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(fields[1].ValueFormat).to.be.eql('Speed');

      // Check first field
      expect(Object.keys(fields[0]).length).to.be.eql(5);
      expect(fields[0].ID).to.be.eql(Fields.TOP);
      expect(fields[0].PathRoot).to.be.eql('Dev/Time');
      expect(fields[0].ValueResource).to.be.eql('LocalTime');
      expect(fields[0].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(fields[0].ValueFormat).to.be.eql('TimeOfDay');

      // Modify last modifiable field
      const displays2 = new WatchSportModeDisplays(component.changeField(fieldsJson, 0, 4, 'TXT_DISTANCE'));
      const fields2 = displays2.displays()[0].Fields;
      expect(fields2.length).to.be.eql(7);

      expect(Object.keys(fields2[5]).length).to.be.eql(8);
      expect(fields2[5].ID).to.be.eql(Fields.CAR_FIFTH);
      expect(fields2[5].PathRoot).to.be.eql('Activity');
      expect(fields2[5].ValueWindow).to.be.eql('Move');
      expect(fields2[5].ValueWindowIndex).to.be.eql(-1);
      expect(fields2[5].ValueResource).to.be.eql('Distance');
      expect(fields2[5].ValueAggregate).to.be.eql('Current');
      expect(fields2[5].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(fields2[5].ValueFormat).to.be.eql('Distance');

      // Check last field
      expect(fields2[6].ID).to.be.eql(Fields.BOTTOM);
      expect(fields2[6].PathRoot).to.be.eql('Activity');
      expect(fields2[6].ValueWindow).to.be.eql('Move');
      expect(fields2[6].ValueWindowIndex).to.be.eql(-1);
      expect(fields2[6].ValueResource).to.be.eql('Duration');
      expect(fields2[6].ValueAggregate).to.be.eql('Current');
      expect(fields2[6].ValueFormatStyle).to.be.eql('Training');
      expect(fields2[6].ValueFormat).to.be.eql('Duration');
    });
  });
  describe('getDisplays', () => {
    it('mode without interval display', () => {
      const component = new Component('Amsterdam', '1.9.0', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displaysColumns.json`, 'utf8');
      const displaySections = JSON.parse(component.getDisplays(displayJson, 3, 0));

      expect(displaySections.length).to.be.eql(3);

      function checkDisplay(disp, id, name, readOnly) {
        expect(disp.id).to.be.eql(id);
        expect(disp.name).to.be.eql(name);
        expect(disp.readOnly).to.equal(readOnly);
      }

      expect(displaySections[0].displays.length).to.be.eql(4);
      checkDisplay(displaySections[0].displays[0], 'Data3Fields', '3 Fields', false);
      checkDisplay(displaySections[0].displays[1], 'Data4Fields', '4 Fields', false);
      checkDisplay(displaySections[0].displays[2], 'Data5Fields', '5 Fields', false);
      checkDisplay(displaySections[0].displays[3], 'Data7Fields', '7 Fields', false);
      expect(displaySections[1].displays.length).to.be.eql(2);
      checkDisplay(displaySections[1].displays[0], 'Table2Columns', '2 Columns', false);
      checkDisplay(displaySections[1].displays[1], 'Table3Columns', '3 Columns', false);
      expect(displaySections[2].displays.length).to.be.eql(5);
      expect(displaySections[2].description).contains('The interval display is activated');
      checkDisplay(displaySections[2].displays[0], 'TXT_DURATION_HR_INTERVAL', 'Duration with heart rate', true);
      checkDisplay(displaySections[2].displays[1], 'TXT_POWER', 'Power', true);
      checkDisplay(displaySections[2].displays[2], 'TXT_POWER_NO_DISTANCE_INTERVAL', 'Power without distance', true);
      checkDisplay(displaySections[2].displays[3], 'TXT_SPEED', 'Speed', true);
      checkDisplay(displaySections[2].displays[4], 'TXT_SPEED_PACE_INTERVAL', 'Speed and pace', true);

      displaySections[0].displays.forEach((display) => {
        expect(display.icon.values).to.be.equal(DisplayIconMap[display.id].values);
        expect(display.icon.placeholder).to.be.equal(DisplayIconMap[display.id].placeholder);
      });

      displaySections[1].displays.forEach((display) => {
        expect(display.icon.values).to.be.equal(DisplayIconMap[display.id].values);
        expect(display.icon.placeholder).to.be.equal(DisplayIconMap[display.id].placeholder);
      });

      displaySections[2].displays.forEach((display) => {
        expect(display.icon.values).to.be.equal(DisplayIconMap[display.id].values);
        expect(display.icon.placeholder).to.be.equal(DisplayIconMap[display.id].placeholder);
      });
    });
    it('one non-interval display', () => {
      const component = new Component('Amsterdam', '1.9.0', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays-without-intervals.json`, 'utf8');
      const displaySections = JSON.parse(component.getDisplays(displayJson, 3, 0));

      expect(displaySections.length).to.be.eql(2);

      function checkDisplay(disp, id, name, readOnly) {
        expect(disp.id).to.be.eql(id);
        expect(disp.name).to.be.eql(name);
        expect(disp.readOnly).to.equal(readOnly);
      }

      expect(displaySections[0].displays.length).to.be.eql(4);
      checkDisplay(displaySections[0].displays[0], 'Data3Fields', '3 Fields', false);
      checkDisplay(displaySections[0].displays[1], 'Data4Fields', '4 Fields', false);
      checkDisplay(displaySections[0].displays[2], 'Data5Fields', '5 Fields', false);
      checkDisplay(displaySections[0].displays[3], 'Data7Fields', '7 Fields', false);
      expect(displaySections[1].displays.length).to.be.eql(2);
      checkDisplay(displaySections[1].displays[0], 'Table2Columns', '2 Columns', false);
      checkDisplay(displaySections[1].displays[1], 'Table3Columns', '3 Columns', false);

      // Interval displays are missing, since user shouldn't be able to change their
      // last display to interval one as there should always exist one non-interval display
    });
    it('mode with interval display', () => {
      const component = new Component('Amsterdam', '1.9.0', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const displaySections = JSON.parse(component.getDisplays(displayJson, 3, 0));

      expect(displaySections.length).to.be.eql(2);

      expect(displaySections[0].displays.length).to.be.eql(4);
      expect(displaySections[0].displays[0].id).to.be.eql('Data3Fields');
      expect(displaySections[0].displays[1].id).to.be.eql('Data4Fields');
      expect(displaySections[0].displays[2].id).to.be.eql('Data5Fields');
      expect(displaySections[0].displays[3].id).to.be.eql('Data7Fields');

      expect(displaySections[1].displays.length).to.be.eql(2);
      expect(displaySections[1].displays[0].id).to.be.eql('Table2Columns');
      expect(displaySections[1].displays[1].id).to.be.eql('Table3Columns');
    });
    it('replace interval display', () => {
      const component = new Component('Amsterdam', '1.9.0', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const displaySections = JSON.parse(component.getDisplays(displayJson, 3, 1));

      expect(displaySections.length).to.be.eql(3);

      expect(displaySections[0].displays.length).to.be.eql(4);
      expect(displaySections[0].displays[0].id).to.be.eql('Data3Fields');
      expect(displaySections[0].displays[1].id).to.be.eql('Data4Fields');
      expect(displaySections[0].displays[2].id).to.be.eql('Data5Fields');
      expect(displaySections[0].displays[3].id).to.be.eql('Data7Fields');

      expect(displaySections[1].displays.length).to.be.eql(2);
      expect(displaySections[1].displays[0].id).to.be.eql('Table2Columns');
      expect(displaySections[1].displays[1].id).to.be.eql('Table3Columns');

      expect(displaySections[2].displays.length).to.be.eql(5);
      expect(displaySections[2].displays[0].id).to.be.eql('TXT_DURATION_HR_INTERVAL');
      expect(displaySections[2].displays[1].id).to.be.eql('TXT_POWER');
      expect(displaySections[2].displays[2].id).to.be.eql('TXT_POWER_NO_DISTANCE_INTERVAL');
      expect(displaySections[2].displays[3].id).to.be.eql('TXT_SPEED');
      expect(displaySections[2].displays[4].id).to.be.eql('TXT_SPEED_PACE_INTERVAL');
    });
    it('zone display', () => {
      if (!SupportHelpers.simSupportsZones()) {
        return;
      }

      const component = new Component('Brighton', '2.3.16', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const displaySections = JSON.parse(component.getDisplays(displayJson, 3, 0));

      expect(displaySections.length).to.be.eql(3);

      expect(displaySections[0].displays.length).to.be.eql(5);
      expect(displaySections[0].displays[0].id).to.be.eql('Data3Fields');
      expect(displaySections[0].displays[1].id).to.be.eql('Data4Fields');
      expect(displaySections[0].displays[2].id).to.be.eql('Data5Fields');
      expect(displaySections[0].displays[3].id).to.be.eql('Data7Fields');
      expect(displaySections[0].displays[4].id).to.be.eql('SimpleLineChart');

      expect(displaySections[1].displays.length).to.be.eql(2);
      expect(displaySections[1].displays[0].id).to.be.eql('Table2Columns');
      expect(displaySections[1].displays[1].id).to.be.eql('Table3Columns');

      expect(displaySections[2].displays.length).to.be.eql(1);
      expect(displaySections[2].name).to.be.eql('Intensity zones');
      expect(displaySections[2].displays[0].id).to.be.eql(Displays.ZONES_8_FIELDS);
      expect(displaySections[2].displays[0].name).to.be.eql('Intensity zones');
    });
    it('Monza', () => {
      if (!SupportHelpers.simSupportsLapTable()) {
        return;
      }

      const component = new Component('Monza', '2.7.10', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const displaySections = JSON.parse(component.getDisplays(displayJson, 3, 1));

      expect(displaySections.length).to.be.eql(3);

      expect(displaySections[0].displays.length).to.be.eql(6);
      expect(displaySections[0].displays[0].id).to.be.eql(Displays.DATA_3_FIELDS);
      expect(displaySections[0].displays[1].id).to.be.eql(Displays.DATA_4_FIELDS);
      expect(displaySections[0].displays[2].id).to.be.eql(Displays.DATA_5_FIELDS);
      expect(displaySections[0].displays[3].id).to.be.eql(Displays.DATA_7_FIELDS);
      expect(displaySections[0].displays[4].id).to.be.eql(Displays.DATA_3_CAROUSEL);
      expect(displaySections[0].displays[5].id).to.be.eql(Displays.SIMPLE_LINE_CHART);

      expect(displaySections[1].displays.length).to.be.eql(1);
      expect(displaySections[1].displays[0].id).to.be.eql(Displays.LAP_TABLE);

      expect(displaySections[2].displays.length).to.be.eql(5);
      expect(displaySections[2].displays[0].id).to.be.eql('TXT_DURATION_HR_INTERVAL');
      expect(displaySections[2].displays[1].id).to.be.eql('TXT_POWER');
      expect(displaySections[2].displays[2].id).to.be.eql('TXT_POWER_NO_DISTANCE_INTERVAL');
      expect(displaySections[2].displays[3].id).to.be.eql('TXT_SPEED');
      expect(displaySections[2].displays[4].id).to.be.eql('TXT_SPEED_PACE_INTERVAL');
    });
    it('Kyoto', () => {
      if (!SupportHelpers.simSupportsKyoto()) {
        return;
      }

      const component = new Component('Kyoto', '1.122', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays-kyoto.json`, 'utf8');
      const displaySections = JSON.parse(component.getDisplays(displayJson, 3, 0));

      expect(displaySections[0].displays.length).to.be.eql(5);
      expect(displaySections[0].displays[0].id).to.be.eql(Displays.DATA_3_FIELDS_WEAR_ROUND);
      expect(displaySections[0].displays[1].id).to.be.eql(Displays.DATA_4_FIELDS_WEAR_ROUND);
      expect(displaySections[0].displays[2].id).to.be.eql(Displays.DATA_5_FIELDS_WEAR_ROUND);
      expect(displaySections[0].displays[3].id).to.be.eql(Displays.DATA_7_FIELDS_WEAR_ROUND);
      expect(displaySections[0].displays[4].id).to.be.eql(Displays.SIMPLE_LINE_CHART_WEAR_ROUND);

      if (displaySections.length === 2) {
        expect(displaySections[1].displays.length).to.be.eql(1);
        expect(displaySections[1].displays[0].id).to.be.eql(Displays.LAP_TABLE_WEAR_ROUND);
      } else {
        expect(displaySections.length).to.be.eql(1);
      }
    });
  });
  describe('changeDisplay', () => {
    describe('Data4Fields to Data5Fields', () => {
      it('Amsterdam', () => {
        const component = new Component('Amsterdam', '1.9.0', 'fi');
        const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

        const mode = JSON.parse(component.changeDisplay(displayJson, settingsJson, 0, 'Data5Fields'));
        expect(mode.modes.length).to.be.eql(1);

        const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
        const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

        expect(displays.displays().length).to.be.eql(2);

        const templates = displays.templates();

        expect(templates.length).to.be.eql(2);
        expect(templates[0]).to.be.eql('Data5Fields');
        expect(templates[1]).to.be.eql('Interval37');

        const dataFiveFields = displays.displays()[0].Fields;

        expect(dataFiveFields.length).to.be.eql(5);

        // Preserved
        expect(Object.keys(dataFiveFields[0]).length).to.be.eql(8);
        expect(dataFiveFields[0].ID).to.be.eql('TopLeft');
        expect(dataFiveFields[0].PathRoot).to.be.eql('Activity');
        expect(dataFiveFields[0].ValueAggregate).to.be.eql('Current');
        expect(dataFiveFields[0].ValueFormat).to.be.eql('Altitude');
        expect(dataFiveFields[0].ValueFormatStyle).to.be.eql('Fourdigits');
        expect(dataFiveFields[0].ValueResource).to.be.eql('Altitude');
        expect(dataFiveFields[0].ValueWindow).to.be.eql('Move');
        expect(dataFiveFields[0].ValueWindowIndex).to.be.eql(-1);

        // Preserved
        expect(Object.keys(dataFiveFields[1]).length).to.be.eql(8);
        expect(dataFiveFields[1].ID).to.be.eql('TopRight');
        expect(dataFiveFields[1].PathRoot).to.be.eql('Activity');
        expect(dataFiveFields[1].ValueAggregate).to.be.eql('Current');
        expect(dataFiveFields[1].ValueFormat).to.be.eql('HeartRate');
        expect(dataFiveFields[1].ValueFormatStyle).to.be.eql('Fourdigits');
        expect(dataFiveFields[1].ValueResource).to.be.eql('HeartRate');
        expect(dataFiveFields[1].ValueWindow).to.be.eql('Move');
        expect(dataFiveFields[1].ValueWindowIndex).to.be.eql(-1);

        // Preserved
        expect(Object.keys(dataFiveFields[2]).length).to.be.eql(8);
        expect(dataFiveFields[2].ID).to.be.eql('CenterLeft');
        expect(dataFiveFields[2].PathRoot).to.be.eql('Activity');
        expect(dataFiveFields[2].ValueAggregate).to.be.eql('Current');
        expect(dataFiveFields[2].ValueFormat).to.be.eql('Pace');
        expect(dataFiveFields[2].ValueFormatStyle).to.be.eql('Fourdigits');
        expect(dataFiveFields[2].ValueResource).to.be.eql('Speed');
        expect(dataFiveFields[2].ValueWindow).to.be.eql('Move');
        expect(dataFiveFields[2].ValueWindowIndex).to.be.eql(-1);

        // Preserved
        expect(Object.keys(dataFiveFields[3]).length).to.be.eql(8);
        expect(dataFiveFields[3].ID).to.be.eql('CenterRight');
        expect(dataFiveFields[3].PathRoot).to.be.eql('Activity');
        expect(dataFiveFields[3].ValueAggregate).to.be.eql('Current');
        expect(dataFiveFields[3].ValueFormat).to.be.eql('Duration');
        expect(dataFiveFields[3].ValueFormatStyle).to.be.eql('Fourdigits');
        expect(dataFiveFields[3].ValueResource).to.be.eql('Duration');
        expect(dataFiveFields[3].ValueWindow).to.be.eql('Move');
        expect(dataFiveFields[3].ValueWindowIndex).to.be.eql(-1);

        // Default
        expect(Object.keys(dataFiveFields[4]).length).to.be.eql(8);
        expect(dataFiveFields[4].ID).to.be.eql('Bottom');
        expect(dataFiveFields[4].PathRoot).to.be.eql('Activity');
        expect(dataFiveFields[4].ValueAggregate).to.be.eql('Current');
        expect(dataFiveFields[4].ValueFormat).to.be.eql('Duration');
        expect(dataFiveFields[4].ValueFormatStyle).contains('digits');
        expect(dataFiveFields[4].ValueResource).to.be.eql('Duration');
        expect(dataFiveFields[4].ValueWindow).to.be.eql('Move');
        expect(dataFiveFields[4].ValueWindowIndex).to.be.eql(-1);

        // Other display is interval type and should not have been changed
        expect(settings.getHasIntervalDisplay()).to.be.true;

        // Check that the mapping to field names work
        JSON.parse(component.getCurrentFields(JSON.stringify(displays), 0))
          .forEach(field => expect(field.name).to.not.equal('(Invalid)'));

        // Settings should be untouched so that hash matches for optimization purposes
        expect(JSON.stringify(JSON.parse(settingsJson)))
          .to.be.equal(JSON.stringify(mode.modes[0].settings));
      });
      describe('Kyoto', () => {
        it('Data5FieldsWearRound', () => {
          if (!SupportHelpers.simSupportsKyoto()) {
            return;
          }

          const component = new Component('Kyoto', '1.122', 'en');
          const displayJson = fs.readFileSync(`${testDataPath}/displays-kyoto.json`, 'utf8');
          const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

          const mode = JSON.parse(component.changeDisplay(displayJson, settingsJson, 0, 'Data5FieldsWearRound'));
          expect(mode.modes.length).to.be.eql(1);

          const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
          const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

          // 5 includes hidden displays
          expect(displays.displays().length).to.be.eql(5);

          const templates = displays.templates();

          expect(templates.length).to.be.eql(5);
          expect(templates[0]).to.be.eql('Data5FieldsWearRound');

          const dataFiveFields = displays.displays()[0].Fields;

          expect(dataFiveFields.length).to.be.eql(6);

          // Always Dev/Time
          expect(Object.keys(dataFiveFields[0]).length).to.be.eql(5);
          expect(dataFiveFields[0].ID).to.be.eql('TopMini');
          expect(dataFiveFields[0].PathRoot).to.be.eql('Dev/Time');
          expect(dataFiveFields[0].ValueFormat).to.be.eql('TimeOfDay');
          expect(dataFiveFields[0].ValueFormatStyle).to.be.eql('Fourdigits');
          expect(dataFiveFields[0].ValueResource).to.be.eql('LocalTime');

          // Preserved
          expect(Object.keys(dataFiveFields[1]).length).to.be.eql(8);
          expect(dataFiveFields[1].ID).to.be.eql('TopLeft');
          expect(dataFiveFields[1].PathRoot).to.be.eql('Activity');
          expect(dataFiveFields[1].ValueAggregate).to.be.eql('Current');
          expect(dataFiveFields[1].ValueFormat).to.be.eql('Distance');
          expect(dataFiveFields[1].ValueFormatStyle).to.be.eql('Fourdigits');
          expect(dataFiveFields[1].ValueResource).to.be.eql('Distance');
          expect(dataFiveFields[1].ValueWindow).to.be.eql('Move');
          expect(dataFiveFields[1].ValueWindowIndex).to.be.eql(-1);

          // Preserved
          expect(Object.keys(dataFiveFields[2]).length).to.be.eql(8);
          expect(dataFiveFields[2].ID).to.be.eql('TopRight');
          expect(dataFiveFields[2].PathRoot).to.be.eql('Activity');
          expect(dataFiveFields[2].ValueAggregate).to.be.eql('Current');
          expect(dataFiveFields[2].ValueFormat).to.be.eql('HeartRate');
          expect(dataFiveFields[2].ValueFormatStyle).to.be.eql('Fourdigits');
          expect(dataFiveFields[2].ValueResource).to.be.eql('HeartRate');
          expect(dataFiveFields[2].ValueWindow).to.be.eql('Move');
          expect(dataFiveFields[2].ValueWindowIndex).to.be.eql(-1);

          // Preserved
          expect(Object.keys(dataFiveFields[3]).length).to.be.eql(8);
          expect(dataFiveFields[3].ID).to.be.eql('CenterLeft');
          expect(dataFiveFields[3].PathRoot).to.be.eql('Activity');
          expect(dataFiveFields[3].ValueAggregate).to.be.eql('Current');
          expect(dataFiveFields[3].ValueFormat).to.be.eql('Pace');
          expect(dataFiveFields[3].ValueFormatStyle).to.be.eql('Fourdigits');
          expect(dataFiveFields[3].ValueResource).to.be.eql('Speed');
          expect(dataFiveFields[3].ValueWindow).to.be.eql('Move');
          expect(dataFiveFields[3].ValueWindowIndex).to.be.eql(-1);

          // Preserved
          expect(Object.keys(dataFiveFields[4]).length).to.be.eql(8);
          expect(dataFiveFields[4].ID).to.be.eql('CenterRight');
          expect(dataFiveFields[4].PathRoot).to.be.eql('Activity');
          expect(dataFiveFields[4].ValueAggregate).to.be.eql('Current');
          expect(dataFiveFields[4].ValueFormat).to.be.eql('Duration');
          expect(dataFiveFields[4].ValueFormatStyle).to.be.eql('Fourdigits');
          expect(dataFiveFields[4].ValueResource).to.be.eql('Duration');
          expect(dataFiveFields[4].ValueWindow).to.be.eql('Move');
          expect(dataFiveFields[4].ValueWindowIndex).to.be.eql(-1);

          // Always Duration
          expect(Object.keys(dataFiveFields[5]).length).to.be.eql(8);
          expect(dataFiveFields[5].ID).to.be.eql('Bottom');
          expect(dataFiveFields[5].PathRoot).to.be.eql('Activity');
          expect(dataFiveFields[5].ValueAggregate).to.be.eql('Current');
          expect(dataFiveFields[5].ValueFormat).to.be.eql('Duration');
          expect(dataFiveFields[5].ValueFormatStyle).to.be.equal('Training');
          expect(dataFiveFields[5].ValueResource).to.be.eql('Duration');
          expect(dataFiveFields[5].ValueWindow).to.be.eql('Move');
          expect(dataFiveFields[5].ValueWindowIndex).to.be.eql(-1);

          // Has not interval display (sanity check only)
          expect(settings.getHasIntervalDisplay()).to.be.false;

          // Check that the mapping to field names work
          JSON.parse(component.getCurrentFields(JSON.stringify(displays), 0))
            .forEach(field => expect(field.name).to.not.equal('(Invalid)'));

          // Also check that getCurrentDisplays can correctly map new display
          const currentDisplays = JSON.parse(component
            .getCurrentDisplays(JSON.stringify(displays), JSON.stringify(settings)));

          expect(currentDisplays.length).to.be.eql(2);
          expect(currentDisplays[0].id).to.be.eql('Data5FieldsWearRound');
          expect(currentDisplays[1].id).to.be.eql('Breadcrumb');
          expect(currentDisplays[1].icon.values).to.be.eql('displays/navigation_wearround.png');
        });
        it('LapTableWearRound', () => {
          if (!SupportHelpers.simSupportsKyoto()) {
            return;
          }

          const component = new Component('Kyoto', '1.122', 'en');
          const displayJson = fs.readFileSync(`${testDataPath}/displays-kyoto.json`, 'utf8');
          const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

          const mode = JSON.parse(component.changeDisplay(displayJson, settingsJson, 0, 'LapTableWearRound'));
          expect(mode.modes.length).to.be.eql(1);

          const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));

          // 5 includes hidden displays
          expect(displays.displays().length).to.be.eql(5);

          const templates = displays.templates();

          expect(templates.length).to.be.eql(5);
          expect(templates[0]).to.be.eql('LapTableWearRound');

          const fields = JSON.parse(component.getCurrentFields(JSON.stringify(displays), 0));
          expect(fields.length).to.be.eql(3);

          expect(fields[0].id).to.be.eql('TXT_LAP_DURATION');
          expect(fields[1].id).to.be.eql('TXT_LAP_MAX_HEART_RATE');
          expect(fields[2].id).to.be.eql('TXT_LAP_AVG_HEART_RATE');
        });
      });
    });
    it('Data4Fields to Table2Columns', () => {
      const component = new Component('Amsterdam', '1.9.0', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const mode = JSON.parse(component.changeDisplay(displayJson, settingsJson, 0, 'Table2Columns'));
      expect(mode.modes.length).to.be.eql(1);

      const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

      expect(displays.displays().length).to.be.eql(2);

      const templates = displays.templates();

      expect(templates.length).to.be.eql(2);
      expect(templates[0]).to.be.eql('Table2Columns');
      expect(templates[1]).to.be.eql('Interval37');

      const columns = displays.displays()[0].Fields;

      expect(columns.length).to.be.eql(4);

      // Default (no merge for any fields in column displays)
      expect(Object.keys(columns[0]).length).to.be.eql(8);
      expect(columns[0].ID).to.be.eql('Top');
      expect(columns[0].PathRoot).to.be.eql('Activity');
      expect(columns[0].ValueAggregate).to.be.eql('Current');
      expect(columns[0].ValueFormat).to.be.eql('Duration');
      expect(columns[0].ValueFormatStyle).contains('digits');
      expect(columns[0].ValueResource).to.be.eql('Duration');
      expect(columns[0].ValueWindow).to.be.eql('Move');
      expect(columns[0].ValueWindowIndex).to.be.eql(-1);

      expect(Object.keys(columns[1]).length).to.be.eql(7);
      expect(columns[1].ID).to.be.eql('IndexColumn');
      expect(columns[1].PathRoot).to.be.eql('Activity');
      expect(columns[1].ValueFormat).to.be.eql('Count');
      expect(columns[1].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(columns[1].ValueResource).to.be.eql('Index');
      expect(columns[1].ValueWindow).to.be.eql('Lap');
      expect(columns[1].ValueWindowIndex).to.be.eql(-1);

      expect(Object.keys(columns[2]).length).to.be.eql(8);
      expect(columns[2].ID).to.be.eql('LeftColumn');
      expect(columns[2].PathRoot).to.be.eql('Activity');
      expect(columns[2].ValueAggregate).to.be.eql('Current');
      expect(columns[2].ValueFormat).to.be.eql('Duration');
      expect(columns[2].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(columns[2].ValueResource).to.be.eql('Duration');
      expect(columns[2].ValueWindow).to.be.eql('Lap');
      expect(columns[2].ValueWindowIndex).to.be.eql(-1);

      expect(Object.keys(columns[3]).length).to.be.eql(8);
      expect(columns[3].ID).to.be.eql('RightColumn');
      expect(columns[3].PathRoot).to.be.eql('Activity');
      expect(columns[3].ValueAggregate).to.be.eql('Avg');
      expect(columns[3].ValueFormat).to.be.eql('HeartRate');
      expect(columns[3].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(columns[3].ValueResource).to.be.eql('HeartRate');
      expect(columns[3].ValueWindow).to.be.eql('Lap');
      expect(columns[3].ValueWindowIndex).to.be.eql(-1);

      // Other display is interval type and should not have been changed
      expect(settings.getHasIntervalDisplay()).to.be.true;

      // Check that the mapping to field names work
      JSON.parse(component.getCurrentFields(JSON.stringify(displays), 0))
        .forEach(field => expect(field.name).to.not.equal('(Invalid)'));

      // Settings should be untouched so that hash matches for optimization purposes
      expect(JSON.stringify(JSON.parse(settingsJson)))
        .to.be.equal(JSON.stringify(mode.modes[0].settings));
    });
    it('Data4Fields to LapTable', () => {
      if (!SupportHelpers.simSupportsLapTable()) {
        return;
      }

      const component = new Component('Monza', '2.7.10', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const mode = JSON.parse(component
        .changeDisplay(displayJson, settingsJson, 0, Displays.LAP_TABLE));
      expect(mode.modes.length).to.be.eql(1);

      const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

      expect(displays.displays().length).to.be.eql(2);

      const templates = displays.templates();

      expect(templates.length).to.be.eql(2);
      expect(templates[0]).to.be.eql(Displays.LAP_TABLE);
      expect(templates[1]).to.be.eql(Displays.INTERVAL_37);

      const columns = displays.displays()[0].Fields;

      expect(columns.length).to.be.eql(4);

      // Default (no merge for any fields in column displays)
      expect(Object.keys(columns[0]).length).to.be.eql(8);
      expect(columns[0].ID).to.be.eql(Fields.LEFT_COLUMN);
      expect(columns[0].PathRoot).to.be.eql('Activity');
      expect(columns[0].ValueAggregate).to.be.eql('Current');
      expect(columns[0].ValueFormat).to.be.eql('Duration');
      expect(columns[0].ValueFormatStyle).contains('digits');
      expect(columns[0].ValueResource).to.be.eql('Duration');
      expect(columns[0].ValueWindow).to.be.eql('Lap');
      expect(columns[0].ValueWindowIndex).to.be.eql(-1);

      expect(Object.keys(columns[1]).length).to.be.eql(8);
      expect(columns[1].ID).to.be.eql(Fields.CENTER_COLUMN);
      expect(columns[1].PathRoot).to.be.eql('Activity');
      expect(columns[1].ValueFormat).to.be.eql('Duration');
      expect(columns[1].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(columns[1].ValueResource).to.be.eql('Duration');
      expect(columns[1].ValueWindow).to.be.eql('Lap');
      expect(columns[1].ValueWindowIndex).to.be.eql(-1);

      expect(Object.keys(columns[2]).length).to.be.eql(8);
      expect(columns[2].ID).to.be.eql(Fields.RIGHT_COLUMN);
      expect(columns[2].PathRoot).to.be.eql('Activity');
      expect(columns[2].ValueAggregate).to.be.eql('Avg');
      expect(columns[2].ValueFormat).to.be.eql('HeartRate');
      expect(columns[2].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(columns[2].ValueResource).to.be.eql('HeartRate');
      expect(columns[2].ValueWindow).to.be.eql('Lap');
      expect(columns[2].ValueWindowIndex).to.be.eql(-1);

      expect(Object.keys(columns[3]).length).to.be.eql(8);
      expect(columns[3].ID).to.be.eql(Fields.BOTTOM);
      expect(columns[3].PathRoot).to.be.eql('Activity');
      expect(columns[3].ValueAggregate).to.be.eql('Current');
      expect(columns[3].ValueFormat).to.be.eql('Duration');
      expect(columns[3].ValueFormatStyle).to.be.eql('Fivedigits');
      expect(columns[3].ValueResource).to.be.eql('Duration');
      expect(columns[3].ValueWindow).to.be.eql('Move');
      expect(columns[3].ValueWindowIndex).to.be.eql(-1);

      expect(settings.getHasIntervalDisplay()).to.be.true;

      // Check that the mapping to field names work
      JSON.parse(component.getCurrentFields(JSON.stringify(displays), 0))
        .forEach(field => expect(field.name).to.not.equal('(Invalid)'));

      // Settings should be untouched so that hash matches for optimization purposes
      expect(JSON.stringify(JSON.parse(settingsJson)))
        .to.be.equal(JSON.stringify(mode.modes[0].settings));
    });
    it('Interval37 to Data3Fields', () => {
      const component = new Component('Amsterdam', '1.9.0', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const mode = JSON.parse(component.changeDisplay(displayJson, settingsJson, 1, 'Data3Fields'));
      expect(mode.modes.length).to.be.eql(1);

      const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

      expect(displays.displays().length).to.be.eql(2);

      const templates = displays.templates();

      expect(templates.length).to.be.eql(2);
      expect(templates[0]).to.be.eql('Data4Fields');
      expect(templates[1]).to.be.eql('Data3Fields');

      const dataThreeFields = displays.displays()[1].Fields;

      expect(dataThreeFields.length).to.be.eql(3);

      // Default
      expect(Object.keys(dataThreeFields[0]).length).to.be.eql(8);
      expect(dataThreeFields[0].ID).to.be.eql('Top');
      expect(dataThreeFields[0].PathRoot).to.be.eql('Activity');
      expect(dataThreeFields[0].ValueAggregate).to.be.eql('Current');
      expect(dataThreeFields[0].ValueFormat).to.be.eql('Duration');
      expect(dataThreeFields[0].ValueFormatStyle).contains('digits');
      expect(dataThreeFields[0].ValueResource).to.be.eql('Duration');
      expect(dataThreeFields[0].ValueWindow).to.be.eql('Move');
      expect(dataThreeFields[0].ValueWindowIndex).to.be.eql(-1);

      // Default
      expect(Object.keys(dataThreeFields[1]).length).to.be.eql(8);
      expect(dataThreeFields[1].ID).to.be.eql('Center');
      expect(dataThreeFields[1].PathRoot).to.be.eql('Activity');
      expect(dataThreeFields[1].ValueAggregate).to.be.eql('Current');
      expect(dataThreeFields[1].ValueFormat).to.be.eql('Duration');
      expect(dataThreeFields[1].ValueFormatStyle).to.be.eql('Fivedigits');
      expect(dataThreeFields[1].ValueResource).to.be.eql('Duration');
      expect(dataThreeFields[1].ValueWindow).to.be.eql('Move');
      expect(dataThreeFields[1].ValueWindowIndex).to.be.eql(-1);

      // Default
      expect(Object.keys(dataThreeFields[2]).length).to.be.eql(8);
      expect(dataThreeFields[2].ID).to.be.eql('Bottom');
      expect(dataThreeFields[2].PathRoot).to.be.eql('Activity');
      expect(dataThreeFields[2].ValueAggregate).to.be.eql('Current');
      expect(dataThreeFields[2].ValueFormat).to.be.eql('Duration');
      expect(dataThreeFields[2].ValueFormatStyle).contains('digits');
      expect(dataThreeFields[2].ValueResource).to.be.eql('Duration');
      expect(dataThreeFields[2].ValueWindow).to.be.eql('Move');
      expect(dataThreeFields[2].ValueWindowIndex).to.be.eql(-1);

      // No more interval displays
      expect(settings.getHasIntervalDisplay()).to.be.false;

      // Check that the mapping to field names work
      JSON.parse(component.getCurrentFields(JSON.stringify(displays), 0))
        .forEach(field => expect(field.name).to.not.equal('(Invalid)'));

      // Add interval display back
      const modesWithInterval = JSON.parse(component
        .changeDisplay(JSON.stringify(displays), JSON.stringify(settings), 1, 'TXT_SPEED'));

      // Again interval displays enabled in settings
      const settingsWithInterval = new WatchSportModeSettings(JSON
        .stringify(modesWithInterval.modes[0].settings));
      expect(settingsWithInterval.getHasIntervalDisplay()).to.be.true;

      // Also check that getCurrentDisplays can correctly map new display
      const currentDisplays = JSON.parse(component
        .getCurrentDisplays(JSON.stringify(displays), JSON.stringify(settings)));

      expect(currentDisplays.length).to.be.eql(3);
      expect(currentDisplays[0].id).to.be.eql('Data4Fields');
      expect(currentDisplays[1].id).to.be.eql('Data3Fields');
      expect(currentDisplays[2].id).to.be.eql('Breadcrumb');
      expect(currentDisplays[2].icon.values).to.be.eql('displays/navigation.png');
    });
    it('LapTable to Data3Fields', () => {
      if (!SupportHelpers.simSupportsLapTable()) {
        return;
      }

      const component = new Component('Monza', '2.7.10', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays-laptable.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const mode = JSON.parse(component
        .changeDisplay(displayJson, settingsJson, 0, Displays.DATA_3_FIELDS));
      expect(mode.modes.length).to.be.eql(1);

      const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

      expect(displays.displays().length).to.be.eql(1);

      const templates = displays.templates();

      expect(templates.length).to.be.eql(1);
      expect(templates[0]).to.be.eql(Displays.DATA_3_FIELDS);

      const dataThreeFields = displays.displays()[0].Fields;

      expect(dataThreeFields.length).to.be.eql(3);

      // Still no interval display
      expect(settings.getHasIntervalDisplay()).to.be.false;

      const newFields = JSON.parse(component.getCurrentFields(JSON.stringify(displays), 0));

      expect(newFields.length).to.be.eql(3);

      // Values from LapTable are not preserved but static default value is used
      expect(newFields[0].name).to.be.eql('Duration');
      expect(newFields[1].name).to.be.eql('Duration');
      expect(newFields[2].name).to.be.eql('Duration');
    });
    it('Data4Fields to TXT_SPEED (Interval77)', () => {
      const component = new Component('Amsterdam', '1.9.0', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays-without-intervals.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const mode = JSON.parse(component.changeDisplay(displayJson, settingsJson, 0, 'TXT_SPEED'));
      expect(mode.modes.length).to.be.eql(1);

      const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

      expect(displays.displays().length).to.be.eql(1);

      const templates = displays.templates();

      expect(templates.length).to.be.eql(1);
      expect(templates[0]).to.be.eql('Interval77');

      const intervalFields = displays.displays()[0].Fields;

      expect(intervalFields.length).to.be.eql(14);

      // Sanity check first and last, this data is straight from SIM
      expect(Object.keys(intervalFields[0]).length).to.be.eql(8);
      expect(intervalFields[0].ID).to.be.eql('1stLeft');
      expect(intervalFields[0].PathRoot).to.be.eql('Activity');
      expect(intervalFields[0].ValueAggregate).to.be.eql('Current');
      expect(intervalFields[0].ValueFormat).to.be.eql('Distance');
      expect(intervalFields[0].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(intervalFields[0].ValueResource).to.be.eql('Distance');
      expect(intervalFields[0].ValueWindow).to.be.eql('Interval');
      expect(intervalFields[0].ValueWindowIndex).to.be.eql(-1);

      expect(Object.keys(intervalFields[6]).length).to.be.eql(8);
      expect(intervalFields[6].ID).to.be.eql('Bottom');
      expect(intervalFields[6].PathRoot).to.be.eql('Activity');
      expect(intervalFields[6].ValueAggregate).to.be.eql('Current');
      expect(intervalFields[6].ValueFormat).to.be.eql('Duration');
      expect(intervalFields[6].ValueFormatStyle).to.be.eql('Training');
      expect(intervalFields[6].ValueResource).to.be.eql('Duration');
      expect(intervalFields[6].ValueWindow).to.be.eql('Interval');
      expect(intervalFields[6].ValueWindowIndex).to.be.eql(-1);

      expect(settings.getHasIntervalDisplay()).to.be.true;

      // Check that the mapping to field names work
      JSON.parse(component.getCurrentFields(JSON.stringify(displays), 0))
        .forEach(field => expect(field.name).to.not.equal('(Invalid)'));

      // Also check that getCurrentDisplays can correctly map new display
      const currentDisplays = JSON.parse(component
        .getCurrentDisplays(JSON.stringify(displays), JSON.stringify(settings)));

      expect(currentDisplays.length).to.be.eql(2);
      expect(currentDisplays[0].id).to.be.eql('Interval77');
      expect(currentDisplays[1].id).to.be.eql('Breadcrumb');
    });
    it('Data4Fields to TXT_DURATION_HR_INTERVAL (Interval37)', () => {
      const component = new Component('Amsterdam', '1.9.0', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays-without-intervals.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const mode = JSON.parse(component.changeDisplay(displayJson, settingsJson, 0, 'TXT_DURATION_HR_INTERVAL'));
      expect(mode.modes.length).to.be.eql(1);

      const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

      expect(displays.displays().length).to.be.eql(1);

      const templates = displays.templates();

      expect(templates.length).to.be.eql(1);
      expect(templates[0]).to.be.eql('Interval37');

      const intervalFields = displays.displays()[0].Fields;

      expect(intervalFields.length).to.be.eql(10);

      // Sanity check first and last, this data is straight from SIM
      expect(Object.keys(intervalFields[0]).length).to.be.eql(3);
      expect(intervalFields[0].ID).to.be.eql('Top');
      expect(intervalFields[0].PathRoot).to.be.eql('Activity/Exercise/Interval');
      expect(intervalFields[0].ValueResource).to.be.eql('CurrentRepetition');

      expect(Object.keys(intervalFields[2]).length).to.be.eql(8);
      expect(intervalFields[2].ID).to.be.eql('Bottom');
      expect(intervalFields[2].PathRoot).to.be.eql('Activity');
      expect(intervalFields[2].ValueAggregate).to.be.eql('Current');
      expect(intervalFields[2].ValueFormat).to.be.eql('Duration');
      expect(intervalFields[2].ValueFormatStyle).to.be.eql('Training');
      expect(intervalFields[2].ValueResource).to.be.eql('Duration');
      expect(intervalFields[2].ValueWindow).to.be.eql('Interval');
      expect(intervalFields[2].ValueWindowIndex).to.be.eql(-1);

      expect(settings.getHasIntervalDisplay()).to.be.true;

      // Check that the mapping to field names work
      JSON.parse(component.getCurrentFields(JSON.stringify(displays), 0))
        .forEach(field => expect(field.name).to.not.equal('(Invalid)'));

      // Also check that getCurrentDisplays can correctly map new display
      const currentDisplays = JSON.parse(component
        .getCurrentDisplays(JSON.stringify(displays), JSON.stringify(settings)));

      expect(currentDisplays.length).to.be.eql(2);
      expect(currentDisplays[0].id).to.be.eql('Interval37');
      expect(currentDisplays[1].id).to.be.eql('Breadcrumb');
    });
    describe('Data4Fields to SimpleLineChart', () => {
      it('Amsterdam', () => {
        if (!SupportHelpers.simSupportsGraphs()) {
          return;
        }

        const component = new Component('Amsterdam', '1.9.0', 'fi');
        const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

        const mode =
              JSON.parse(component
                .changeDisplay(displayJson, settingsJson, 0, Displays.SIMPLE_LINE_CHART));
        expect(mode.modes.length).to.be.eql(1);

        const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
        const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

        expect(displays.displays().length).to.be.eql(2);

        const templates = displays.templates();

        expect(templates.length).to.be.eql(2);
        expect(templates[0]).to.be.eql(Displays.SIMPLE_LINE_CHART);
        expect(templates[1]).to.be.eql('Interval37');

        const graphFields = displays.displays()[0].Fields;

        expect(graphFields.length).to.be.eql(3);

        // Default - chart display is not merged
        expect(Object.keys(graphFields[0]).length).to.be.eql(8);
        expect(graphFields[0].ID).to.be.eql('Top');
        expect(graphFields[0].PathRoot).to.be.eql('Activity');
        expect(graphFields[0].ValueAggregate).to.be.eql('Current');
        expect(graphFields[0].ValueFormat).to.be.eql('HeartRate');
        expect(graphFields[0].ValueFormatStyle).to.be.eql('Fourdigits');
        expect(graphFields[0].ValueResource).to.be.eql('HeartRate');
        expect(graphFields[0].ValueWindow).to.be.eql('Move');
        expect(graphFields[0].ValueWindowIndex).to.be.eql(-1);

        // Default
        expect(Object.keys(graphFields[1]).length).to.be.eql(15);
        expect(graphFields[1].ID).to.be.eql(Fields.CHART);
        expect(graphFields[1].PathRoot).to.be.eql('Activity');
        expect(graphFields[1].SecondDimensionScale).to.be.eql(300);
        expect(graphFields[1].SecondDimensionScaleToggle).to.be.eql(1800);
        expect(graphFields[1].SecondDimensionScaleUnit).to.be.eql('Duration');
        expect(graphFields[1].ValueAggregate).to.be.eql('Current');
        expect(graphFields[1].ValueFormat).to.be.eql('HeartRate');
        expect(graphFields[1].ValueFormatStyle).to.be.eql('Fourdigits');
        expect(graphFields[1].ValueResource).to.be.eql('HeartRate');
        expect(graphFields[1].ValueScaleImperialMax).to.be.closeTo(1.3333333730697632, 0.001);
        expect(graphFields[1].ValueScaleImperialMin).to.be.eql(1);
        expect(graphFields[1].ValueScaleMax).to.be.closeTo(1.3333333730697632, 0.001);
        expect(graphFields[1].ValueScaleMin).to.be.eql(1);
        expect(graphFields[1].ValueWindow).to.be.eql('Move');
        expect(graphFields[1].ValueWindowIndex).to.be.eql(-1);

        // Default
        expect(Object.keys(graphFields[2]).length).to.be.eql(8);
        expect(graphFields[2].ID).to.be.eql(Fields.BOTTOM);
        expect(graphFields[2].PathRoot).to.be.eql('Activity');
        expect(graphFields[2].ValueAggregate).to.be.eql('Current');
        expect(graphFields[2].ValueFormat).to.be.eql('Duration');
        expect(graphFields[2].ValueFormatStyle).contains('digits');
        expect(graphFields[2].ValueResource).to.be.eql('Duration');
        expect(graphFields[2].ValueWindow).to.be.eql('Move');
        expect(graphFields[2].ValueWindowIndex).to.be.eql(-1);

        // Other display is interval type and should not have been changed
        expect(settings.getHasIntervalDisplay()).to.be.true;

        // Check that the mapping to field names work
        JSON.parse(component.getCurrentFields(JSON.stringify(displays), 0))
          .forEach(field => expect(field.name).to.not.equal('(Invalid)'));

        // Settings should be untouched so that hash matches for optimization purposes
        expect(JSON.stringify(JSON.parse(settingsJson)))
          .to.be.equal(JSON.stringify(mode.modes[0].settings));
      });
      it('Kyoto', () => {
        if (!SupportHelpers.simSupportsKyoto()) {
          return;
        }

        const component = new Component('Kyoto', '1.122', 'en');
        const displayJson = fs.readFileSync(`${testDataPath}/displays-kyoto.json`, 'utf8');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

        const mode =
              JSON.parse(component
                .changeDisplay(
                  displayJson,
                  settingsJson, 0,
                  Displays.SIMPLE_LINE_CHART_WEAR_ROUND
                ));
        expect(mode.modes.length).to.be.eql(1);

        const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));

        // 5 includes hidden displays
        expect(displays.displays().length).to.be.eql(5);

        const templates = displays.templates();

        expect(templates.length).to.be.eql(5);
        expect(templates[0]).to.be.eql(Displays.SIMPLE_LINE_CHART_WEAR_ROUND);

        const graphFields = displays.displays()[0].Fields;

        expect(graphFields.length).to.be.eql(4);

        // Default - chart display is not merged
        expect(Object.keys(graphFields[0]).length).to.be.eql(5);
        expect(graphFields[0].ID).to.be.eql('TopMini');
        expect(graphFields[0].PathRoot).to.be.eql('Dev/Time');
        expect(graphFields[0].ValueFormat).to.be.eql('TimeOfDay');
        expect(graphFields[0].ValueFormatStyle).to.be.eql('Fourdigits');
        expect(graphFields[0].ValueResource).to.be.eql('LocalTime');

        // Default
        expect(Object.keys(graphFields[1]).length).to.be.eql(8);
        expect(graphFields[1].ID).to.be.eql('Top');
        expect(graphFields[1].PathRoot).to.be.eql('Activity');
        expect(graphFields[1].ValueAggregate).to.be.eql('Current');
        expect(graphFields[1].ValueFormat).to.be.eql('HeartRate');
        expect(graphFields[1].ValueFormatStyle).to.be.eql('Fivedigits');
        expect(graphFields[1].ValueResource).to.be.eql('HeartRate');
        expect(graphFields[1].ValueWindow).to.be.eql('Move');
        expect(graphFields[1].ValueWindowIndex).to.be.eql(-1);

        // Default
        expect(Object.keys(graphFields[2]).length).to.be.eql(12);
        expect(graphFields[2].ID).to.be.eql(Fields.CHART);
        expect(graphFields[2].PathRoot).to.be.eql('Sensor/HeartRate');
        expect(graphFields[2].SecondDimensionScale).to.be.eql(600);
        expect(graphFields[2].SecondDimensionScaleToggle).to.be.eql(1800);
        expect(graphFields[2].SecondDimensionScaleUnit).to.be.eql('Duration');
        expect(graphFields[2].ValueFormat).to.be.eql('HeartRateBpm');
        expect(graphFields[2].ValueFormatStyle).to.be.eql('Fourdigits');
        expect(graphFields[2].ValueResource).to.be.eql('com.google.heart_rate.Bpm.bpm');
        expect(graphFields[2].ValueScaleImperialMax).to.be.eql(80);
        expect(graphFields[2].ValueScaleImperialMin).to.be.eql(60);
        expect(graphFields[2].ValueScaleMax).to.be.eql(80);
        expect(graphFields[2].ValueScaleMin).to.be.eql(60);

        // Default
        expect(Object.keys(graphFields[3]).length).to.be.eql(8);
        expect(graphFields[3].ID).to.be.eql(Fields.BOTTOM);
        expect(graphFields[3].PathRoot).to.be.eql('Activity');
        expect(graphFields[3].ValueAggregate).to.be.eql('Current');
        expect(graphFields[3].ValueFormat).to.be.eql('Duration');
        expect(graphFields[3].ValueFormatStyle).to.be.equal('Training');
        expect(graphFields[3].ValueResource).to.be.eql('Duration');
        expect(graphFields[3].ValueWindow).to.be.eql('Move');
        expect(graphFields[3].ValueWindowIndex).to.be.eql(-1);

        // Check that the mapping to field names work
        const fieldNameCheck = () => {
          JSON.parse(component.getCurrentFields(JSON.stringify(displays), 0))
            .forEach(field => expect(field.name).to.not.equal('(Invalid)'));
        };

        fieldNameCheck();

        // ESW software cannot handle periods in its internal enums, so make sure
        // Component recognizes also all underscore ValueResources
        graphFields[2].ValueResource = 'com_google_heart_rate_Bpm_bpm';
        fieldNameCheck();
      });
    });
    it('Data4Fields to Data3Carousel', () => {
      if (!Packages.displayFieldProperties(Displays.DATA_3_CAROUSEL)) {
        // Older SIM does not support default fields
        return;
      }

      const component = new Component('Monza', '2.7.10', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const mode =
        JSON.parse(component
          .changeDisplay(displayJson, settingsJson, 0, Displays.DATA_3_CAROUSEL));
      expect(mode.modes.length).to.be.eql(1);

      const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

      expect(displays.displays().length).to.be.eql(2);

      const templates = displays.templates();

      expect(templates.length).to.be.eql(2);
      expect(templates[0]).to.be.eql(Displays.DATA_3_CAROUSEL);
      expect(templates[1]).to.be.eql('Interval37');

      const carouselFields = displays.displays()[0].Fields;

      expect(carouselFields.length).to.be.eql(7);

      // Default - TOP field is "Static" and hence not merged
      expect(Object.keys(carouselFields[0]).length).to.be.eql(5);
      expect(carouselFields[0].ID).to.be.eql(Fields.TOP);
      expect(carouselFields[0].PathRoot).to.be.eql('Dev/Time');
      expect(carouselFields[0].ValueFormat).to.be.eql('TimeOfDay');
      expect(carouselFields[0].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(carouselFields[0].ValueResource).to.be.eql('LocalTime');

      // Merged
      expect(carouselFields[1].ID).to.be.eql(Fields.CAR_FIRST);
      expect(carouselFields[1].ValueResource).to.be.eql('HeartRate');

      // Merged
      expect(carouselFields[2].ID).to.be.eql(Fields.CAR_SECOND);
      expect(carouselFields[2].ValueResource).to.be.eql('Speed');

      // Merged
      expect(carouselFields[3].ID).to.be.eql(Fields.CAR_THIRD);
      expect(carouselFields[3].ValueResource).to.be.eql('Duration');

      // Default
      expect(carouselFields[4].ID).to.be.eql(Fields.CAR_FOURTH);
      expect(carouselFields[4].ValueResource).to.be.eql('Duration');

      // Default
      expect(carouselFields[5].ID).to.be.eql(Fields.CAR_FIFTH);
      expect(carouselFields[5].ValueResource).to.be.eql('Duration');

      // "Static" from SIM
      expect(carouselFields[6].ID).to.be.eql(Fields.BOTTOM);
      expect(carouselFields[6].ValueResource).to.be.eql('Duration');

      // Other display is interval type and should not have been changed
      expect(settings.getHasIntervalDisplay()).to.be.true;

      // Check that the mapping to field names work
      JSON.parse(component.getCurrentFields(JSON.stringify(displays), 0))
        .forEach(field => expect(field.name).to.not.equal('(Invalid)'));

      // Settings should be untouched so that hash matches for optimization purposes
      expect(JSON.stringify(JSON.parse(settingsJson)))
        .to.be.equal(JSON.stringify(mode.modes[0].settings));
    });
    it('SimpleLineChart to Data3Fields', () => {
      if (!SupportHelpers.simSupportsGraphs()) {
        return;
      }

      const component = new Component('Amsterdam', '1.9.0', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays-graph.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const mode = JSON.parse(component.changeDisplay(displayJson, settingsJson, 0, 'Data3Fields'));
      expect(mode.modes.length).to.be.eql(1);

      const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

      // No interval displays
      expect(settings.getHasIntervalDisplay()).to.be.false;

      expect(displays.displays().length).to.be.eql(1);

      const templates = displays.templates();

      expect(templates.length).to.be.eql(1);
      expect(templates[0]).to.be.eql('Data3Fields');

      const fields = displays.displays()[0].Fields;

      expect(fields.length).to.be.eql(3);

      // Check common values. No merge is done when changing from Graph
      fields.forEach((field) => {
        expect(Object.keys(field).length).to.be.eql(8);
        expect(field.PathRoot).to.be.eql('Activity');
        expect(field.ValueAggregate).to.be.eql('Current');
        expect(field.ValueFormat).to.be.eql('Duration');
        expect(field.ValueResource).to.be.eql('Duration');
        expect(field.ValueWindow).to.be.eql('Move');
        expect(field.ValueWindowIndex).to.be.eql(-1);
      });

      expect(fields[0].ValueFormatStyle).contains('digits');
      expect(fields[0].ID).to.be.eql('Top');

      expect(fields[1].ValueFormatStyle).to.be.eql('Fivedigits');
      expect(fields[1].ID).to.be.eql('Center');

      expect(fields[2].ValueFormatStyle).contains('digits');
      expect(fields[2].ID).to.be.eql('Bottom');
    });
    it('Data4Fields to Zones8Fields', () => {
      if (!SupportHelpers.simSupportsZones()) {
        return;
      }

      const component = new Component('Amsterdam', '2.3.16', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const mode = JSON.parse(component.changeDisplay(displayJson, settingsJson, 0, 'Zones8Fields'));
      expect(mode.modes.length).to.be.eql(1);

      const displays = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

      expect(displays.displays().length).to.be.eql(2);

      const templates = displays.templates();

      expect(templates.length).to.be.eql(2);
      expect(templates[0]).to.be.eql(Displays.ZONES_8_FIELDS);
      expect(templates[1]).to.be.eql(Displays.INTERVAL_37);

      const graphFields = displays.displays()[0].Fields;

      expect(graphFields.length).to.be.eql(7);

      const find = (position => graphFields.find(field => field.ID === position));

      // Default - zone display is not merged
      const top = find(Fields.TOP);
      expect(Object.keys(top).length).to.be.eql(8);
      expect(top.PathRoot).to.be.eql('Activity');
      expect(top.ValueAggregate).to.be.eql('Avg');
      expect(top.ValueFormat).to.be.eql('HeartRate');
      expect(top.ValueFormatStyle).to.be.eql('Fourdigits');
      expect(top.ValueResource).to.be.eql('HeartRate');
      expect(top.ValueWindow).to.be.eql('Move');
      expect(top.ValueWindowIndex).to.be.eql(-1);

      // Synced with 'Top'
      const topSecond = find(Fields.TOP_SECOND);
      expect(Object.keys(topSecond).length).to.be.eql(8);
      expect(topSecond.ValueResource).to.be.eql('HeartRate');

      // Synced with 'Top'
      const topThird = find(Fields.TOP_THIRD);
      expect(Object.keys(topThird).length).to.be.eql(8);
      expect(topThird.ValueResource).to.be.eql('HeartRate');

      const center = find(Fields.CENTER);
      expect(Object.keys(center).length).to.be.eql(8);
      expect(center.ValueResource).to.be.eql('HeartRate');

      const centerTwo = find(Fields.CENTER_SECOND);
      expect(Object.keys(centerTwo).length).to.be.eql(8);
      expect(centerTwo.ValueResource).to.be.eql('Power');

      const centerThird = find(Fields.CENTER_THIRD);
      expect(Object.keys(centerThird).length).to.be.eql(8);
      expect(centerThird.ValueResource).to.be.eql('Speed');

      const bottom = find(Fields.BOTTOM);
      expect(Object.keys(bottom).length).to.be.eql(8);
      expect(bottom.ValueResource).to.be.eql('Duration');

      // Other display is interval type and should not have been changed
      expect(settings.getHasIntervalDisplay()).to.be.true;

      // Check that the mapping to field names work
      JSON.parse(component.getCurrentFields(JSON.stringify(displays), 0))
        .forEach(field => expect(field.name).to.not.equal('(Invalid)'));

      // Settings should be untouched so that hash matches for optimization purposes
      expect(JSON.stringify(JSON.parse(settingsJson)))
        .to.be.equal(JSON.stringify(mode.modes[0].settings));
    });
  });
  describe('getFields', () => {
    const getDuplicates = (ar) => {
      const checked = [];
      const duplicates = ar.filter((item) => {
        const ret = checked.includes(item.id);
        checked.push(item.id);
        return ret;
      });
      return duplicates;
    };
    it('1.9.0', () => {
      const component = new Component('Amsterdam', '1.9.0', 'fi');
      const fieldsections = JSON.parse(component.getFields(3, 'Data4Fields', 'TopLeft'));
      if (!Packages.isFieldSectionsSupported()) {
        // SIM version 2.0.15 and older only contains one section for fields
        expect(fieldsections.length).to.be.eql(1);
        expect(fieldsections[0].fields.length).to.be.eql(69);
      } else {
        expect(fieldsections.length).to.be.eql(11);
        fieldsections.forEach((section) => {
          expect(getDuplicates(section.fields).length).to.be.eql(0);
        });
        expect(fieldsections[0].fields.length).to.be.eql(4);
        expect(fieldsections[1].fields.length).to.be.eql(8);
        expect(fieldsections[2].fields.length).to.be.eql(5);
        expect(fieldsections[3].fields.length).to.be.eql(6);
        expect(fieldsections[4].fields.length).to.be.eql(4);
        expect(fieldsections[5].fields.length).to.be.above(3);
        expect(fieldsections[6].fields.length).to.be.eql(2);
        expect(fieldsections[7].fields.length).to.be.above(11);
        expect(fieldsections[8].fields.length).to.be.eql(12);
        expect(fieldsections[9].fields.length).to.be.eql(7);
        expect(fieldsections[10].fields.length).to.be.eql(8);
      }
      const intervalFieldSections2 = JSON.parse(component.getFields(3, 'TXT_POWER', 'TopLeft'));
      expect(intervalFieldSections2.length).to.be.eql(0);
    });
    it('2.3.0', () => {
      const component = new Component('Amsterdam', '2.3.0', 'fi');
      const fieldsections = JSON.parse(component.getFields(3, 'Data4Fields', 'TopLeft'));
      if (!Packages.isFieldSectionsSupported()) {
        // SIM version 2.0.15 and older only contains one section for fields
        expect(fieldsections.length).to.be.eql(1);
        expect(fieldsections[0].fields.length).to.be.eql(87);
      } else {
        expect(fieldsections.length).to.be.eql(12);
        fieldsections.forEach((section) => {
          expect(getDuplicates(section.fields).length).to.be.eql(0);
        });
        expect(fieldsections[0].fields.length).to.be.eql(4);
        expect(fieldsections[1].fields.length).to.be.eql(10);
        expect(fieldsections[2].fields.length).to.be.eql(5);
        expect(fieldsections[3].fields.length).to.be.eql(12);
        expect(fieldsections[4].fields.length).to.be.eql(4);
        expect(fieldsections[5].fields.length).to.be.above(5);
        expect(fieldsections[6].fields.length).to.be.eql(2);
        expect(fieldsections[7].fields.length).to.be.eql(6);
        expect(fieldsections[8].fields.length).to.be.above(11);
        expect(fieldsections[9].fields.length).to.be.eql(15);
        expect(fieldsections[10].fields.length).to.be.eql(14);
      }
      const intervalFieldSections2 = JSON.parse(component.getFields(3, 'TXT_POWER', 'TopLeft'));
      expect(intervalFieldSections2.length).to.be.eql(0);
    });
    describe('SimpleLineChart', () => {
      it('Running', () => {
        if (!SupportHelpers.simSupportsGraphs()) {
          return;
        }

        const component = new Component('Amsterdam', '2.3.12', 'fi');
        const fieldsections = JSON.parse(component.getFields(3, Displays.SIMPLE_LINE_CHART, 1));

        expect(fieldsections.length).to.be.eql(6);
        expect(fieldsections[0].fields.length).to.be.eql(1);
        expect(fieldsections[0].fields[0].id).to.be.eql('TXT_EPOC');
        expect(fieldsections[1].fields.length).to.be.eql(1);
        expect(fieldsections[1].fields[0].id).to.be.eql('TXT_CADENCE');
        expect(fieldsections[2].fields.length).to.be.eql(1);
        expect(fieldsections[2].fields[0].id).to.be.eql('TXT_SPEED');
        expect(fieldsections[3].fields.length).to.be.eql(1);
        expect(fieldsections[3].fields[0].id).to.be.eql('TXT_ALTITUDE');
        expect(fieldsections[4].fields.length).to.be.eql(1);
        expect(fieldsections[4].fields[0].id).to.be.eql('TXT_HEART_RATE');
        expect(fieldsections[5].fields.length).to.be.eql(1);
        expect(fieldsections[5].fields[0].id).to.be.eql('TXT_POWER_3S');
      });
      it('Swimming', () => {
        if (!SupportHelpers.simSupportsGraphs()) {
          return;
        }

        const component = new Component('Amsterdam', '2.3.12', 'fi');
        const fieldsections = JSON.parse(component.getFields(6, Displays.SIMPLE_LINE_CHART, 1));

        // Exact fields depend on SIM version
        expect(fieldsections.length).to.be.above(2);
        expect(fieldsections[0].fields.length).to.be.eql(1);
        expect(fieldsections[0].fields[0].id).to.be.eql('TXT_EPOC');
      });
    });
    describe('SimpleLineChartWearRound', () => {
      it('Running', () => {
        if (!SupportHelpers.simSupportsKyoto()) {
          return;
        }

        const component = new Component('Kyoto', '1.122', 'en');
        const fieldsections =
          JSON.parse(component.getFields(3, Displays.SIMPLE_LINE_CHART_WEAR_ROUND, 1));

        expect(fieldsections.length).to.be.eql(4);

        expect(fieldsections[0].fields.length).to.be.eql(1);
        expect(fieldsections[0].fields[0].id).to.be.eql('TXT_CADENCE');

        expect(fieldsections[1].fields.length).to.be.eql(1);
        expect(fieldsections[1].fields[0].id).to.be.eql('TXT_HEART_RATE');

        expect(fieldsections[2].fields.length).to.be.eql(2);
        expect(fieldsections[2].fields[0].id).to.be.eql('TXT_PACE');
        expect(fieldsections[2].fields[1].id).to.be.eql('TXT_SPEED');

        expect(fieldsections[3].fields.length).to.be.eql(3);
        expect(fieldsections[3].fields[0].id).to.be.eql('TXT_ALTITUDE');
        expect(fieldsections[3].fields[1].id).to.be.eql('TXT_VERTICAL_SPEED_SLOW');
        expect(fieldsections[3].fields[2].id).to.be.eql('TXT_VERTICAL_SPEED_FAST');
      });
    });
  });
  describe('addDisplay', () => {
    it('interval', () => {
      const component = new Component('Amsterdam', '1.9.0', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays-without-intervals.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const mode = JSON.parse(component.addDisplay(displayJson, settingsJson, 0, 'TXT_SPEED'));
      expect(mode.modes.length).to.be.eql(1);

      const display = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const templates = display.templates();

      expect(templates.length).to.be.eql(2);
      expect(templates[0]).to.be.eql('Interval77');
      expect(templates[1]).to.be.eql('Data4Fields');

      // Sanity check fields
      const interval = display.displays()[0].Fields;
      expect(interval.length).to.be.eql(14);

      // Check one field that it contains sane values and not nulls
      expect(Object.keys(interval[0]).length).to.be.eql(8);
      expect(interval[0].ID).to.be.eql('1stLeft');
      expect(interval[0].PathRoot).to.be.eql('Activity');
      expect(interval[0].ValueAggregate).to.be.eql('Current');
      expect(interval[0].ValueFormat).to.be.eql('Distance');
      expect(interval[0].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(interval[0].ValueResource).to.be.eql('Distance');
      expect(interval[0].ValueWindow).to.be.eql('Interval');
      expect(interval[0].ValueWindowIndex).to.be.eql(-1);

      const watchSettings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));
      expect(watchSettings.getHasIntervalDisplay()).to.be.true;

      // Try add second interval display
      const twoIntervalDisplays = component.addDisplay(JSON.stringify(mode.modes[0].displays), settingsJson, 1, 'TXT_POWER_NO_DISTANCE_INTERVAL');
      expect(twoIntervalDisplays).to.be.eql('');
    });
    it('columns', () => {
      const component = new Component('Amsterdam', '1.9.0', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays-without-intervals.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const mode = JSON.parse(component.addDisplay(displayJson, settingsJson, 0, 'Table2Columns'));
      expect(mode.modes.length).to.be.eql(1);

      const display = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const templates = display.templates();

      expect(templates.length).to.be.eql(2);
      expect(templates[0]).to.be.eql('Table2Columns');
      expect(templates[1]).to.be.eql('Data4Fields');

      const columns = display.displays()[0].Fields;

      expect(columns.length).to.be.eql(4);

      // Default (no merge for any fields in column displays)
      expect(Object.keys(columns[0]).length).to.be.eql(8);
      expect(columns[0].ID).to.be.eql('Top');
      expect(columns[0].PathRoot).to.be.eql('Activity');
      expect(columns[0].ValueAggregate).to.be.eql('Current');
      expect(columns[0].ValueFormat).to.be.eql('Duration');
      expect(columns[0].ValueFormatStyle).contains('digits');
      expect(columns[0].ValueResource).to.be.eql('Duration');
      expect(columns[0].ValueWindow).to.be.eql('Move');
      expect(columns[0].ValueWindowIndex).to.be.eql(-1);

      expect(Object.keys(columns[1]).length).to.be.eql(7);
      expect(columns[1].ID).to.be.eql('IndexColumn');
      expect(columns[1].PathRoot).to.be.eql('Activity');
      expect(columns[1].ValueFormat).to.be.eql('Count');
      expect(columns[1].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(columns[1].ValueResource).to.be.eql('Index');
      expect(columns[1].ValueWindow).to.be.eql('Lap');
      expect(columns[1].ValueWindowIndex).to.be.eql(-1);

      expect(Object.keys(columns[2]).length).to.be.eql(8);
      expect(columns[2].ID).to.be.eql('LeftColumn');
      expect(columns[2].PathRoot).to.be.eql('Activity');
      expect(columns[2].ValueAggregate).to.be.eql('Current');
      expect(columns[2].ValueFormat).to.be.eql('Duration');
      expect(columns[2].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(columns[2].ValueResource).to.be.eql('Duration');
      expect(columns[2].ValueWindow).to.be.eql('Lap');
      expect(columns[2].ValueWindowIndex).to.be.eql(-1);

      expect(Object.keys(columns[3]).length).to.be.eql(8);
      expect(columns[3].ID).to.be.eql('RightColumn');
      expect(columns[3].PathRoot).to.be.eql('Activity');
      expect(columns[3].ValueAggregate).to.be.eql('Avg');
      expect(columns[3].ValueFormat).to.be.eql('HeartRate');
      expect(columns[3].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(columns[3].ValueResource).to.be.eql('HeartRate');
      expect(columns[3].ValueWindow).to.be.eql('Lap');
      expect(columns[3].ValueWindowIndex).to.be.eql(-1);

      const watchSettings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));
      expect(watchSettings.getHasIntervalDisplay()).to.be.false;
    });
    it('non-interval', () => {
      const component = new Component('Amsterdam', '1.9.0', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays-without-intervals.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const mode = JSON.parse(component.addDisplay(displayJson, settingsJson, 0, 'Data3Fields'));
      expect(mode.modes.length).to.be.eql(1);

      const display = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const templates = display.templates();

      expect(templates.length).to.be.eql(2);
      expect(templates[0]).to.be.eql('Data3Fields');
      expect(templates[1]).to.be.eql('Data4Fields');

      const fields = display.displays()[0].Fields;
      const fieldIds = ['Top', 'Center', 'Bottom'];
      fields.forEach((field, fieldIndex) => {
        expect(field.ID).to.be.eql(fieldIds[fieldIndex]);
        expect(field.PathRoot).to.be.eql('Activity');
        expect(field.ValueAggregate).to.be.eql('Current');
        expect(field.ValueFormat).to.be.eql('Duration');
        expect(field.ValueFormatStyle).contains('digits');
        expect(field.ValueResource).to.be.eql('Duration');
        expect(field.ValueWindow).to.be.eql('Move');
        expect(field.ValueWindowIndex).to.be.eql(-1);
      });

      const watchSettings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));
      expect(watchSettings.getHasIntervalDisplay()).to.be.false;
    });
    it('Zones8Fields', () => {
      if (!SupportHelpers.simSupportsZones()) {
        return;
      }

      const component = new Component('Amsterdam', '2.3.16', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const mode =
        JSON.parse(component.addDisplay(displayJson, settingsJson, 2, Displays.ZONES_8_FIELDS));
      expect(mode.modes.length).to.be.eql(1);

      const display = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const templates = display.templates();

      expect(templates.length).to.be.eql(3);
      expect(templates[0]).to.be.eql(Displays.DATA_4_FIELDS);
      expect(templates[1]).to.be.eql(Displays.INTERVAL_37);
      expect(templates[2]).to.be.eql(Displays.ZONES_8_FIELDS);

      const fields = display.displays()[2].Fields;
      const find = (position => fields.find(field => field.ID === position));

      const top = find(Fields.TOP);
      expect(Object.keys(top).length).to.be.eql(8);
      expect(top.PathRoot).to.be.eql('Activity');
      expect(top.ValueAggregate).to.be.eql('Avg');
      expect(top.ValueFormat).to.be.eql('HeartRate');
      expect(top.ValueFormatStyle).to.be.eql('Fourdigits');
      expect(top.ValueResource).to.be.eql('HeartRate');
      expect(top.ValueWindow).to.be.eql('Move');
      expect(top.ValueWindowIndex).to.be.eql(-1);

      const topSecond = find(Fields.TOP_SECOND);
      expect(Object.keys(topSecond).length).to.be.eql(8);
      expect(topSecond.ValueResource).to.be.eql('HeartRate');

      const topThird = find(Fields.TOP_THIRD);
      expect(Object.keys(topThird).length).to.be.eql(8);
      expect(topThird.ValueResource).to.be.eql('HeartRate');

      const center = find(Fields.CENTER);
      expect(Object.keys(center).length).to.be.eql(8);
      expect(center.ValueResource).to.be.eql('HeartRate');

      const centerTwo = find(Fields.CENTER_SECOND);
      expect(Object.keys(centerTwo).length).to.be.eql(8);
      expect(centerTwo.ValueResource).to.be.eql('Power');

      const centerThird = find(Fields.CENTER_THIRD);
      expect(Object.keys(centerThird).length).to.be.eql(8);
      expect(centerThird.ValueResource).to.be.eql('Speed');

      const bottom = find(Fields.BOTTOM);
      expect(Object.keys(bottom).length).to.be.eql(8);
      expect(bottom.ValueResource).to.be.eql('Duration');

      const watchSettings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));
      expect(watchSettings.getHasIntervalDisplay()).to.be.true;
    });
    it('to-end', () => {
      const component = new Component('Amsterdam', '1.9.0', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays-without-intervals.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const mode1 = JSON.parse(component.addDisplay(displayJson, settingsJson, 0, 'Data3Fields'));
      const mode2 = JSON.parse(component.addDisplay(JSON.stringify(mode1.modes[0].displays), JSON.stringify(mode1.modes[0].settings), 2, 'Data7Fields'));
      expect(mode2.modes.length).to.be.eql(1);

      const display = new WatchSportModeDisplays(JSON.stringify(mode2.modes[0].displays));
      const templates = display.templates();

      expect(templates.length).to.be.eql(3);
      expect(templates[0]).to.be.eql('Data3Fields');
      expect(templates[1]).to.be.eql('Data4Fields');
      expect(templates[2]).to.be.eql('Data7Fields');

      const fields = display.displays()[2].Fields;
      const fieldIds = ['1stLeft', '1stRight', '2ndLeft', '2ndRight', '3rdLeft', '3rdRight', 'Bottom'];
      fields.forEach((field, fieldIndex) => {
        expect(field.ID).to.be.eql(fieldIds[fieldIndex]);
        expect(field.PathRoot).to.be.eql('Activity');
        expect(field.ValueAggregate).to.be.eql('Current');
        expect(field.ValueFormat).to.be.eql('Duration');
        expect(field.ValueFormatStyle).contains('digits');
        expect(field.ValueResource).to.be.eql('Duration');
        expect(field.ValueWindow).to.be.eql('Move');
        expect(field.ValueWindowIndex).to.be.eql(-1);
      });

      const watchSettings = new WatchSportModeSettings(JSON.stringify(mode2.modes[0].settings));
      expect(watchSettings.getHasIntervalDisplay()).to.be.false;
    });
    it('SimpleLineChart', () => {
      if (!SupportHelpers.simSupportsGraphs()) {
        return;
      }

      const component = new Component('Amsterdam', '2.4.0', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const mode = JSON.parse(component.addDisplay(displayJson, settingsJson, 2, 'SimpleLineChart'));
      expect(mode.modes.length).to.be.eql(1);

      const display = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const templates = display.templates();

      expect(templates.length).to.be.eql(3);
      expect(templates[0]).to.be.eql(Displays.DATA_4_FIELDS);
      expect(templates[1]).to.be.eql(Displays.INTERVAL_37);
      expect(templates[2]).to.be.eql(Displays.SIMPLE_LINE_CHART);

      const graphFields = display.displays()[2].Fields;

      expect(graphFields.length).to.be.eql(3);

      expect(Object.keys(graphFields[0]).length).to.be.eql(8);
      expect(graphFields[0].ID).to.be.eql(Fields.TOP);
      expect(graphFields[0].ValueResource).to.be.eql('HeartRate');

      expect(Object.keys(graphFields[1]).length).to.be.eql(15);
      expect(graphFields[1].ID).to.be.eql(Fields.CHART);
      expect(graphFields[1].ValueResource).to.be.eql('HeartRate');

      expect(Object.keys(graphFields[2]).length).to.be.eql(8);
      expect(graphFields[2].ID).to.be.eql(Fields.BOTTOM);
      expect(graphFields[2].ValueResource).to.be.eql('Duration');

      const watchSettings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));
      expect(watchSettings.getHasIntervalDisplay()).to.be.true;
    });
    it('SimpleLineChartWearRound', () => {
      if (!SupportHelpers.simSupportsKyoto()) {
        return;
      }

      const component = new Component('Kyoto', '1.122', 'en');
      const displayJson = fs.readFileSync(`${testDataPath}/displays-kyoto.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const mode = JSON.parse(component.addDisplay(displayJson, settingsJson, 1, 'SimpleLineChartWearRound'));
      expect(mode.modes.length).to.be.eql(1);

      const display = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const templates = display.templates();

      // 6 includes hidden displays
      expect(templates.length).to.be.eql(6);
      expect(templates[0]).to.be.eql(Displays.DATA_4_FIELDS_WEAR_ROUND);
      expect(templates[1]).to.be.eql(Displays.SIMPLE_LINE_CHART_WEAR_ROUND);

      const graphFields = display.displays()[1].Fields;

      expect(graphFields.length).to.be.eql(4);

      expect(Object.keys(graphFields[0]).length).to.be.eql(5);
      expect(graphFields[0].ID).to.be.eql(Fields.TOP_MINI);
      expect(graphFields[0].ValueResource).to.be.eql('LocalTime');

      expect(Object.keys(graphFields[1]).length).to.be.eql(8);
      expect(graphFields[1].ID).to.be.eql(Fields.TOP);
      expect(graphFields[1].ValueResource).to.be.eql('HeartRate');

      expect(Object.keys(graphFields[2]).length).to.be.eql(12);
      expect(graphFields[2].ID).to.be.eql(Fields.CHART);
      expect(graphFields[2].ValueResource).to.be.eql('com.google.heart_rate.Bpm.bpm');

      expect(Object.keys(graphFields[3]).length).to.be.eql(8);
      expect(graphFields[3].ID).to.be.eql(Fields.BOTTOM);
      expect(graphFields[3].ValueResource).to.be.eql('Duration');
    });
    it('LapTable', () => {
      if (!SupportHelpers.simSupportsLapTable()) {
        return;
      }

      const component = new Component('Monza', '2.7.10', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const mode = JSON.parse(component
        .addDisplay(displayJson, settingsJson, 2, Displays.LAP_TABLE));
      expect(mode.modes.length).to.be.eql(1);

      const display = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const templates = display.templates();

      expect(templates.length).to.be.eql(3);
      expect(templates[0]).to.be.eql(Displays.DATA_4_FIELDS);
      expect(templates[1]).to.be.eql(Displays.INTERVAL_37);
      expect(templates[2]).to.be.eql(Displays.LAP_TABLE);

      const lapTableFields = display.displays()[2].Fields;

      expect(lapTableFields.length).to.be.eql(4);

      expect(Object.keys(lapTableFields[0]).length).to.be.eql(8);
      expect(lapTableFields[0].ID).to.be.eql(Fields.LEFT_COLUMN);
      expect(lapTableFields[0].ValueResource).to.be.eql('Duration');
      expect(lapTableFields[0].ValueWindow).to.be.eql('Lap');

      expect(Object.keys(lapTableFields[1]).length).to.be.eql(8);
      expect(lapTableFields[1].ID).to.be.eql(Fields.CENTER_COLUMN);
      expect(lapTableFields[1].ValueResource).to.be.eql('Duration');
      expect(lapTableFields[1].ValueWindow).to.be.eql('Lap');

      expect(Object.keys(lapTableFields[2]).length).to.be.eql(8);
      expect(lapTableFields[2].ID).to.be.eql(Fields.RIGHT_COLUMN);
      expect(lapTableFields[2].ValueResource).to.be.eql('HeartRate');
      expect(lapTableFields[2].ValueWindow).to.be.eql('Lap');

      expect(Object.keys(lapTableFields[3]).length).to.be.eql(8);
      expect(lapTableFields[3].ID).to.be.eql(Fields.BOTTOM);
      expect(lapTableFields[3].ValueResource).to.be.eql('Duration');
      expect(lapTableFields[3].ValueWindow).to.be.eql('Move');

      const watchSettings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));
      expect(watchSettings.getHasIntervalDisplay()).to.be.true;
    });
    it('Data3Carousel', () => {
      if (!Packages.displayFieldProperties(Displays.DATA_3_CAROUSEL)) {
        // Older SIM does not support default fields
        return;
      }

      const component = new Component('Monza', '2.7.10', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const mode = JSON.parse(component
        .addDisplay(displayJson, settingsJson, 2, Displays.DATA_3_CAROUSEL));
      expect(mode.modes.length).to.be.eql(1);

      const display = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
      const templates = display.templates();

      expect(templates.length).to.be.eql(3);
      expect(templates[0]).to.be.eql(Displays.DATA_4_FIELDS);
      expect(templates[1]).to.be.eql(Displays.INTERVAL_37);
      expect(templates[2]).to.be.eql(Displays.DATA_3_CAROUSEL);

      const carouselFields = display.displays()[2].Fields;

      expect(carouselFields.length).to.be.eql(7);

      // Field type from SIM
      expect(Object.keys(carouselFields[0]).length).to.be.eql(5);
      expect(carouselFields[0].ID).to.be.eql(Fields.TOP);
      expect(carouselFields[0].PathRoot).to.be.eql('Dev/Time');
      expect(carouselFields[0].ValueFormat).to.be.eql('TimeOfDay');
      expect(carouselFields[0].ValueFormatStyle).to.be.eql('Fourdigits');
      expect(carouselFields[0].ValueResource).to.be.eql('LocalTime');

      // Default field type Duration used for rest of the fields,
      // except the last one (Bottom) which also happens to be
      // Duration.
      for (let i = 1; i < carouselFields.length; ++i) {
        expect(Object.keys(carouselFields[i]).length).to.be.eql(8);
        expect(carouselFields[i].ValueResource).to.be.eql('Duration');
        expect(carouselFields[i].ValueWindow).to.be.eql('Move');
      }

      const watchSettings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));
      expect(watchSettings.getHasIntervalDisplay()).to.be.true;
    });
    describe('Kyoto', () => {
      it('At valid index', () => {
        if (!SupportHelpers.simSupportsKyoto()) {
          return;
        }

        const component = new Component('Kyoto', '1.122', 'en');
        const displayJson = fs.readFileSync(`${testDataPath}/displays-kyoto.json`, 'utf8');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
        const mode = JSON.parse(component
          .addDisplay(displayJson, settingsJson, 1, Displays.DATA_3_FIELDS_WEAR_ROUND));
        expect(mode.modes.length).to.be.eql(1);

        const display = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
        const templates = display.templates();

        expect(templates.length).to.be.eql(6);
        expect(templates[0]).to.be.eql(Displays.DATA_4_FIELDS_WEAR_ROUND);
        expect(templates[1]).to.be.eql(Displays.DATA_3_FIELDS_WEAR_ROUND);
        // Hidden displays
        expect(templates[2]).to.be.eql(Displays.MAP_WEAR_ROUND);
        expect(templates[3]).to.be.eql(Displays.DISTANCE_AUTO_LAP);
        expect(templates[4]).to.be.eql(Displays.DURATION_AUTO_LAP);
        expect(templates[5]).to.be.eql(Displays.MANUAL_LAP);

        const threeFields = display.displays()[1].Fields;

        expect(threeFields.length).to.be.eql(4);

        // Field type from SIM
        expect(Object.keys(threeFields[0]).length).to.be.eql(5);
        expect(threeFields[0].ID).to.be.eql(Fields.TOP_MINI);
        expect(threeFields[0].PathRoot).to.be.eql('Dev/Time');
        expect(threeFields[0].ValueFormat).to.be.eql('TimeOfDay');
        expect(threeFields[0].ValueFormatStyle).to.be.eql('Fourdigits');
        expect(threeFields[0].ValueResource).to.be.eql('LocalTime');

        // Default field type Duration used for rest of the fields,
        // except the last one (Bottom) which also happens to be
        // Duration.
        for (let i = 1; i < threeFields.length; ++i) {
          expect(Object.keys(threeFields[i]).length).to.be.eql(8);
          expect(threeFields[i].ValueResource).to.be.eql('Duration');
          expect(threeFields[i].ValueWindow).to.be.eql('Move');
        }
      });
      it('Too large index', () => {
        // Android app tries to set the new display AFTER map display
        // which is not possible - make sure any such attempt will add
        // the display as last visible display

        if (!SupportHelpers.simSupportsKyoto()) {
          return;
        }

        const component = new Component('Kyoto', '1.122', 'en');
        const displayJson = fs.readFileSync(`${testDataPath}/displays-kyoto.json`, 'utf8');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
        // Too large index (99))
        const mode = JSON.parse(component
          .addDisplay(displayJson, settingsJson, 99, Displays.DATA_3_FIELDS_WEAR_ROUND));
        expect(mode.modes.length).to.be.eql(1);

        const display = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
        const templates = display.templates();

        expect(templates.length).to.be.eql(6);
        expect(templates[0]).to.be.eql(Displays.DATA_4_FIELDS_WEAR_ROUND);
        // Regardless of too large index, display was set to here just
        // before hidden displays
        expect(templates[1]).to.be.eql(Displays.DATA_3_FIELDS_WEAR_ROUND);
        // Hidden displays
        expect(templates[2]).to.be.eql(Displays.MAP_WEAR_ROUND);
        expect(templates[3]).to.be.eql(Displays.DISTANCE_AUTO_LAP);
        expect(templates[4]).to.be.eql(Displays.DURATION_AUTO_LAP);
        expect(templates[5]).to.be.eql(Displays.MANUAL_LAP);
      });
      it('Default fields', () => {
        if (!SupportHelpers.simSupportsKyoto()) {
          return;
        }

        const component = new Component('Kyoto', '1.122', 'en');
        const displayJson = fs.readFileSync(`${testDataPath}/displays-kyoto.json`, 'utf8');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
        const mode = JSON.parse(component
          .addDisplay(displayJson, settingsJson, 1, Displays.LAP_TABLE_WEAR_ROUND));
        expect(mode.modes.length).to.be.eql(1);

        const display = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
        const templates = display.templates();

        expect(templates.length).to.be.eql(6);
        expect(templates[0]).to.be.eql(Displays.DATA_4_FIELDS_WEAR_ROUND);
        expect(templates[1]).to.be.eql(Displays.LAP_TABLE_WEAR_ROUND);
        // Hidden displays
        expect(templates[2]).to.be.eql(Displays.MAP_WEAR_ROUND);
        expect(templates[3]).to.be.eql(Displays.DISTANCE_AUTO_LAP);
        expect(templates[4]).to.be.eql(Displays.DURATION_AUTO_LAP);
        expect(templates[5]).to.be.eql(Displays.MANUAL_LAP);

        const fields = JSON.parse(component.getCurrentFields(JSON.stringify(display), 1));
        expect(fields.length).to.be.eql(3);

        expect(fields[0].id).to.be.eql('TXT_LAP_DURATION');
        expect(fields[1].id).to.be.eql('TXT_LAP_MAX_HEART_RATE');
        expect(fields[2].id).to.be.eql('TXT_LAP_AVG_HEART_RATE');
      });
      it('Specific fields', () => {
        if (!SupportHelpers.simSupportsKyoto()) {
          return;
        }

        const component = new Component('Kyoto', '1.122', 'en');
        const displayJson = fs.readFileSync(`${testDataPath}/displays-kyoto.json`, 'utf8');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
        const poolSettings = new WatchSportModeSettings(settingsJson);
        // Pool Swimming has specific default values
        poolSettings.Settings.CustomModes[0].ActivityID = 6;

        const mode = JSON.parse(component
          .addDisplay(displayJson, JSON.stringify(poolSettings), 1, Displays.LAP_TABLE_WEAR_ROUND));
        expect(mode.modes.length).to.be.eql(1);

        const display = new WatchSportModeDisplays(JSON.stringify(mode.modes[0].displays));
        const templates = display.templates();

        expect(templates.length).to.be.eql(6);
        expect(templates[1]).to.be.eql(Displays.LAP_TABLE_WEAR_ROUND);

        const fields = JSON.parse(component.getCurrentFields(JSON.stringify(display), 1));
        expect(fields.length).to.be.eql(3);

        expect(fields[0].id).to.be.eql('TXT_INTERVAL_SWIMDISTANCE');
        expect(fields[1].id).to.be.eql('TXT_INTERVAL_DURATION');
        expect(fields[2].id).to.be.eql('TXT_INTERVAL_AVG_SWIM_PACE');
      });
    });
  });
  describe('deleteDisplay', () => {
    it('interval', () => {
      const component = new Component('Amsterdam', '1.9.0', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const mode = JSON.parse(component.deleteDisplay(displayJson, settingsJson, 1));
      expect(mode.modes.length).to.be.eql(1);

      const settingsObj = mode.modes[0].settings;
      const displaysObj = mode.modes[0].displays;

      const display = new WatchSportModeDisplays(JSON.stringify(displaysObj));
      const templates = display.templates();

      // 'Interval37' should be gone
      expect(templates.length).to.be.eql(1);
      expect(templates[0]).to.be.eql('Data4Fields');

      // Since there are no interval displays anymore, settings should have
      // been updated accordingly
      const settings = new WatchSportModeSettings(JSON.stringify(settingsObj));
      expect(settings.getHasIntervalDisplay()).to.be.false;
    });
    it('non-interval', () => {
      const component = new Component('Amsterdam', '1.9.0', 'fi');
      const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
      const mode = JSON.parse(component.deleteDisplay(displayJson, settingsJson, 0));
      expect(mode.modes.length).to.be.eql(1);

      const settingsObj = mode.modes[0].settings;
      const displaysObj = mode.modes[0].displays;

      const display = new WatchSportModeDisplays(JSON.stringify(displaysObj));
      const templates = display.templates();

      // 'Data4Fields' should be gone
      expect(templates.length).to.be.eql(1);
      expect(templates[0]).to.be.eql('Interval37');

      // Since there is still interval displays, settings should have
      // been untouched
      const settings = new WatchSportModeSettings(JSON.stringify(settingsObj));
      expect(settings.getHasIntervalDisplay()).to.be.true;
      expect(settings).to.deep.equal(JSON.parse(settingsJson));
    });
  });
  it('Plain function API wrappers', () => {
    // Test makes sure that all the methods of Component have also function
    // in plain function API
    const component = new Component();
    Object.getOwnPropertyNames(Object.getPrototypeOf(component)).forEach((name) => {
      const method = component[name];
      if (method instanceof Function && name !== 'constructor' && !name.startsWith('_')) {
        expect(index[name]).to.be.an.instanceof(Function);
      }
    });
  });
  describe('Number of displays', () => {
    describe('With GPS', () => {
      it('Min', () => {
        const component = new Component('Amsterdam', '1.9.0', 'fi');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
        const min = JSON.parse(component.getMinNumberOfDisplays(settingsJson));
        expect(min).to.be.eql(2);
      });
      it('Max', () => {
        const component = new Component('Amsterdam', '1.9.0', 'fi');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
        const max = JSON.parse(component.getMaxNumberOfDisplays(settingsJson));
        expect(max).to.be.eql(4);
      });
    });
    describe('Helsinki', () => {
      it('Min', () => {
        const component = new Component('Helsinki', '1.9.0', 'fi');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
        const min = JSON.parse(component.getMinNumberOfDisplays(settingsJson));
        expect(min).to.be.eql(1);
      });
      it('Max', () => {
        const component = new Component('Helsinki', '1.9.0', 'fi');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
        const max = JSON.parse(component.getMaxNumberOfDisplays(settingsJson));
        expect(max).to.be.eql(3);
      });
    });
    describe('Without GPS', () => {
      it('Min', () => {
        const component = new Component('Amsterdam', '1.9.0', 'fi');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
        const settingsObj = new WatchSportModeSettings(settingsJson);
        settingsObj.setGpsInterval(0);
        const min = JSON.parse(component.getMinNumberOfDisplays(JSON.stringify(settingsObj)));
        expect(min).to.be.eql(1);
      });
      it('Max', () => {
        const component = new Component('Amsterdam', '1.9.0', 'fi');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
        const settingsObj = new WatchSportModeSettings(settingsJson);
        settingsObj.setGpsInterval(0);
        const min = JSON.parse(component.getMaxNumberOfDisplays(JSON.stringify(settingsObj)));
        expect(min).to.be.eql(3);
      });
    });
  });
  it('Supported extensions', () => {
    const component = new Component();
    const extensions = JSON.parse(component.getSupportedExtensions());
    expect(extensions).to.be.an.instanceof(Array);
    expect(extensions.length).to.be.eql(0);
  });
  it('Supported languages', () => {
    const component = new Component();
    const languages = JSON.parse(component.getSupportedLanguages());
    expect(languages).to.be.an.instanceof(Array);
    expect(languages.length).to.be.eql(19);

    expect(languages[0]).to.be.eql('en');
    expect(languages[1]).to.be.eql('fi');
    expect(languages[2]).to.be.eql('nb');
    expect(languages[3]).to.be.eql('ko');
    expect(languages[4]).to.be.eql('it');
    expect(languages[5]).to.be.eql('de');
    expect(languages[6]).to.be.eql('fr');
    expect(languages[7]).to.be.eql('nl');
    expect(languages[8]).to.be.eql('cs');
    expect(languages[9]).to.be.eql('da');
    expect(languages[10]).to.be.eql('zh-Hans');
    expect(languages[11]).to.be.eql('sv');
    expect(languages[12]).to.be.eql('es');
    expect(languages[13]).to.be.eql('ru');
    expect(languages[14]).to.be.eql('pt');
    expect(languages[15]).to.be.eql('pl');
    expect(languages[16]).to.be.eql('ja');
    expect(languages[17]).to.be.eql('th');
    expect(languages[18]).to.be.eql('tr');
  });
  describe('isListFull', () => {
    it('Factory modes', () => {
      const component = new Component();
      const groups = JSON.parse(fs.readFileSync(`${testDataPath}/groups.json`, 'utf8'));
      const factoryGroup = groups[1];
      // Check that the group is factory mode
      expect(factoryGroup.CustomModePackageID).to.be.eql(1);

      for (let i = 0; groups.length < 20; ++i) {
        groups.push(factoryGroup);
      }

      // 20 modes shouldn't be full because there is always room for one factory mode
      expect(groups.length).to.be.eql(20);
      expect(component.isListFull(JSON.stringify(groups), true)).to.be.false;
    });
    it('Custom modes', () => {
      const component = new Component();
      const groups = JSON.parse(fs.readFileSync(`${testDataPath}/groups.json`, 'utf8'));
      const customGroup = groups[0];
      // Check that the group is factory mode
      expect(customGroup.CustomModePackageID).to.be.undefined;

      for (let i = 0; groups.length < 19; ++i) {
        groups.push(customGroup);
      }

      // 19 modes shouldn't be full, because there is still one factory mode which
      // can be dropped
      expect(groups.length).to.be.eql(19);
      expect(component.isListFull(JSON.stringify(groups), false)).to.be.false;

      // Remove last factory mode and add yet another custom mode
      groups.splice(1, 1);
      groups.push(customGroup);

      // 19 custom modes exists, cannot add any more modes before deleting one
      expect(groups.length).to.be.eql(19);
      expect(component.isListFull(JSON.stringify(groups), false)).to.be.true;
    });
  });
  describe('getSettingSections', () => {
    const checkSettingTranslations = (component, sections, settingsJson) => {
      sections.forEach((section) => {
        section.settings.forEach((settingId) => {
          const setting = JSON.parse(component.getSetting(null, settingsJson, settingId));
          if (setting.header.name) {
            expect(setting.header.name).to.not.have.string('TXT_');
          }

          setting.meta.inputs.forEach((input) => {
            expect(input.label).to.be.a('string');
            expect(input.label).to.not.have.string('TXT_');

            if (input.labelDescription) {
              expect(input.labelDescription).to.be.a('string');
              expect(input.labelDescription).to.not.have.string('TXT_');
            }
          });
        });
      });
    };
    it('Amsterdam', () => {
      const component = new Component('Amsterdam', '1.9.0', 'en');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const sections = JSON.parse(component.getSettingSections(settingsJson, settingsJson, 0));

      let i = 0;
      expect(sections[i].id).to.be.eql('UsePODs');
      expect(sections[i].title).to.be.eql('PODs to search');
      expect(sections[i].description.length).to.be.eql(177);
      expect(sections[i++].settings.length).to.be.eql(4);

      expect(sections[i++].id).to.be.eql('GPSInterval');
      expect(sections[i++].id).to.be.eql('AltiBaroMode');
      expect(sections[i].id).to.be.eql('AutoLap');
      expect(sections[i].title).to.be.eql('Autolap');
      expect(sections[i].description.length).to.be.eql(146);
      expect(sections[i].settings.length).to.be.eql(1);
      expect(sections[i++].settings[0]).to.be.eql('AutoLap');
      expect(sections[i++].id).to.be.eql('RecordingInterval');
      expect(sections[i++].id).to.be.eql('AutoPause');
      expect(sections[i++].id).to.be.eql('TouchEnabled');
      expect(sections[i++].id).to.be.eql('FusedSpeedEnabled');
      expect(sections[i++].id).to.be.eql('FusedAltitudeEnabled');
      expect(sections[i++].id).to.be.eql('UseAccelerometer');
      expect(sections[i++].id).to.be.eql('FeelingSelectionEnabled');
      expect(sections[i++].id).to.be.eql('DisplayLowColor');
      expect(sections[i++].id).to.be.eql('DisplayTimeout');
      expect(sections[i++]).to.be.undefined;

      checkSettingTranslations(component, sections, settingsJson);

      const componentChina = new Component('AmsterdamC', '1.9.0', 'en');
      const sectionsChinaString = componentChina.getSettingSections(settingsJson, settingsJson, 0);

      expect(sectionsChinaString).to.be.equal(JSON.stringify(sections));
    });
    it('Forssa', () => {
      const component = new Component('Forssa', '1.9.0', 'en');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const sections = JSON.parse(component.getSettingSections(settingsJson, settingsJson, 0));

      let i = 0;
      expect(sections[i++].id).to.be.eql('HR');
      expect(sections[i].id).to.be.eql('UsePODs');
      expect(sections[i++].settings.length).to.be.eql(3);
      expect(sections[i++].id).to.be.eql('GPSInterval');
      expect(sections[i++].id).to.be.eql('AutoLap');
      expect(sections[i++].id).to.be.eql('RecordingInterval');
      expect(sections[i++].id).to.be.eql('AutoPause');
      expect(sections[i++].id).to.be.eql('FusedSpeedEnabled');
      expect(sections[i++].id).to.be.eql('UseAccelerometer');
      expect(sections[i++].id).to.be.eql('FeelingSelectionEnabled');
      expect(sections[i++].id).to.be.eql('DisplayTimeout');
      expect(sections[i++]).to.be.undefined;

      checkSettingTranslations(component, sections, settingsJson);

      const componentChina = new Component('ForssaC', '1.9.0', 'en');
      const sectionsChinaString = componentChina.getSettingSections(settingsJson, settingsJson, 0);

      expect(sectionsChinaString).to.be.equal(JSON.stringify(sections));
    });
    it('Helsinki', () => {
      const component = new Component('Helsinki', '1.9.0', 'en');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const sections = JSON.parse(component.getSettingSections(settingsJson, settingsJson, 0));

      let i = 0;
      expect(sections[i++].id).to.be.eql('HR');
      expect(sections[i].id).to.be.eql('UsePODs');
      expect(sections[i++].settings.length).to.be.eql(3);
      expect(sections[i++].id).to.be.eql('AutoLap');
      expect(sections[i++].id).to.be.eql('RecordingInterval');
      expect(sections[i++].id).to.be.eql('AutoPause');
      expect(sections[i++].id).to.be.eql('FusedSpeedEnabled');
      expect(sections[i++].id).to.be.eql('UseAccelerometer');
      expect(sections[i++].id).to.be.eql('FeelingSelectionEnabled');
      expect(sections[i++].id).to.be.eql('DisplayTimeout');

      const componentChina = new Component('HelsinkiC', '1.9.0', 'en');
      const sectionsChinaString = componentChina.getSettingSections(settingsJson, settingsJson, 0);

      expect(sectionsChinaString).to.be.equal(JSON.stringify(sections));
    });
    describe('Downhill', () => {
      it('< 1.11.0', () => {
        const component = new Component('Amsterdam', '1.9.0', 'en');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
        const settingsObj = JSON.parse(settingsJson);
        settingsObj.Settings.CustomModes[0].ActivityID = 20;

        const sections = JSON
          .parse(component.getSettingSections(JSON.stringify(settingsObj), settingsJson, 0));

        let i = 0;
        expect(sections[i++].id).to.be.eql('UsePODs');
        expect(sections[i++].id).to.be.eql('GPSInterval');
        expect(sections[i++].id).to.be.eql('AltiBaroMode');
        expect(sections[i++].id).to.be.eql('AutoLap');
        expect(sections[i++].id).to.be.eql('RecordingInterval');
        expect(sections[i++].id).to.be.eql('AutoPause');
        expect(sections[i++].id).to.be.eql('TouchEnabled');
        expect(sections[i++].id).to.be.eql('FusedSpeedEnabled');
        expect(sections[i++].id).to.be.eql('FusedAltitudeEnabled');
        expect(sections[i++].id).to.be.eql('UseAccelerometer');
        expect(sections[i++].id).to.be.eql('FeelingSelectionEnabled');
        expect(sections[i++].id).to.be.eql('DisplayLowColor');
        expect(sections[i++].id).to.be.eql('DisplayTimeout');
        expect(sections[i++]).to.be.undefined;

        checkSettingTranslations(component, sections, settingsJson);

        const componentChina = new Component('AmsterdamC', '1.9.0', 'en');
        const sectionsChinaString = componentChina.getSettingSections(
          JSON.stringify(settingsObj),
          settingsJson,
          0
        );

        expect(sectionsChinaString).to.be.equal(JSON.stringify(sections));
      });
      it('>= 1.11.0', () => {
        const component = new Component('Amsterdam', '1.11.0', 'en');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');
        const settingsObj = JSON.parse(settingsJson);
        settingsObj.Settings.CustomModes[0].ActivityID = 20;

        const sections = JSON
          .parse(component.getSettingSections(JSON.stringify(settingsObj), settingsJson, 0));

        let i = 0;
        expect(sections[i++].id).to.be.eql('UsePODs');
        expect(sections[i++].id).to.be.eql('GPSInterval');
        expect(sections[i++].id).to.be.eql('AltiBaroMode');
        expect(sections[i++].id).to.be.eql('AutoLap');
        expect(sections[i].id).to.be.eql('Downhill+Uphill');
        expect(sections[i].title).to.be.eql('Vertical lap');
        expect(sections[i].description.length).to.be.eql(28);
        expect(sections[i].settings.length).to.be.eql(1);
        expect(sections[i++].settings[0]).to.be.eql('Downhill+Uphill');
        expect(sections[i++].id).to.be.eql('RecordingInterval');
        expect(sections[i++].id).to.be.eql('AutoPause');
        expect(sections[i++].id).to.be.eql('TouchEnabled');
        expect(sections[i++].id).to.be.eql('FusedSpeedEnabled');
        expect(sections[i++].id).to.be.eql('FusedAltitudeEnabled');
        expect(sections[i++].id).to.be.eql('UseAccelerometer');
        expect(sections[i++].id).to.be.eql('FeelingSelectionEnabled');
        expect(sections[i++].id).to.be.eql('DisplayLowColor');
        expect(sections[i++].id).to.be.eql('DisplayTimeout');
        expect(sections[i++]).to.be.undefined;

        checkSettingTranslations(component, sections, settingsJson);

        const componentChina = new Component('AmsterdamC', '1.11.0', 'en');
        const sectionsChinaString = componentChina.getSettingSections(
          JSON.stringify(settingsObj),
          settingsJson,
          0
        );

        expect(sectionsChinaString).to.be.equal(JSON.stringify(sections));
      });
    });
  });
  describe('getSetting', () => {
    it('Name', () => {
      const component = new Component('Ibiza', '1.9.0', 'fi');
      const groupJson = fs.readFileSync(`${testDataPath}/group-custom.json`, 'utf8');

      const setting = JSON.parse(component.getSetting(groupJson, null, 'Name'));

      // Value
      expect(setting.value).to.be.eql('My Running Mode');

      // Header
      expect(setting.header.id).to.be.eql('Name');
      expect(setting.header.name).to.be.null;

      // Type, in this case 'Text'
      expect(setting.meta.type).to.be.eql('Text');
      expect(setting.meta.inputs[0].length.min).to.be.eql(1);
      expect(setting.meta.inputs[0].length.max).to.be.eql(32);

      expect(setting.readOnly).to.be.false;
    });
    it('UseFootPOD', () => {
      const component = new Component('Ibiza', '1.9.0', 'fi');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const setting = JSON.parse(component.getSetting(null, settingsJson, 'UseFootPOD'));

      // Value
      expect(setting.value).to.be.true;

      // Header
      expect(setting.header.id).to.be.eql('UseFootPOD');
      expect(setting.header.name).to.be.null;

      // Meta
      expect(setting.meta.type).to.be.eql('CheckBox');
      expect(setting.meta.inputs.length).to.be.eql(1);
      expect(setting.meta.inputs[0].label).to.be.eql('Foot POD');
      expect(setting.meta.inputs[0].value).to.be.true;

      expect(setting.readOnly).to.be.false;
    });
    it('AltiBaroMode', () => {
      const component = new Component('Ibiza', '1.9.0', 'en');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const setting = JSON.parse(component.getSetting(null, settingsJson, 'AltiBaroMode'));

      // Value
      expect(setting.value).to.be.eql(0);

      // Header
      expect(setting.header.id).to.be.eql('AltiBaroMode');
      expect(setting.header.name).to.be.eql('Alti-Baro');

      // Meta
      expect(setting.meta.type).to.be.eql('RadioButton');
      expect(setting.meta.inputs.length).to.be.eql(3);
      expect(setting.meta.inputs[0].label).to.be.eql('Altimeter');
      expect(setting.meta.inputs[0].value).to.be.eql(0);
      expect(setting.meta.inputs[1].label).to.be.eql('Barometer');
      expect(setting.meta.inputs[1].value).to.be.eql(1);
      expect(setting.meta.inputs[2].label).to.be.eql('Automatic');
      expect(setting.meta.inputs[2].value).to.be.eql(2);

      expect(setting.readOnly).to.be.false;

      // Convert to Setting object and test methods
      const settingObj = new Setting(
        setting.header,
        setting.meta,
        setting.value,
        setting.readOnly
      );

      expect(settingObj.id).to.be.eql('AltiBaroMode');
      expect(settingObj.name).to.be.eql('Alti-Baro');
    });
    it('DisplayLowColor', () => {
      const component = new Component('Ibiza', '1.9.0', 'en');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const setting = JSON.parse(component.getSetting(null, settingsJson, 'DisplayLowColor'));

      // Value
      expect(setting.value).to.be.true;

      // Header
      expect(setting.header.id).to.be.eql('DisplayLowColor');
      expect(setting.header.name).to.be.eql('Display color');

      // Meta
      expect(setting.meta.type).to.be.eql('RadioButton');
      expect(setting.meta.inputs.length).to.be.eql(2);
      expect(setting.meta.inputs[0].label).to.be.eql('Low color (power save)');
      expect(setting.meta.inputs[0].value).to.be.true;
      expect(setting.meta.inputs[1].label).to.be.eql('Full color');
      expect(setting.meta.inputs[1].value).to.be.false;

      expect(setting.readOnly).to.be.false;
    });
    it('DisplayTimeout', () => {
      const component = new Component('Ibiza', '1.9.0', 'en');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const setting = JSON.parse(component.getSetting(null, settingsJson, 'DisplayTimeout'));

      // Value
      expect(setting.value).to.be.eql(0);

      // Header
      expect(setting.header.id).to.be.eql('DisplayTimeout');
      expect(setting.header.name).to.be.eql('Display timeout');

      // Meta
      expect(setting.meta.type).to.be.eql('RadioButton');
      expect(setting.meta.inputs.length).to.be.eql(2);
      expect(setting.meta.inputs[0].label).to.be.eql('On (power save)');
      expect(setting.meta.inputs[0].value).to.be.eql(10);
      expect(setting.meta.inputs[1].label).to.be.eql('Off');
      expect(setting.meta.inputs[1].value).to.be.eql(0);

      expect(setting.readOnly).to.be.false;
    });
    describe('GPSInterval', () => {
      const getGpsInterval = (variant) => {
        const component = new Component(variant, '1.9.0', 'en');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

        return JSON.parse(component.getSetting(null, settingsJson, 'GPSInterval'));
      };
      it('Amsterdam', () => {
        const setting = getGpsInterval('Amsterdam');
        // Value
        expect(setting.value).to.be.eql(1);

        // Header
        expect(setting.header.id).to.be.eql('GPSInterval');
        expect(setting.header.name).to.be.eql('GPS accuracy');

        // Meta
        expect(setting.meta.type).to.be.eql('RadioButton');
        expect(setting.meta.inputs.length).to.be.eql(4);
        expect(setting.meta.inputs[0].label).to.be.eql('Best - 18h battery life');
        expect(setting.meta.inputs[0].value).to.be.eql(1);
        expect(setting.meta.inputs[1].label).to.be.eql('Good - 35h battery life');
        expect(setting.meta.inputs[1].value).to.be.eql(10);
        expect(setting.meta.inputs[2].label).to.be.eql('OK - 140h battery life');
        expect(setting.meta.inputs[2].value).to.be.eql(60);
        expect(setting.meta.inputs[3].label).to.be.eql('GPS off');
        expect(setting.meta.inputs[3].value).to.be.eql(0);

        expect(setting.readOnly).to.be.false;

        expect(setting).to.be.deep.eql(getGpsInterval('AmsterdamC'));
      });
      it('Brighton', () => {
        const setting = getGpsInterval('Brighton');

        expect(setting.meta.inputs[0].label).to.be.eql('Best - 10h battery life');
        expect(setting.meta.inputs[1].label).to.be.eql('Good - 25h battery life');
        expect(setting.meta.inputs[2].label).to.be.eql('OK - 80h battery life');
        expect(setting.meta.inputs[3].label).to.be.eql('GPS off');

        expect(setting).to.be.deep.eql(getGpsInterval('BrightonC'));
      });
      it('Cairo', () => {
        const setting = getGpsInterval('Cairo');

        expect(setting.meta.inputs[0].label).to.be.eql('Best - 10h battery life');
        expect(setting.meta.inputs[1].label).to.be.eql('Good - 20h battery life');
        expect(setting.meta.inputs[2].label).to.be.eql('OK - 40h battery life');
        expect(setting.meta.inputs[3].label).to.be.eql('GPS off');

        expect(setting).to.be.deep.eql(getGpsInterval('CairoC'));
      });
      it('Forssa', () => {
        const setting = getGpsInterval('Forssa');

        expect(setting.meta.inputs[0].label).to.be.eql('Best - 10h battery life');
        expect(setting.meta.inputs[1].label).to.be.eql('Good - 16h battery life');
        expect(setting.meta.inputs[2].label).to.be.eql('OK - 30h battery life');
        expect(setting.meta.inputs[3].label).to.be.eql('GPS off');

        expect(setting).to.be.deep.eql(getGpsInterval('ForssaC'));
      });
      it('Gdansk', () => {
        const setting = getGpsInterval('Gdansk');

        expect(setting.meta.inputs[0].label).to.be.eql('Best - 10h battery life');
        expect(setting.meta.inputs[1].label).to.be.eql('Good - 12h battery life');
        expect(setting.meta.inputs[2].label).to.be.eql('OK - 40h battery life');
        expect(setting.meta.inputs[3].label).to.be.eql('GPS off');

        expect(setting).to.be.deep.eql(getGpsInterval('GdanskC'));
      });
      it('Ibiza', () => {
        const setting = getGpsInterval('Ibiza');

        expect(setting.meta.inputs[0].label).to.be.eql('Best - 25h battery life');
        expect(setting.meta.inputs[1].label).to.be.eql('Good - 40h battery life');
        expect(setting.meta.inputs[2].label).to.be.eql('OK - 120h battery life');
        expect(setting.meta.inputs[3].label).to.be.eql('GPS off');

        expect(setting).to.be.deep.eql(getGpsInterval('IbizaC'));
      });
      it('Unknown', () => {
        const setting = getGpsInterval('Unknown');

        expect(setting.meta.inputs[0].label).to.be.eql('TXT_GPS_UNKNOWN_BEST');
        expect(setting.meta.inputs[1].label).to.be.eql('TXT_GPS_UNKNOWN_GOOD');
        expect(setting.meta.inputs[2].label).to.be.eql('TXT_GPS_UNKNOWN_OK');
        expect(setting.meta.inputs[3].label).to.be.eql('GPS off');
      });
    });
    describe('Read-only', () => {
      it('FusedSpeedEnabled', () => {
        const component = new Component('Ibiza', '1.9.0', 'en');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

        const setting = JSON.parse(component.getSetting(null, settingsJson, 'FusedSpeedEnabled'));

        // Value
        expect(setting.value).to.be.true;

        // Header
        expect(setting.header.id).to.be.eql('FusedSpeedEnabled');
        expect(setting.header.name).to.be.eql('FusedSpeed');

        // Meta
        expect(setting.meta.type).to.be.eql('RadioButton');
        expect(setting.meta.inputs.length).to.be.eql(2);
        expect(setting.meta.inputs[0].label).to.be.eql('On');
        expect(setting.meta.inputs[0].value).to.be.true;
        expect(setting.meta.inputs[1].label).to.be.eql('Off');
        expect(setting.meta.inputs[1].value).to.be.false;

        expect(setting.readOnly).to.be.true;
      });
      it('UseAccelerometer', () => {
        const component = new Component('Ibiza', '1.9.0', 'en');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

        const setting = JSON.parse(component.getSetting(null, settingsJson, 'UseAccelerometer'));

        // Value
        expect(setting.value).to.be.true;

        // Header
        expect(setting.header.id).to.be.eql('UseAccelerometer');
        expect(setting.header.name).to.be.eql('Wrist cadence');

        // Meta
        expect(setting.meta.type).to.be.eql('RadioButton');
        expect(setting.meta.inputs.length).to.be.eql(2);
        expect(setting.meta.inputs[0].label).to.be.eql('On');
        expect(setting.meta.inputs[0].value).to.be.true;
        expect(setting.meta.inputs[1].label).to.be.eql('Off');
        expect(setting.meta.inputs[1].value).to.be.false;

        expect(setting.readOnly).to.be.true;
      });
      describe('Triggers', () => {
        describe('AutoLap', () => {
          it('Off', () => {
            const component = new Component('Ibiza', '1.9.0', 'en');
            const settings = new WatchSportModeSettings(fs
              .readFileSync(`${testDataPath}/settings.json`, 'utf8'));

            // Remove all triggers
            settings.triggers().length = 0;

            const setting = JSON.parse(component.getSetting(
              null,
              JSON.stringify(settings),
              'AutoLap'
            ));

            // Value
            expect(setting.value.value).to.be.eql(0);
            expect(setting.value.subvalue).to.be.undefined;

            // Header
            expect(setting.header.id).to.be.eql('AutoLap');
            expect(setting.header.name).to.be.eql('Autolap');

            // Meta
            expect(setting.meta.type).to.be.eql('Select');
            expect(setting.meta.inputs.length).to.be.eql(3);
            expect(setting.meta.inputs[0].label).to.be.eql('Off');
            expect(setting.meta.inputs[0].value).to.be.eql(0);

            expect(setting.meta.inputs[1].label).to.be.eql('Distance');
            expect(setting.meta.inputs[1].value).to.be.eql(1);
            expect(setting.meta.inputs[1].subValue.type).to.be.eql('InputNumber');
            expect(setting.meta.inputs[1].subValue.default).to.be.eql(0);
            expect(setting.meta.inputs[1].subValue.min).to.be.eql(100);
            expect(setting.meta.inputs[1].subValue.max).to.be.eql(100000);

            expect(setting.meta.inputs[2].label).to.be.eql('Duration');
            expect(setting.meta.inputs[2].value).to.be.eql(2);
            expect(setting.meta.inputs[2].subValue.type).to.be.eql('InputTime');
            expect(setting.meta.inputs[2].subValue.default).to.be.eql(600);
            expect(setting.meta.inputs[2].subValue.min).to.be.eql(30);
            expect(setting.meta.inputs[2].subValue.max).to.be.eql(36000);

            expect(setting.readOnly).to.be.false;
          });
          it('CumulativeDistance', () => {
            const component = new Component('Ibiza', '1.9.0', 'en');
            const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

            const setting = JSON.parse(component.getSetting(null, settingsJson, 'AutoLap'));

            // Value
            expect(setting.value.value).to.be.eql(1);
            expect(setting.value.subvalue.metric).to.be.eql(1000);
            expect(setting.value.subvalue.imperial).to.be.closeTo(1609.3, 0.1);
          });
          it('CumulativeDuration', () => {
            const component = new Component('Ibiza', '1.9.0', 'en');
            const settings = new WatchSportModeSettings(fs
              .readFileSync(`${testDataPath}/settings.json`, 'utf8'));

            // Remove all triggers first
            settings.triggers().length = 0;
            // Add duration trigger
            settings.triggers().push({
              ActionsOnFall: null,
              ActionsOnRise: [{
                Type: 'AutoLap',
              }],
              Enabled: 2,
              LimitImperial: 1200,
              LimitMetric: 1200,
              Type: 'CumulativeDuration',
            });

            const setting = JSON
              .parse(component.getSetting(null, JSON.stringify(settings), 'AutoLap'));

            // Value
            expect(setting.value.value).to.be.eql(2);
            expect(setting.value.subvalue.metric).to.be.eql(1200);
            expect(setting.value.subvalue.imperial).to.be.eql(1200);
          });
        });
        describe('Vertical', () => {
          it('On', () => {
            const component = new Component('Ibiza', '2.2.0', 'en');
            const settings = new WatchSportModeSettings(fs
              .readFileSync(`${testDataPath}/settings.json`, 'utf8'));

            // Remove all triggers first
            settings.triggers().length = 0;
            // Add duration trigger
            settings.triggers().push({
              ActionsOnFall: null,
              ActionsOnRise: [{
                Type: 'Downhill',
              }],
              Enabled: 2,
              LimitImperial: 0,
              LimitMetric: 0,
              Type: 'ThresholdDownhill',
            });

            const setting = JSON.parse(component.getSetting(
              null,
              JSON.stringify(settings),
              'Downhill+Uphill'
            ));

            // Value
            expect(setting.value.value).to.be.eql(1);
            expect(setting.value.subvalue).to.be.undefined;

            // Header
            expect(setting.header.id).to.be.eql('Downhill+Uphill');
            expect(setting.header.name).to.be.eql('Vertical lap');

            // Meta
            expect(setting.meta.type).to.be.eql('Select');
            expect(setting.meta.inputs.length).to.be.eql(2);
            expect(setting.meta.inputs[0].label).to.be.eql('Off');
            expect(setting.meta.inputs[0].value).to.be.eql(0);

            expect(setting.meta.inputs[1].label).to.be.eql('Downhill');
            expect(setting.meta.inputs[1].value).to.be.eql(1);
            expect(setting.meta.inputs[1].subValue.type).to.be.eql('None');
            expect(setting.meta.inputs[1].subValue.default).to.be.null;

            expect(setting.readOnly).to.be.false;
          });
          it('Off', () => {
            const component = new Component('Ibiza', '2.2.0', 'en');
            const settings = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

            const setting = JSON.parse(component.getSetting(
              null,
              settings,
              'Downhill+Uphill'
            ));

            // Value
            expect(setting.value.value).to.be.eql(0);
            expect(setting.value.subvalue).to.be.undefined;
          });
        });
      });
    });
  });
  describe('changeSetting', () => {
    describe('Name', () => {
      it('Valid', () => {
        const component = new Component('Ibiza', '1.9.0', 'fi');
        const groupJson = fs.readFileSync(`${testDataPath}/group-custom.json`, 'utf8');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

        // 32 bytes is the maximum length
        const newName = '01234567890123456789012345678901';
        expect(newName.length).to.be.eql(32);
        const mode = JSON.parse(component.changeSetting(groupJson, settingsJson, 'Name', newName));
        const group = new WatchSportModeGroup(JSON.stringify(mode.group.data));

        expect(group.name).to.be.eql(newName);

        // Settings should be unaffected
        expect(JSON.parse(settingsJson)).to.deep.eql(mode.modes[0].settings);
      });
      it('Too long', () => {
        const component = new Component('Ibiza', '1.9.0', 'fi');
        const groupJson = fs.readFileSync(`${testDataPath}/group-custom.json`, 'utf8');
        const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

        // Name is 32 characters but 33 bytes long which is greater than allowed 32 B.
        // Name consist of 31 single byte characters and one &Auml; which is presented
        // using two bytes in UTF-8 bringing the total to 33 bytes.
        const Auml = String.fromCharCode(196);
        const newName = `0123456789012345678901234567890${Auml}`;
        expect(newName.length).to.be.eql(32);
        const mode = JSON.parse(component.changeSetting(groupJson, settingsJson, 'Name', newName));

        const group = new WatchSportModeGroup(JSON.stringify(mode.group.data));

        // Name should be unchanged since input was invalid (too long in bytes)
        expect(group.name).to.be.eql('My Running Mode');

        // Settings should be unaffected
        expect(JSON.parse(settingsJson)).to.deep.eql(mode.modes[0].settings);
      });
    });
    it('UseFootPOD', () => {
      const component = new Component('Ibiza', '1.9.0', 'fi');
      const groupJson = fs.readFileSync(`${testDataPath}/group.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const newValue = false;
      const mode = JSON.parse(component.changeSetting(groupJson, settingsJson, 'UseFootPOD', newValue));
      const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

      expect(settings.settings().UseFootPOD).to.be.eql(newValue);

      // Group should be unaffected
      expect(JSON.parse(groupJson)).to.deep.eql(mode.group.data);
    });
    it('AltiBaroMode', () => {
      const component = new Component('Ibiza', '1.9.0', 'fi');
      const groupJson = fs.readFileSync(`${testDataPath}/group.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const newValue = 2;
      const mode = JSON.parse(component.changeSetting(groupJson, settingsJson, 'AltiBaroMode', newValue));
      const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

      expect(settings.settings().AltiBaroMode).to.be.eql(newValue);

      // Group should be unaffected
      expect(JSON.parse(groupJson)).to.deep.eql(mode.group.data);
    });
    it('DisplayLowColor', () => {
      const component = new Component('Ibiza', '1.9.0', 'fi');
      const groupJson = fs.readFileSync(`${testDataPath}/group.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const newValue = false;
      const mode = JSON.parse(component.changeSetting(groupJson, settingsJson, 'DisplayLowColor', newValue));
      const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

      expect(settings.settings().DisplayLowColor).to.be.eql(newValue);

      // Group should be unaffected
      expect(JSON.parse(groupJson)).to.deep.eql(mode.group.data);
    });
    it('DisplayTimeout', () => {
      const component = new Component('Ibiza', '1.9.0', 'fi');
      const groupJson = fs.readFileSync(`${testDataPath}/group.json`, 'utf8');
      const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

      const newValue = 10;
      const mode = JSON.parse(component.changeSetting(groupJson, settingsJson, 'DisplayTimeout', newValue));
      const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

      expect(settings.settings().DisplayTimeout).to.be.eql(newValue);

      // Group should be unaffected
      expect(JSON.parse(groupJson)).to.deep.eql(mode.group.data);
    });
    describe('Triggers', () => {
      describe('AutoLap', () => {
        it('Off', () => {
          const component = new Component('Ibiza', '2.0.0', 'en');
          const groupJson = fs.readFileSync(`${testDataPath}/group.json`, 'utf8');
          const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

          const newValue = JSON.stringify({ value: 0 });
          const mode = JSON.parse(component.changeSetting(groupJson, settingsJson, 'AutoLap', newValue));
          const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

          const triggers = settings.settings().Triggers;
          expect(triggers.length).to.be.eql(1);
          expect(triggers[0].Type).to.be.eql('ManualLap');

          // Group should be unaffected
          expect(JSON.parse(groupJson)).to.deep.eql(mode.group.data);
        });
        describe('Change distance', () => {
          const changeDistanceTest = (metric, imperial, expectedMetric, expectedImperial) => {
            const component = new Component('Ibiza', '2.0.0', 'en');
            const groupJson = fs.readFileSync(`${testDataPath}/group.json`, 'utf8');
            const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

            const newValue = JSON.stringify({
              value: 1, subvalue: { metric, imperial },
            });
            const mode = JSON.parse(component.changeSetting(groupJson, settingsJson, 'AutoLap', newValue));
            const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

            const triggers = settings.settings().Triggers;
            expect(triggers.length).to.be.eql(2);
            expect(triggers[0].Type).to.be.eql('ManualLap');

            expect(triggers[1].Type).to.be.eql('CumulativeDistance');
            expect(triggers[1].Enabled).to.be.eql(2);
            expect(triggers[1].LimitMetric).to.be.closeTo(expectedMetric, 0.001);
            expect(triggers[1].LimitImperial).to.be.closeTo(expectedImperial, 0.001);
            expect(triggers[1].ActionsOnFall).to.be.null;
            expect(triggers[1].ActionsOnRise.length).to.be.eql(1);
            expect(Object.keys(triggers[1].ActionsOnRise[0]).length).to.be.eql(1);
            expect(triggers[1].ActionsOnRise[0].Type).to.be.eql('AutoLap');

            // Settings should be unaffected
            expect(JSON.parse(groupJson)).to.deep.eql(mode.group.data);
          };

          it('metric', () => {
            changeDistanceTest(500, null, 500, 804.672);
          });
          it('imperial', () => {
            changeDistanceTest(null, 3218.688, 2000, 3218.688);
          });
          it('both', () => {
            changeDistanceTest(100, 200, 100, 200);
          });
        });
        it('Change duration', () => {
          const component = new Component('Ibiza', '2.0.0', 'en');
          const groupJson = fs.readFileSync(`${testDataPath}/group.json`, 'utf8');
          const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

          const newValue = JSON.stringify({
            value: 2, subvalue: { metric: 600, imperial: 600 },
          });
          const mode = JSON.parse(component.changeSetting(groupJson, settingsJson, 'AutoLap', newValue));
          const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

          const triggers = settings.settings().Triggers;
          expect(triggers.length).to.be.eql(2);
          expect(triggers[0].Type).to.be.eql('ManualLap');

          expect(triggers[1].Type).to.be.eql('CumulativeDuration');
          expect(triggers[1].Enabled).to.be.eql(2);
          expect(triggers[1].LimitMetric).to.be.eql(600);
          expect(triggers[1].LimitImperial).to.be.eql(600);
          expect(triggers[1].ActionsOnFall).to.be.null;
          expect(triggers[1].ActionsOnRise.length).to.be.eql(1);
          expect(Object.keys(triggers[1].ActionsOnRise[0]).length).to.be.eql(1);
          expect(triggers[1].ActionsOnRise[0].Type).to.be.eql('AutoLap');

          // Group should be unaffected
          expect(JSON.parse(groupJson)).to.deep.eql(mode.group.data);
        });
      });
      describe('Vertical', () => {
        it('On and Off', () => {
          const component = new Component('Ibiza', '2.0.0', 'en');
          const groupJson = fs.readFileSync(`${testDataPath}/group.json`, 'utf8');
          const settingsJson = fs.readFileSync(`${testDataPath}/settings.json`, 'utf8');

          const mode = JSON.parse(component.changeSetting(
            groupJson,
            settingsJson,
            'Downhill+Uphill',
            JSON.stringify({ value: 1 })
          ));
          const settings = new WatchSportModeSettings(JSON.stringify(mode.modes[0].settings));

          const triggers = settings.settings().Triggers;
          expect(triggers.length).to.be.eql(3);
          expect(triggers[0].Type).to.be.eql('ManualLap');
          expect(triggers[1].Type).to.be.eql('CumulativeDistance');

          expect(triggers[2].Type).to.be.eql('ThresholdDownhill');
          expect(triggers[2].Enabled).to.be.eql(2);
          expect(triggers[2].LimitMetric).to.be.null;
          expect(triggers[2].LimitImperial).to.be.null;
          expect(triggers[2].ActionsOnFall).to.be.null;
          expect(triggers[2].ActionsOnRise.length).to.be.eql(1);
          expect(Object.keys(triggers[1].ActionsOnRise[0]).length).to.be.eql(1);
          expect(triggers[2].ActionsOnRise[0].Type).to.be.eql('Downhill');

          // Group should be unaffected
          expect(JSON.parse(groupJson)).to.deep.eql(mode.group.data);

          // Check that the vertical trigger can be removed without affecting other triggers
          const removedMode = JSON.parse(component.changeSetting(
            groupJson,
            JSON.stringify(settings),
            'Downhill+Uphill',
            JSON.stringify({ value: 0 })
          ));
          const removedSettings = new WatchSportModeSettings(JSON
            .stringify(removedMode.modes[0].settings));
          const removedTriggers = removedSettings.settings().Triggers;
          expect(removedTriggers.length).to.be.eql(2);
          expect(removedTriggers[0].Type).to.be.eql('ManualLap');
          expect(removedTriggers[1].Type).to.be.eql('CumulativeDistance');
        });
      });
    });
  });
});
