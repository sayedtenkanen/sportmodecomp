import { expect } from 'chai';
import PackageIds from '../sim-tools/packageids/packageIds.json';
import Localization from '../src/localization/localization';

describe('PackageIDs', () => {
  it('translations', () => {
    // S3F specific value is not available for the time being - will be updated
    // when we get the translation. TXT_AIR_MODE is dive specific which was not
    // ever really used.
    const knownMissing = [
      'TXT_FITNESS_MODE',
      'TXT_AIR_MODE',
    ];

    PackageIds.forEach((item) => {
      if (!knownMissing.includes(item.nameId)) {
        const translation = Localization.translate(item.nameId, 'fi');
        expect(translation.startsWith('TXT_')).to.be.false;
      }
    });
  });
});
