import { expect } from 'chai';
import fs from 'fs';
import WatchSportModeDisplays from '../src/datamodels/watchsportmode/displays';

describe('WatchSportModeDisplays', () => {
  const testDataPath = 'test/data';
  it('templates', () => {
    const displayJson = fs.readFileSync(`${testDataPath}/displays.json`, 'utf8');
    const displays = new WatchSportModeDisplays(displayJson);

    expect(displays.templates().length).to.be.eql(2);
    expect(displays.templates()[0]).to.be.eql('Data4Fields');
    expect(displays.templates()[1]).to.be.eql('Interval37');
  });
  it('display filter', () => {
    const testDisplay = {
      TemplateType: 'type',
      Name: 'name',
      ShowTimeout: 'timeout',
      Empty: null,
      Fields: [{
        ID: 'id',
        Empty: null,
      }],
    };
    const filtered = WatchSportModeDisplays.filterDisplay(testDisplay);

    expect(filtered.TemplateType).to.be.eql('type');
    expect(filtered.Name).to.be.undefined;
    expect(filtered.ShowTimeout).to.be.undefined;
    expect(filtered.Empty).to.be.undefined;
    expect(filtered.Fields[0].ID).to.be.eql('id');
    expect(filtered.Fields[0].Empty).to.be.undefined;
  });
});
