import { expect } from 'chai';
import fs from 'fs';
import SportModes from '../src/sportmodes/sportmodes';

describe('SportModes', () => {
  const testDataPath = 'test/data';
  it('list', () => {
    // Test data in actual ESW format
    const watchSportModeList = fs.readFileSync(`${testDataPath}/groups.json`, 'utf8');
    const modes = SportModes.list(watchSportModeList, 'fi');

    expect(modes.length).to.be.eql(2);

    const firstMode = modes[0];
    expect(firstMode.id).to.be.eql(0xC0000000 | 0);
    expect(firstMode.name).to.be.eql('NewMode');
    expect(firstMode.factorymode).to.be.false;
    expect(firstMode.activity.id).to.be.eql(5);
    expect(firstMode.activity.type).to.be.null;
    expect(firstMode.modeIds.length).to.be.eql(1);
    expect(firstMode.modeIds[0]).to.be.eql(0xC0000000 | 0);

    const secondMode = modes[1];
    expect(secondMode.id).to.be.eql(-2147287039);
    expect(secondMode.name).to.be.null;
    expect(secondMode.factorymode).to.be.true;
    expect(secondMode.activity.id).to.be.eql(3);
    expect(secondMode.activity.type).to.be.eql('Perustaso');
    expect(secondMode.modeIds.length).to.be.eql(1);
    expect(secondMode.modeIds[0]).to.be.eql(60001);
  });
  it('newModeId', () => {
    // Test data in actual ESW format
    const watchSportModeList = fs.readFileSync(`${testDataPath}/groups.json`, 'utf8');
    const newId = SportModes.newModeId(watchSportModeList);
    /* eslint no-bitwise: ["error", { "allow": ["|"] }] */
    expect(newId).to.be.eql(0xC0000000 | 1);
    const watchSportModeListLarge = fs.readFileSync(`${testDataPath}/groups-large.json`, 'utf8');
    const newId2 = SportModes.newModeId(watchSportModeListLarge);
    /* eslint no-bitwise: ["error", { "allow": ["|"] }] */
    expect(newId2).to.be.eql(0xC0000000 | 2);
  });
});
