import fs from 'fs';
import * as Displays from '../src/constants/displayIds';

export const simSupportsGraphs = () => {
  const supportedTemplates =
    JSON.parse(fs.readFileSync(
      'suunto-information-model/Specifications/CustomModes/DisplayTemplates.json',
      'utf8'
    ));
  return supportedTemplates[Displays.SIMPLE_LINE_CHART] !== undefined;
};

export const simSupportsZones = () => {
  const supportedTemplates =
    JSON.parse(fs.readFileSync(
      'suunto-information-model/Specifications/CustomModes/DisplayTemplates.json',
      'utf8'
    ));
  return supportedTemplates.Zones8Fields !== undefined;
};

export const simSupportsLapTable = () => {
  const supportedTemplates =
    JSON.parse(fs.readFileSync(
      'suunto-information-model/Specifications/CustomModes/DisplayTemplates.json',
      'utf8'
    ));
  return supportedTemplates.LapTable !== undefined;
};

export const simSupportsKyoto = () => {
  let supported = false;
  const baseModes =
    JSON.parse(fs.readFileSync(
      'suunto-information-model/Specifications/CustomModes/BaseModes.json',
      'utf8'
    ));
  Object.keys(baseModes).forEach((key) => {
    baseModes[key].Modes.forEach((mode) => {
      if (mode.Compatibility.Kyoto !== undefined) {
        supported = true;
      }
    });
  });

  return supported;
};
