import { expect } from 'chai';
import Packages from '../src/packages/packages';
import Display from '../src/datamodels/display';
import Field from '../src/datamodels/field';
import FieldSection from '../src/datamodels/fieldsection';
import * as Fields from '../src/constants/fieldIds';
import * as SupportHelpers from './supportHelpers';

const fieldsDisplays = [
  new Display('Data3Fields', '3 Fields', false, '', '', true, true),
  new Display('Data4Fields', '4 Fields', false, '', '', true, true),
  new Display('Data5Fields', '5 Fields', false, '', '', true, true),
  new Display('Data7Fields', '7 Fields', false, '', '', true, true),
];
const fieldsDisplaysHelsinki = [
  new Display('Data3Fields', '3 Fields', false, '', '', true, true),
  new Display('Data4Fields', '4 Fields', false, '', '', true, true),
  new Display('Data5Fields', '5 Fields', false, '', '', true, true),
  new Display('Data7Fields', '7 Fields', false, '', '', true, true),
  new Display('Data3Carousel', 'Carousel', false, '', '', true, true),
];
const fieldsDisplaysHelsinkiWoCarousel = [
  new Display('Data3Fields', '3 Fields', false, '', '', true, true),
  new Display('Data4Fields', '4 Fields', false, '', '', true, true),
  new Display('Data5Fields', '5 Fields', false, '', '', true, true),
  new Display('Data7Fields', '7 Fields', false, '', '', true, true),
];
const columnsDisplays = [
  new Display('Table2Columns', '2 Columns', false, '', '', true, true),
  new Display('Table3Columns', '3 Columns', false, '', '', true, true),
];
const intervalDisplays = [
  new Display('TXT_DURATION_HR_INTERVAL', 'Duration with heart rate', true, '', '', true, true),
  new Display('TXT_POWER', 'Power', true, '', '', true, true),
  new Display('TXT_POWER_NO_DISTANCE_INTERVAL', 'Power without distance', true, '', '', true, true),
  new Display('TXT_SPEED', 'Speed', true, '', '', true, true),
  new Display('TXT_SPEED_PACE_INTERVAL', 'Speed and pace', true, '', '', true, true),
];
const intervalDisplaysHelsinki = [
  new Display('TXT_DURATION_HR_INTERVAL', 'Duration with heart rate', true, '', '', true, true),
  new Display('TXT_SPEED', 'Speed', true, '', '', true, true),
  new Display('TXT_SPEED_PACE_INTERVAL', 'Speed and pace', true, '', '', true, true),
];
const fieldsForRunning = [
  new FieldSection('TXT_CADENCE', 'Cadence', [
    new Field('TXT_AVG_CADENCE', 'Average cadence'),
    new Field('TXT_CADENCE', 'Cadence'),
    new Field('TXT_LAP_AVG_CADENCE', 'Lap avg. cadence'),
    new Field('TXT_LAP_MAX_CADENCE', 'Lap max. cadence'),
    new Field('TXT_MAX_CADENCE', 'Max. cadence'),
  ]),
  new FieldSection('TXT_DISTANCE', 'Distance', [
    new Field('TXT_CURRENT_ACTIVITY_DISTANCE', 'Current activity distance'),
    new Field('TXT_CURRENT_INTERVAL_DISTANCE', 'Current interval distance'),
    new Field('TXT_DISTANCE', 'Distance'),
    new Field('TXT_LAP_DISTANCE', 'Lap distance'),
    new Field('TXT_NAUTICAL_DISTANCE', 'Nautical distance'),
  ]),
  new FieldSection('TXT_DURATION_AND_TIME', 'Duration and time', [
    new Field('TXT_CURRENT_ACTIVITY_DURATION', 'Current activity duration'),
    new Field('TXT_CURRENT_INTERVAL_DURATION', 'Current interval duration'),
    new Field('TXT_DURATION', 'Duration'),
    new Field('TXT_LAP_DURATION', 'Lap duration'),
    new Field('TXT_PREVIOUS_ACTIVITY_DURATION', 'Previous activity duration'),
    new Field('TXT_TIMEOFDAY', 'Time of day'),
  ]),
  new FieldSection('TXT_HEART_RATE', 'Heart rate', [
    new Field('TXT_AVERAGE_HEART_RATE', 'Average heart rate'),
    new Field('TXT_HEART_RATE', 'Heart rate'),
    new Field('TXT_INTERVAL_AVG_HR', 'Interval avg. HR'),
    new Field('TXT_INTERVAL_MAX_HR', 'Interval max. HR'),
    new Field('TXT_LAP_AVG_HEART_RATE', 'Lap avg. heart rate'),
    new Field('TXT_LAP_MAX_HEART_RATE', 'Lap max. heart rate'),
    new Field('TXT_LAP_MIN_HEART_RATE', 'Lap min. heart rate'),
  ]),
  new FieldSection('TXT_INT_MODE', 'Interval', [
    new Field('TXT_CURRENT_INTERVAL_DISTANCE', 'Current interval distance'),
    new Field('TXT_CURRENT_INTERVAL_DURATION', 'Current interval duration'),
    new Field('TXT_CURRENT_INTERVAL_REP', 'Current interval repetition'),
    new Field('TXT_INTERVAL_AVG_PACE', 'Interval avg pace'),
    new Field('TXT_INTERVAL_AVG_HR', 'Interval avg. HR'),
    new Field('TXT_INTERVAL_AVG_POWER', 'Interval avg. power'),
    new Field('TXT_INTERVAL_AVG_SPEED', 'Interval avg. speed'),
    new Field('TXT_INTERVAL_MAX_HR', 'Interval max. HR'),
  ]),
  new FieldSection('TXT_OTHER', 'Other', [
    new Field('TXT_EMPTY', 'Empty'),
    new Field('TXT_LAP_NUMBER', 'Lap number'),
  ]),
  new FieldSection('TXT_PHYSIOLOGY', 'Physiology', [
    new Field('TXT_CALORIES', 'Calories'),
    new Field('TXT_EPOC', 'EPOC'),
    new Field('TXT_PTE', 'PTE'),
    new Field('TXT_RECOVERY_TIME', 'Recovery time'),
  ]),
  new FieldSection('TXT_POWER', 'Power', [
    new Field('TXT_AVG_POWER', 'Average power'),
    new Field('TXT_INTERVAL_AVG_POWER', 'Interval avg. power'),
    new Field('TXT_LAP_AVG_POWER', 'Lap avg. power'),
    new Field('TXT_LAP_MAX_POWER', 'Lap max. power'),
    new Field('TXT_MAX_POWER', 'Max. power'),
    new Field('TXT_POWER_10S', 'Power 10 sec'),
    new Field('TXT_POWER_3S', 'Power 3 sec'),
    new Field('TXT_POWER_30S', 'Power 30 sec'),
  ]),
  new FieldSection('TXT_SPEED_PACE_INTERVAL', 'Speed and pace', [
    new Field('TXT_AVG_NAUTICAL_SPEED', 'Average nautical speed'),
    new Field('TXT_AVG_PACE', 'Average pace'),
    new Field('TXT_AVERAGE_SPEED', 'Average speed'),
    new Field('TXT_INTERVAL_AVG_PACE', 'Interval avg pace'),
    new Field('TXT_INTERVAL_AVG_SPEED', 'Interval avg. speed'),
    new Field('TXT_LAP_AVERAGE_PACE', 'Lap avg. pace'),
    new Field('TXT_LAP_AVG_SPEED', 'Lap avg. speed'),
    new Field('TXT_LAP_MAX_PACE', 'Lap max. pace'),
    new Field('TXT_LAP_MAX_SPEED', 'Lap max. speed'),
    new Field('TXT_MAX_PACE', 'Max. pace'),
    new Field('TXT_MAX_SPEED', 'Max. speed'),
    new Field('TXT_NAUTICAL_SPEED', 'Nautical speed'),
    new Field('TXT_PACE', 'Pace'),
    new Field('TXT_ROWING_PACE', 'Rowing pace'),
    new Field('TXT_SPEED', 'Speed'),
  ]),
  new FieldSection('TXT_TEMPERATURE', 'Temperature', [
    new Field('TXT_AVG_TEMPERATURE', 'Avg. temperature'),
    new Field('TXT_LAP_TEMPERATURE', 'Lap temperature'),
    new Field('TXT_MAX_TEMPERATURE', 'Max temperature'),
    new Field('TXT_TEMPERATURE', 'Temperature'),
  ]),
  new FieldSection('TXT_VERTICAL', 'Vertical', [
    new Field('TXT_ALTITUDE', 'Altitude'),
    new Field('TXT_ASCENT', 'Ascent'),
    new Field('TXT_AVG_VERTICAL_SPEED_FAST', 'Avg vertical speed m/min (ft/min)'),
    new Field('TXT_DESCENT', 'Descent'),
    new Field('TXT_LAP_ASCENT', 'Lap ascent'),
    new Field('TXT_LAP_AVG_VERTICAL_SPEED_FAST', 'Lap avg vertical speed m/min (ft/min)'),
    new Field('TXT_LAP_DESCENT', 'Lap descent'),
    new Field('TXT_LAP_MAX_ALTITUDE', 'Lap max. altitude'),
    new Field('TXT_LAP_MIN_ALTITUDE', 'Lap min. altitude'),
    new Field('TXT_MAX_ALTITUDE', 'Max. altitude'),
    new Field('TXT_MIN_ALTITUDE', 'Min. altitude'),
    new Field('TXT_VERTICAL_SPEED_FAST', 'Vertical speed m/min (ft/min)'),
  ]),
];
const fieldsForRunningSingleSection = [
  new FieldSection('TXT_FIELDS', 'Fields', [
    new Field('TXT_ALTITUDE', 'Altitude'),
    new Field('TXT_ASCENT', 'Ascent'),
    new Field('TXT_AVG_CADENCE', 'Average cadence'),
    new Field('TXT_AVERAGE_HEART_RATE', 'Average heart rate'),
    new Field('TXT_AVG_NAUTICAL_SPEED', 'Average nautical speed'),
    new Field('TXT_AVG_PACE', 'Average pace'),
    new Field('TXT_AVG_POWER', 'Average power'),
    new Field('TXT_AVERAGE_SPEED', 'Average speed'),
    new Field('TXT_AVG_VERTICAL_SPEED_FAST', 'Avg vertical speed m/min (ft/min)'),
    new Field('TXT_AVG_TEMPERATURE', 'Avg. temperature'),
    new Field('TXT_CADENCE', 'Cadence'),
    new Field('TXT_CALORIES', 'Calories'),
    new Field('TXT_CURRENT_ACTIVITY_DISTANCE', 'Current activity distance'),
    new Field('TXT_CURRENT_ACTIVITY_DURATION', 'Current activity duration'),
    new Field('TXT_CURRENT_INTERVAL_DISTANCE', 'Current interval distance'),
    new Field('TXT_CURRENT_INTERVAL_DURATION', 'Current interval duration'),
    new Field('TXT_CURRENT_INTERVAL_REP', 'Current interval repetition'),
    new Field('TXT_DESCENT', 'Descent'),
    new Field('TXT_DISTANCE', 'Distance'),
    new Field('TXT_DURATION', 'Duration'),
    new Field('TXT_EMPTY', 'Empty'),
    new Field('TXT_EPOC', 'EPOC'),
    new Field('TXT_HEART_RATE', 'Heart rate'),
    new Field('TXT_INTERVAL_AVG_PACE', 'Interval avg pace'),
    new Field('TXT_INTERVAL_AVG_HR', 'Interval avg. HR'),
    new Field('TXT_INTERVAL_AVG_POWER', 'Interval avg. power'),
    new Field('TXT_INTERVAL_AVG_SPEED', 'Interval avg. speed'),
    new Field('TXT_INTERVAL_MAX_HR', 'Interval max. HR'),
    new Field('TXT_LAP_ASCENT', 'Lap ascent'),
    new Field('TXT_LAP_AVG_VERTICAL_SPEED_FAST', 'Lap avg vertical speed m/min (ft/min)'),
    new Field('TXT_LAP_AVG_CADENCE', 'Lap avg. cadence'),
    new Field('TXT_LAP_AVG_HEART_RATE', 'Lap avg. heart rate'),
    new Field('TXT_LAP_AVERAGE_PACE', 'Lap avg. pace'),
    new Field('TXT_LAP_AVG_POWER', 'Lap avg. power'),
    new Field('TXT_LAP_AVG_SPEED', 'Lap avg. speed'),
    new Field('TXT_LAP_DESCENT', 'Lap descent'),
    new Field('TXT_LAP_DISTANCE', 'Lap distance'),
    new Field('TXT_LAP_DURATION', 'Lap duration'),
    new Field('TXT_LAP_MAX_ALTITUDE', 'Lap max. altitude'),
    new Field('TXT_LAP_MAX_CADENCE', 'Lap max. cadence'),
    new Field('TXT_LAP_MAX_HEART_RATE', 'Lap max. heart rate'),
    new Field('TXT_LAP_MAX_PACE', 'Lap max. pace'),
    new Field('TXT_LAP_MAX_POWER', 'Lap max. power'),
    new Field('TXT_LAP_MAX_SPEED', 'Lap max. speed'),
    new Field('TXT_LAP_MIN_ALTITUDE', 'Lap min. altitude'),
    new Field('TXT_LAP_MIN_HEART_RATE', 'Lap min. heart rate'),
    new Field('TXT_LAP_NUMBER', 'Lap number'),
    new Field('TXT_LAP_TEMPERATURE', 'Lap temperature'),
    new Field('TXT_MAX_TEMPERATURE', 'Max temperature'),
    new Field('TXT_MAX_ALTITUDE', 'Max. altitude'),
    new Field('TXT_MAX_CADENCE', 'Max. cadence'),
    new Field('TXT_MAX_PACE', 'Max. pace'),
    new Field('TXT_MAX_POWER', 'Max. power'),
    new Field('TXT_MAX_SPEED', 'Max. speed'),
    new Field('TXT_MIN_ALTITUDE', 'Min. altitude'),
    new Field('TXT_NAUTICAL_DISTANCE', 'Nautical distance'),
    new Field('TXT_NAUTICAL_SPEED', 'Nautical speed'),
    new Field('TXT_PACE', 'Pace'),
    new Field('TXT_POWER_10S', 'Power 10 sec'),
    new Field('TXT_POWER_3S', 'Power 3 sec'),
    new Field('TXT_POWER_30S', 'Power 30 sec'),
    new Field('TXT_PREVIOUS_ACTIVITY_DURATION', 'Previous activity duration'),
    new Field('TXT_PTE', 'PTE'),
    new Field('TXT_RECOVERY_TIME', 'Recovery time'),
    new Field('TXT_ROWING_PACE', 'Rowing pace'),
    new Field('TXT_SPEED', 'Speed'),
    new Field('TXT_TEMPERATURE', 'Temperature'),
    new Field('TXT_TIMEOFDAY', 'Time of day'),
    new Field('TXT_VERTICAL_SPEED_FAST', 'Vertical speed m/min (ft/min)'),
  ]),
];
const fieldsForSwimming = [
  new FieldSection('TXT_DISTANCE', 'Distance', [
    new Field('TXT_CURRENT_ACTIVITY_DISTANCE', 'Current activity distance'),
    new Field('TXT_CURRENT_INTERVAL_SWIMDISTANCE', 'Current interval swim distance'),
    new Field('TXT_INTERVAL_SWIMDISTANCE', 'Interval swim distance'),
    new Field('TXT_LAP_DISTANCE', 'Lap distance'),
    new Field('TXT_NAUTICAL_DISTANCE', 'Nautical distance'),
    new Field('TXT_SWIM_DISTANCE', 'Swim distance'),
  ]),
  new FieldSection('TXT_DURATION_AND_TIME', 'Duration and time', [
    new Field('TXT_DURATION', 'Duration'),
    new Field('TXT_INTERVAL_DURATION', 'Interval duration'),
    new Field('TXT_LAP_DURATION', 'Lap duration'),
    new Field('TXT_PREVIOUS_ACTIVITY_DURATION', 'Previous activity duration'),
    new Field('TXT_TIMEOFDAY', 'Time of day'),
  ]),
  new FieldSection('TXT_HEART_RATE', 'Heart rate', [
    new Field('TXT_AVERAGE_HEART_RATE', 'Average heart rate'),
    new Field('TXT_HEART_RATE', 'Heart rate'),
    new Field('TXT_INTERVAL_AVERAGE_HEART_RATE', 'Interval average heart rate'),
    new Field('TXT_LAP_AVG_HEART_RATE', 'Lap avg. heart rate'),
    new Field('TXT_LAP_MAX_HEART_RATE', 'Lap max. heart rate'),
    new Field('TXT_LAP_MIN_HEART_RATE', 'Lap min. heart rate'),
  ]),
  new FieldSection('TXT_INT_MODE', 'Interval', [
    new Field('TXT_CURRENT_SWIM_INTERVAL_NUMBER', 'Current interval number'),
    new Field('TXT_CURRENT_INTERVAL_SWIMDISTANCE', 'Current interval swim distance'),
    new Field('TXT_INTERVAL_AVERAGE_HEART_RATE', 'Interval average heart rate'),
    new Field('TXT_INTERVAL_AVERAGE_STROKE_RATE', 'Interval average stroke rate'),
    new Field('TXT_INTERVAL_AVG_SWIM_PACE', 'Interval average swim pace'),
    new Field('TXT_INTERVAL_DURATION', 'Interval duration'),
    new Field('TXT_INTERVAL_SWIMDISTANCE', 'Interval swim distance'),
  ]),
  new FieldSection('TXT_OTHER', 'Other', [
    new Field('TXT_EMPTY', 'Empty'),
    new Field('TXT_LAP_NUMBER', 'Lap number'),
  ]),
  new FieldSection('TXT_PHYSIOLOGY', 'Physiology', [
    new Field('TXT_CALORIES', 'Calories'),
    new Field('TXT_EPOC', 'EPOC'),
    new Field('TXT_PTE', 'PTE'),
    new Field('TXT_RECOVERY_TIME', 'Recovery time'),
  ]),
  new FieldSection('TXT_SPEED_PACE_INTERVAL', 'Speed and pace', [
    new Field('TXT_AVG_NAUTICAL_SPEED', 'Average nautical speed'),
    new Field('TXT_AVERAGE_SPEED', 'Average speed'),
    new Field('TXT_AVG_SWIM_PACE', 'Average swim pace'),
    new Field('TXT_INTERVAL_AVG_SWIM_PACE', 'Interval average swim pace'),
    new Field('TXT_LAP_AVERAGE_PACE', 'Lap avg. pace'),
    new Field('TXT_LAP_AVG_SPEED', 'Lap avg. speed'),
    new Field('TXT_LAP_MAX_PACE', 'Lap max. pace'),
    new Field('TXT_LAP_MAX_SPEED', 'Lap max. speed'),
    new Field('TXT_MAX_PACE', 'Max. pace'),
    new Field('TXT_MAX_SPEED', 'Max. speed'),
    new Field('TXT_NAUTICAL_SPEED', 'Nautical speed'),
    new Field('TXT_PACE', 'Pace'),
    new Field('TXT_ROWING_PACE', 'Rowing pace'),
    new Field('TXT_SPEED', 'Speed'),
  ]),
  new FieldSection('TXT_SWIMMING', 'Swimming', [
    new Field('TXT_AVERAGE_STROKE_RATE', 'Average stroke rate'),
    new Field('TXT_INTERVAL_AVERAGE_STROKE_RATE', 'Interval average stroke rate'),
    new Field('TXT_SWOLF_POSTFIX', 'SWOLF'),
  ]),
  new FieldSection('TXT_TEMPERATURE', 'Temperature', [
    new Field('TXT_AVG_TEMPERATURE', 'Avg. temperature'),
    new Field('TXT_LAP_TEMPERATURE', 'Lap temperature'),
    new Field('TXT_MAX_TEMPERATURE', 'Max temperature'),
    new Field('TXT_TEMPERATURE', 'Temperature'),
  ]),
  new FieldSection('TXT_VERTICAL', 'Vertical', [
    new Field('TXT_ALTITUDE', 'Altitude'),
    new Field('TXT_LAP_MAX_ALTITUDE', 'Lap max. altitude'),
    new Field('TXT_LAP_MIN_ALTITUDE', 'Lap min. altitude'),
    new Field('TXT_MAX_ALTITUDE', 'Max. altitude'),
    new Field('TXT_MIN_ALTITUDE', 'Min. altitude'),
  ]),
];
const fieldsForSwimmingSingleSection = [
  new FieldSection('TXT_FIELDS', 'Fields', [
    new Field('TXT_ALTITUDE', 'Altitude'),
    new Field('TXT_AVERAGE_HEART_RATE', 'Average heart rate'),
    new Field('TXT_AVG_NAUTICAL_SPEED', 'Average nautical speed'),
    new Field('TXT_AVERAGE_SPEED', 'Average speed'),
    new Field('TXT_AVERAGE_STROKE_RATE', 'Average stroke rate'),
    new Field('TXT_AVG_SWIM_PACE', 'Average swim pace'),
    new Field('TXT_AVG_TEMPERATURE', 'Avg. temperature'),
    new Field('TXT_CALORIES', 'Calories'),
    new Field('TXT_CURRENT_ACTIVITY_DISTANCE', 'Current activity distance'),
    new Field('TXT_CURRENT_SWIM_INTERVAL_NUMBER', 'Current interval number'),
    new Field('TXT_CURRENT_INTERVAL_SWIMDISTANCE', 'Current interval swim distance'),
    new Field('TXT_DURATION', 'Duration'),
    new Field('TXT_EMPTY', 'Empty'),
    new Field('TXT_EPOC', 'EPOC'),
    new Field('TXT_HEART_RATE', 'Heart rate'),
    new Field('TXT_INTERVAL_AVERAGE_HEART_RATE', 'Interval average heart rate'),
    new Field('TXT_INTERVAL_AVERAGE_STROKE_RATE', 'Interval average stroke rate'),
    new Field('TXT_INTERVAL_AVG_SWIM_PACE', 'Interval average swim pace'),
    new Field('TXT_INTERVAL_DURATION', 'Interval duration'),
    new Field('TXT_INTERVAL_SWIMDISTANCE', 'Interval swim distance'),
    new Field('TXT_LAP_AVG_HEART_RATE', 'Lap avg. heart rate'),
    new Field('TXT_LAP_AVERAGE_PACE', 'Lap avg. pace'),
    new Field('TXT_LAP_AVG_SPEED', 'Lap avg. speed'),
    new Field('TXT_LAP_DISTANCE', 'Lap distance'),
    new Field('TXT_LAP_DURATION', 'Lap duration'),
    new Field('TXT_LAP_MAX_ALTITUDE', 'Lap max. altitude'),
    new Field('TXT_LAP_MAX_HEART_RATE', 'Lap max. heart rate'),
    new Field('TXT_LAP_MAX_PACE', 'Lap max. pace'),
    new Field('TXT_LAP_MAX_SPEED', 'Lap max. speed'),
    new Field('TXT_LAP_MIN_ALTITUDE', 'Lap min. altitude'),
    new Field('TXT_LAP_MIN_HEART_RATE', 'Lap min. heart rate'),
    new Field('TXT_LAP_NUMBER', 'Lap number'),
    new Field('TXT_LAP_TEMPERATURE', 'Lap temperature'),
    new Field('TXT_MAX_TEMPERATURE', 'Max temperature'),
    new Field('TXT_MAX_ALTITUDE', 'Max. altitude'),
    new Field('TXT_MAX_PACE', 'Max. pace'),
    new Field('TXT_MAX_SPEED', 'Max. speed'),
    new Field('TXT_MIN_ALTITUDE', 'Min. altitude'),
    new Field('TXT_NAUTICAL_DISTANCE', 'Nautical distance'),
    new Field('TXT_NAUTICAL_SPEED', 'Nautical speed'),
    new Field('TXT_PACE', 'Pace'),
    new Field('TXT_PREVIOUS_ACTIVITY_DURATION', 'Previous activity duration'),
    new Field('TXT_PTE', 'PTE'),
    new Field('TXT_RECOVERY_TIME', 'Recovery time'),
    new Field('TXT_ROWING_PACE', 'Rowing pace'),
    new Field('TXT_SPEED', 'Speed'),
    new Field('TXT_SWIM_DISTANCE', 'Swim distance'),
    new Field('TXT_SWOLF_POSTFIX', 'SWOLF'),
    new Field('TXT_TEMPERATURE', 'Temperature'),
    new Field('TXT_TIMEOFDAY', 'Time of day'),
  ]),
];

function compareDisplays(displayList1, displayList2) {
  const displayListFiltered = displayList1.filter((display, index) =>
    (display.id === displayList2[index].id &&
     display.name === displayList2[index].name &&
     display.readOnly === displayList2[index].readOnly &&
     display.deletable === displayList2[index].deletable &&
     display.replaceable === displayList2[index].replaceable));
  expect(displayListFiltered.length).to.be.eql(displayList2.length);
}

function compareFields(fieldList1, fieldList2, slack, retryLeft = true) {
  const fieldListFiltered = fieldList1.filter((fieldSection, index) => {
    const fields = fieldSection.fields.filter((field, fieldIndex) =>
      (field.id === fieldList2[index].fields[fieldIndex].id &&
       field.name === fieldList2[index].fields[fieldIndex].name));
    // Some SIM versions have options title, i.e. TXT_POWER_OPTIONS instead of TXT_POWER
    // so support both by using "includes"
    if (fieldSection.id.includes(fieldList2[index].id) &&
        fieldSection.name.includes(fieldList2[index].name) &&
        fields.length === fieldList2[index].fields.length) {
      return true;
    }
    return false;
  });
  if (fieldListFiltered.length !== fieldList2.length) {
    expect(slack).not.to.be.null;
    expect(retryLeft).to.be.true;

    // Some SIM versions have Nautical, Rowing and some other fields
    // filtered away from activity types where they make no
    // sense. Adapt tests so that they work with both
    const fieldList2filtered = fieldList2.map((section) => {
      section.fields = section.fields.filter(field => // eslint-disable-line no-param-reassign
        (!slack.nautical || !field.id.includes('NAUTICAL')) &&
        (!slack.rowingPace || !field.id.includes('TXT_ROWING_PACE')) &&
        (!slack.pace || !field.id.includes('TXT_PACE')) &&
        (!slack.altitude || !field.id.includes('ALTITUDE')));
      return section;
    }).filter(section => section.fields.length > 0);

    // Try comparing without filterer fields
    compareFields(fieldList1, fieldList2filtered, slack, false);
  }
}

describe('Packages', () => {
  describe('getSportModes', () => {
    describe('Amsterdam', () => {
      it('1.9.22', () => {
        const packages = new Packages('Amsterdam', '1.9.22', 'en');
        const sportModes = packages.sportModes();
        expect(sportModes.length).to.be.eql(33);
      });
      it('2.8.4', () => {
        const packages = new Packages('Amsterdam', '2.8.4', 'en');
        const sportModes = packages.sportModes();
        expect(sportModes.length).to.be.eql(69);
      });
    });
    describe('AmsterdamC', () => {
      it('1.9.22', () => {
        const packages = new Packages('AmsterdamC', '1.9.22', 'en');
        const sportModes = packages.sportModes();
        expect(sportModes.length).to.be.eql(33);
      });
      it('2.8.4', () => {
        const packages = new Packages('AmsterdamC', '2.8.4', 'en');
        const sportModes = packages.sportModes();
        expect(sportModes.length).to.be.eql(69);
      });
    });
    describe('HelsinkiC', () => {
      it('1.9.22', () => {
        const packages = new Packages('HelsinkiC', '1.9.22', 'en');
        const sportModes = packages.sportModes();
        expect(sportModes.length).to.be.eql(0);
      });
      it('2.2.0', () => {
        const packages = new Packages('HelsinkiC', '2.2.0', 'en');
        const sportModes = packages.sportModes();
        expect(sportModes.length).to.be.eql(70);
      });
    });
    describe('Helsinki', () => {
      it('1.9.22', () => {
        const packages = new Packages('Helsinki', '2.0.12', 'en');
        const sportModes = packages.sportModes();
        expect(sportModes.length).to.be.eql(70);
      });
      it('2.2.0', () => {
        const packages = new Packages('Helsinki', '2.2.0', 'en');
        const sportModes = packages.sportModes();
        expect(sportModes.length).to.be.eql(70);
      });
    });
    describe('Kyoto', () => {
      it('1.210.0', () => {
        if (!SupportHelpers.simSupportsKyoto()) {
          return;
        }

        const packages = new Packages('Kyoto', '1.210.0', 'en');
        const sportModes = packages.sportModes();
        expect(sportModes.length).to.be.eql(72);
      });
    });
  });
  describe('getSportModeTemplate', () => {
    it('Running', () => {
      const packages = new Packages('Amsterdam', '2.2.8', 'fi');
      // Activity Id = 3, new modeId = 5;
      const watchSportMode = packages.sportModeTemplate(3, 5);
      expect(watchSportMode.group.data.id).to.be.eql(3);
      /* eslint no-bitwise: ["error", { "allow": ["|"] }] */
      expect(watchSportMode.group.id).to.be.eql(0xC0000000 | 5);
      expect(watchSportMode.group.data.Settings.CustomModeGroups[0].CustomModeIDs[0]).to.be.eql(5);

      // Check that (some) unused values have been filtered
      expect(watchSportMode.modes[0].displays.Settings.CustomModes[0]
        .Displays[0].Name).to.be.undefined;
      expect(watchSportMode.modes[0].displays.Settings.CustomModes[0]
        .Displays[0].ShowTimeout).to.be.undefined;
      expect(watchSportMode.modes[0].displays.Settings.CustomModes[0]
        .Displays[0].Fields[0].ValueScaleMin).to.be.undefined;
    });
    it('Cycling', () => {
      const packages = new Packages('Amsterdam', '2.2.8', 'fi');
      // Activity Id = 4, new modeId = 78;
      const watchSportMode = packages.sportModeTemplate(5, 78);
      expect(watchSportMode.group.data.id).to.be.eql(5);
      expect(watchSportMode.group.id).to.be.eql(0xC0000000 | 78);
      expect(watchSportMode.group.data.Settings.CustomModeGroups[0].CustomModeIDs[0]).to.be.eql(78);
    });
    it('Transition', () => {
      const packages = new Packages('Amsterdam', '2.2.8', 'fi');
      // Activity Id = 99, new modeId = 8;
      const watchSportMode = packages.sportModeTemplate(99, 8);
      expect(watchSportMode).to.be.eql(undefined);
    });
    it('Downhill', () => {
      const packages = new Packages('Amsterdam', '2.2.8', 'fi');
      // Activity Id = 20, new modeId = 5;
      const watchSportMode = packages.sportModeTemplate(20, 5);
      expect(watchSportMode.group.data.id).to.be.eql(20);
      /* eslint no-bitwise: ["error", { "allow": ["|"] }] */
      expect(watchSportMode.group.id).to.be.eql(0xC0000000 | 5);
      expect(watchSportMode.group.data.Settings.CustomModeGroups[0].CustomModeIDs[0]).to.be.eql(5);

      // Check that all the nulls have been filtered
      const displays = watchSportMode.modes[0].displays.Settings.CustomModes[0].Displays;
      displays.forEach((disp) => {
        Object.keys(disp).forEach(key => expect(disp[key]).to.not.equal(null));

        disp.Fields.forEach((field) => {
          Object.keys(field).forEach(key => expect(field[key]).to.not.equal(null));
        });
      });
    });
    it('Chart', () => {
      if (!SupportHelpers.simSupportsKyoto()) {
        return;
      }

      const packages = new Packages('Kyoto', '1.122.0', 'en');
      // Activity Id = 33, new modeId = 0;
      const watchSportMode = packages.sportModeTemplate(33, 0);

      // Check that Chart scale values exist
      const chart = watchSportMode.modes[0].displays.Settings.CustomModes[0]
        .Displays[1].Fields[2];
      expect(chart.ID).to.be.eql(Fields.CHART);
      expect(chart.SecondDimensionScale).to.be.eql(600);
    });
    describe('Helsinki', () => {
      it('Running', () => {
        const packages = new Packages('Helsinki', '2.2.8', 'fi');
        // Activity Id = 3, new modeId = 5;
        const watchSportMode = packages.sportModeTemplate(3, 5);

        if (Packages.isCustomizationSupported('Helsinki')) {
          expect(watchSportMode.group.data.id).to.be.eql(3);
          /* eslint no-bitwise: ["error", { "allow": ["|"] }] */
          expect(watchSportMode.group.id).to.be.eql(0xC0000000 | 5);
          expect(watchSportMode.group.data.Settings.CustomModeGroups[0].CustomModeIDs[0])
            .to.be.eql(5);
          const displays = watchSportMode.modes[0].displays.Settings.CustomModes[0].Displays;

          // Check that (some) unused values have been filtered
          expect(Object.keys(displays[0]).length).to.be.eql(2);

          // Check display
          expect(displays[0].TemplateType).to.be.eql('Data3Carousel');

          // In SIM version 2.0.19 and older Helsinki templates included interval display
          if (displays.length === 2) {
            // Check that (some) unused values have been filtered
            expect(Object.keys(displays[1]).length).to.be.eql(2);
            expect(displays[1].TemplateType).to.be.eql('Interval37');
          } else {
            expect(displays.length).to.be.eql(1);
          }
        } else {
          expect(watchSportMode).to.be.eql(undefined);
        }
      });
      it('Cycling', () => {
        const packages = new Packages('Helsinki', '2.2.8', 'fi');
        // Activity Id = 4, new modeId = 78;
        const watchSportMode = packages.sportModeTemplate(5, 78);

        if (Packages.isCustomizationSupported('Helsinki')) {
          expect(watchSportMode.group.data.id).to.be.eql(5);
          expect(watchSportMode.group.id).to.be.eql(0xC0000000 | 78);
          expect(watchSportMode.group.data.Settings.CustomModeGroups[0].CustomModeIDs[0])
            .to.be.eql(78);

          // Check displays
          const displays = watchSportMode.modes[0].displays.Settings.CustomModes[0].Displays;
          expect(displays[0].TemplateType).to.be.eql('Data3Carousel');

          // In SIM version 2.0.19 and older Helsinki templates included interval display
          if (displays.length === 2) {
            // Check that (some) unused values have been filtered
            expect(displays[1].TemplateType).to.be.eql('Interval37');
          } else {
            expect(displays.length).to.be.eql(1);
          }
        } else {
          expect(watchSportMode).to.be.eql(undefined);
        }
      });
      it('PoolSwimming', () => {
        const packages = new Packages('Helsinki', '2.2.8', 'fi');
        // Activity Id = 6, new modeId = 79;
        const watchSportMode = packages.sportModeTemplate(6, 79);

        if (Packages.isCustomizationSupported('Helsinki')) {
          expect(watchSportMode.group.data.id).to.be.eql(6);
          expect(watchSportMode.group.id).to.be.eql(0xC0000000 | 79);
          expect(watchSportMode.group.data.Settings.CustomModeGroups[0].CustomModeIDs[0])
            .to.be.eql(79);
        } else {
          expect(watchSportMode).to.be.eql(undefined);
        }
      });
      it('OpenWaterSwimming', () => {
        const packages = new Packages('Helsinki', '2.2.8', 'fi');
        // Activity Id = 83, new modeId = 80;
        const watchSportMode = packages.sportModeTemplate(83, 80);

        if (Packages.isCustomizationSupported('Helsinki')) {
          expect(watchSportMode.group.data.id).to.be.eql(83);
          expect(watchSportMode.group.id).to.be.eql(0xC0000000 | 80);
          expect(watchSportMode.group.data.Settings.CustomModeGroups[0].CustomModeIDs[0])
            .to.be.eql(80);
        } else {
          expect(watchSportMode).to.be.eql(undefined);
        }
      });
      it('Sailing', () => {
        const packages = new Packages('Helsinki', '2.2.8', 'fi');
        // Activity Id = 13, new modeId = 81;
        const watchSportMode = packages.sportModeTemplate(13, 81);

        if (Packages.isCustomizationSupported('Helsinki')) {
          expect(watchSportMode.group.data.id).to.be.eql(13);
          expect(watchSportMode.group.id).to.be.eql(0xC0000000 | 81);
          expect(watchSportMode.group.data.Settings.CustomModeGroups[0].CustomModeIDs[0])
            .to.be.eql(81);
        } else {
          expect(watchSportMode).to.be.eql(undefined);
        }
      });
      it('Snowboarding', () => {
        const packages = new Packages('Helsinki', '2.2.8', 'fi');
        // Activity Id = 21, new modeId = 82;
        const watchSportMode = packages.sportModeTemplate(21, 82);

        if (Packages.isCustomizationSupported('Helsinki')) {
          expect(watchSportMode.group.data.id).to.be.eql(21);
          expect(watchSportMode.group.id).to.be.eql(0xC0000000 | 82);
          expect(watchSportMode.group.data.Settings.CustomModeGroups[0].CustomModeIDs[0])
            .to.be.eql(82);
        } else {
          expect(watchSportMode).to.be.eql(undefined);
        }
      });
      it('Hiking', () => {
        const packages = new Packages('Helsinki', '2.2.8', 'fi');
        // Activity Id = 96, new modeId = 83;
        const watchSportMode = packages.sportModeTemplate(96, 83);

        if (Packages.isCustomizationSupported('Helsinki')) {
          expect(watchSportMode.group.data.id).to.be.eql(96);
          expect(watchSportMode.group.id).to.be.eql(0xC0000000 | 83);
          expect(watchSportMode.group.data.Settings.CustomModeGroups[0].CustomModeIDs[0])
            .to.be.eql(83);
        } else {
          expect(watchSportMode).to.be.eql(undefined);
        }
      });
    });
  });
  describe('displayList', () => {
    it('all', () => {
      const packages = new Packages('Amsterdam', '1.9.0', 'en');
      const currentDisplays = [new Display('Data3Fields'), new Display('TXT_SPEED')];
      const displaySections = packages.displayList(3, currentDisplays, 0);

      expect(displaySections.length).to.be.eql(3);

      compareDisplays(displaySections[0].displays, fieldsDisplays);
      expect(displaySections[0].id).to.be.eql('Fields');
      expect(displaySections[0].name).to.be.eql('Fields');
      expect(displaySections[0].description).to.be.null;

      compareDisplays(displaySections[1].displays, columnsDisplays);
      expect(displaySections[1].id).to.be.eql('Columns');
      expect(displaySections[1].name).to.be.eql('Columns');
      expect(displaySections[1].description).to.be.null;

      compareDisplays(displaySections[2].displays, intervalDisplays);
      expect(displaySections[2].id).to.be.eql('Intervals');
      expect(displaySections[2].name).to.be.eql('Interval');
      expect(displaySections[2].description.length).to.be.eql(153);

      const swimDisplaySections = packages.displayList(6, currentDisplays, 0);
      expect(swimDisplaySections.length).to.be.eql(2);

      compareDisplays(swimDisplaySections[0].displays, fieldsDisplays);
      compareDisplays(swimDisplaySections[1].displays, columnsDisplays);
    });
    it('empty list', () => {
      const packages = new Packages('Amsterdam', '1.9.0', 'en');
      const displaySections = packages.displayList(3, [], 0);

      expect(displaySections.length).to.be.eql(2);

      compareDisplays(displaySections[0].displays, fieldsDisplays);
      expect(displaySections[0].id).to.be.eql('Fields');
      expect(displaySections[0].name).to.be.eql('Fields');
      expect(displaySections[0].description).to.be.null;

      compareDisplays(displaySections[1].displays, columnsDisplays);
      expect(displaySections[1].id).to.be.eql('Columns');
      expect(displaySections[1].name).to.be.eql('Columns');
      expect(displaySections[1].description).to.be.null;

      // No interval displays as it can't be the only display. Empty list is not
      // a normal scenario but e.g. possible with some unexpected bug in app
    });
    if (Packages.isCustomizationSupported('Helsinki')) {
      it('Helsinki', () => {
        const packages = new Packages('Helsinki', '2.2.0', 'en');
        const currentDisplays = [];
        const display = new Display('Data3Fields');
        currentDisplays.push(display);
        const displaySections = packages.displayList(3, currentDisplays, 1);
        expect(displaySections.length).to.be.eql(3);

        compareDisplays(displaySections[0].displays, fieldsDisplaysHelsinki);
        expect(displaySections[0].id).to.be.eql('Fields');
        expect(displaySections[0].name).to.be.eql('Fields');
        expect(displaySections[0].description).to.be.null;

        compareDisplays(displaySections[1].displays, columnsDisplays);
        expect(displaySections[1].id).to.be.eql('Columns');
        expect(displaySections[1].name).to.be.eql('Columns');
        expect(displaySections[1].description).to.be.null;

        compareDisplays(displaySections[2].displays, intervalDisplaysHelsinki);
        expect(displaySections[2].id).to.be.eql('Intervals');
        expect(displaySections[2].name).to.be.eql('Interval');
        expect(displaySections[2].description.length).to.be.eql(153);

        const swimDisplaySections = packages.displayList(6, currentDisplays, 0);
        expect(swimDisplaySections.length).to.be.eql(2);
        compareDisplays(swimDisplaySections[0].displays, fieldsDisplaysHelsinki);
        compareDisplays(swimDisplaySections[1].displays, columnsDisplays);
      });
    }
    if (Packages.isCustomizationSupported('Helsinki')) {
      it('maxInstances', () => {
        const packages = new Packages('Helsinki', '2.1.0', 'en');
        const currentDisplays = [];
        const display = new Display('Data3Fields');
        currentDisplays.push(display);
        const carouselDisplay = new Display('Data3Carousel');
        currentDisplays.push(carouselDisplay);

        const displaySectionsCarousel = packages.displayList(3, currentDisplays, 1);

        expect(displaySectionsCarousel.length).to.be.eql(3);
        compareDisplays(displaySectionsCarousel[0].displays, fieldsDisplaysHelsinki);
        expect(displaySectionsCarousel[0].id).to.be.eql('Fields');
        expect(displaySectionsCarousel[0].name).to.be.eql('Fields');
        expect(displaySectionsCarousel[0].description).to.be.null;

        const displaySections = packages.displayList(3, currentDisplays, 0);
        expect(displaySections.length).to.be.eql(3);
        compareDisplays(displaySections[0].displays, fieldsDisplaysHelsinkiWoCarousel);
        expect(displaySections[0].id).to.be.eql('Fields');
        expect(displaySections[0].name).to.be.eql('Fields');
        expect(displaySections[0].description).to.be.null;
      });
    }
    it('interval', () => {
      const packages = new Packages('Amsterdam', '1.9.0', 'en');
      const currentDisplays = [];
      const intervalDisplay = new Display('Interval77');
      const display = new Display('Data3Fields');
      currentDisplays.push(intervalDisplay);
      currentDisplays.push(display);
      // Get possible displays for second display -> interval displays should not be available
      const displaySections = packages.displayList(3, currentDisplays, 1);

      expect(displaySections.length).to.be.eql(2);
      compareDisplays(displaySections[0].displays, fieldsDisplays);
      compareDisplays(displaySections[1].displays, columnsDisplays);

      // Get possible displays for first display -> interval displays should be available
      const displaySections2 = packages.displayList(3, currentDisplays, 0);

      expect(displaySections2.length).to.be.eql(3);
      compareDisplays(displaySections2[0].displays, fieldsDisplays);
      compareDisplays(displaySections2[1].displays, columnsDisplays);
      compareDisplays(displaySections2[2].displays, intervalDisplays);
    });
    it('not supported', () => {
      const packages = new Packages('Amsterdam', '1.2.9', 'fi');
      const currentDisplays = [];
      const display = new Display('Data3Fields');
      currentDisplays.push(display);
      const displaySections = packages.displayList(3, currentDisplays, 0);
      expect(displaySections.length).to.be.eql(0);
      const packages2 = new Packages('Amsterdam', '0.9.9', 'fi');
      const displaySections2 = packages2.displayList(3, currentDisplays, 0);
      expect(displaySections2.length).to.be.eql(0);
      const packages3 = new Packages('Amsterdam', '1.6', 'fi');
      const displaySections3 = packages3.displayList(3, currentDisplays, 0);
      expect(displaySections3.length).to.be.eql(0);
    });
  });
  describe('getFields', () => {
    it('Amsterdam', () => {
      const packages = new Packages('Amsterdam', '1.9.0', 'en');
      const fieldsections = packages.fields(3, 'Data3Fields');
      if (!Packages.isFieldSectionsSupported()) {
        // SIM version 2.0.15 and older only contains one section for fields
        compareFields(fieldsections, fieldsForRunningSingleSection);
      } else {
        const slack = {
          nautical: true,
          rowingPace: true,
        };
        compareFields(fieldsections, fieldsForRunning, slack);
      }
      // Available fields for interval display
      expect(packages.fields(3, 'TXT_POWER', 0).length).to.be.eql(0);

      // Available fields for swimming
      const swimmingFieldSections = packages.fields(6, 'Data3Fields', 0);
      if (!Packages.isFieldSectionsSupported()) {
        // SIM version 2.0.15 and older only contains one section for fields
        compareFields(swimmingFieldSections, fieldsForSwimmingSingleSection);
      } else {
        const slack = {
          nautical: true,
          rowingPace: true,
          pace: true,
          altitude: true,
        };
        compareFields(swimmingFieldSections, fieldsForSwimming, slack);
      }
    });
    describe('Kyoto', () => {
      it('Running', () => {
        if (!SupportHelpers.simSupportsKyoto()) {
          return;
        }

        const packages = new Packages('Kyoto', '1.122.0', 'en');
        const fieldsections = packages.fields(3, 'LapTableWearRound', 0);

        // 'LapTableWearRound' has new concept of "AvailableFieldIds" which
        // can limit the available options per field location
        expect(fieldsections.length).to.be.eql(2);
        expect(fieldsections[0].id).to.include('TXT_DISTANCE');
        expect(fieldsections[0].fields.length).to.be.eql(1);
        expect(fieldsections[0].fields[0].id).to.be.eql('TXT_LAP_DISTANCE');
        expect(fieldsections[1].id).to.include('TXT_DURATION_AND_TIME');
        expect(fieldsections[1].fields.length).to.be.eql(1);
        expect(fieldsections[1].fields[0].id).to.be.eql('TXT_LAP_DURATION');
      });
      it('PoolSwimming', () => {
        if (!SupportHelpers.simSupportsKyoto()) {
          return;
        }

        const packages = new Packages('Kyoto', '1.122.0', 'en');
        const fieldsections = packages.fields(6, 'LapTableWearRound', 0);

        // 'LapTableWearRound' has new concept of "AvailableFieldIds" which
        // can limit the available options per field location
        expect(fieldsections.length).to.be.eql(1);
        expect(fieldsections[0].id).to.contains('TXT_DISTANCE');
        expect(fieldsections[0].fields.length).to.be.eql(1);
        expect(fieldsections[0].fields[0].id).to.be.eql('TXT_INTERVAL_SWIMDISTANCE');
      });
    });
  });
  describe('intervalDisplay', () => {
    describe('version filtering', () => {
      it('< 1.9.22', () => {
        const packages = new Packages('Amsterdam', '1.9.0', 'en');
        const display = packages.intervalDisplay('TXT_SPEED');
        expect(display.TemplateType).to.be.eql('Interval77');
        expect(display.Fields[6].ID).to.be.eql(Fields.BOTTOM);
        expect(display.Fields[6].ValueResource).to.be.eql('Duration');
      });
      it('>= 1.9.22', () => {
        const packages = new Packages('Amsterdam', '2.0.0', 'en');
        const display = packages.intervalDisplay('TXT_SPEED');
        expect(display.TemplateType).to.be.eql('Interval77');
        expect(display.Fields[6].ID).to.be.eql(Fields.BOTTOM);
        expect(display.Fields[6].PathRoot).to.be.eql('Activity/Exercise/Interval/CountDown/Duration');
      });
      if (Packages.isCustomizationSupported('Helsinki')) {
        it('Helsinki', () => {
          const packages = new Packages('Helsinki', '2.0.12', 'en');
          const display = packages.intervalDisplay('TXT_SPEED');
          expect(display.TemplateType).to.be.eql('Interval77');
          expect(display.Fields[6].ID).to.be.eql(Fields.BOTTOM);
          expect(display.Fields[6].PathRoot).to.be.eql('Activity/Exercise/Interval/CountDown/Duration');
        });
      }
    });
  });
  describe('Number of displays', () => {
    it('Min', () => {
      const packages = new Packages('Amsterdam', '1.0', 'fi');
      expect(packages.minNumberOfDisplays()).to.be.eql(1);
    });
    it('Max', () => {
      const packages = new Packages('Amsterdam', '1.0', 'fi');
      expect(packages.maxNumberOfDisplays()).to.be.eql(3);
    });
  });
  it('versionCompare', () => {
    expect(Packages.versionCompare('1.0.0', '1.0.0')).to.be.true;
    expect(Packages.versionCompare('1.1.0', '1.0.0')).to.be.true;
    expect(Packages.versionCompare('1.0.1', '1.0.0')).to.be.true;

    expect(Packages.versionCompare('2.0.0', '1.10.0')).to.be.true;
    expect(Packages.versionCompare('1.1.0', '1.0.10')).to.be.true;

    expect(Packages.versionCompare('1.0.0', '1.1.0')).to.be.false;
    expect(Packages.versionCompare('1.0.0', '1.0.1')).to.be.false;
    expect(Packages.versionCompare('1.0.9', '1.1.0')).to.be.false;
  });
});
