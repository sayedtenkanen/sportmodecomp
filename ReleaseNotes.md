# Sport Mode Component release notes

## 1.4.0
Features:
  * Suunto Dolphin ("Nagano") support

## 1.3.6
Features:
  * Fields from Lap Table are no longer merged to another display

## 1.3.5
Bugfixes:
  * Fix zh-Hans OPTIONS translations
  * Fix zh-Hans TXT_SEA_LEVEL_PRESSURE

## 1.3.4
Improvements:
  * Separate translations for field options

## 1.3.3
Improvements:
  * Proper map display handling for Wear OS devices [98251]

## 1.3.2
Bugfixes:
  * Custom Mode IDs can collide with factory modes [98372]

Improvements:
  * Unittest adaptation for changes in salmon-master

## 1.3.1
Improvements:
  * Add translations for "2 Fields" and "6 Fields" displays for Suunto 7 [98161]
  * Unittest adaptation for changes in salmon-master

## 1.3.0
Features:
  * Suunto 7 ("Kyoto") support

## 1.2.0
Features:
  * Suunto 3 ("Oulu") support

## 1.1.0
Features:
  * Add time-of-day to 4, 5 and 7 field display pictures [91213]

Improvements:
  * Grey out time-of-day and duration in carousel display placeholder picture [85796]

## 1.0.7
Bugfixes:
  * Carousel screen is not showing Time of Day TP[88326]
  * Add missing TXT_INDOOR_WITH_POWER and TXT_RACE_WITH_POW_MODE

Technical:
  * Unittest adaptation to latest SIM

## 1.0.6
Improvements:
 * Add new translations

## 1.0.5
Improvements:
 * Add TXT_MAX_HR for Suunto 5 in-box release [86103]
 * Updated LapTable images [85341]

Notes:
 * Some new phrases are pending translations and are marked with "(placeholder)" text

## 1.0.4
Features:
 * Suunto 5 support [85763]
 * Turkish and Thai support [86080]

Notes:
 * Some new phrases are pending translations and are marked with "(placeholder)" text

## 1.0.3
Bugfixes:
 * Missing TXT_AIR_MODE translation [80560]

Improvements:
 * Improved display images

## 1.0.2
Bugfixes:
 * Handle missing Navigation display in getMaxNumberOfDisplays and getMinNumberOfDisplays

## 1.0.1
Bugfixes:
 * Navigation display is shown for Suunto 3 Fitness [79717]

## 1.0.0
Features:
 * Suunto 3 Fitness support [77831, 77894]

Improvements:
 * Add missing TXT_INTERVAL_DISPLAY_DESCRIPTION_SHORT [77611]

Bugfixes:
 * Wrong interval display template selected [78839]

Notes:
 * No multisport support until further notice

## 0.10.0
Features:
 * Graph display support [78382]
 * Zone display support [78225]
 * Lima variant

Improvements:
 * Sort display arrays alphabetically in getDisplay [78389]
 * Add missing display group translations [78180]
 * Add "replaceable" field to Display object [78620]

Bugfixes:
 * Disallow removing last non-interval display or replacing it with interval display [78202]
 * Adding new interval display [78183]
 * Remove duplicated field [77956]

Notes:
 * 0.9.0 notes apply

## 0.9.0
Initial release

Notes:
 * Missing translation TXT_INTERVAL_DISPLAY_DESCRIPTION_SHORT
 * No multisport support
 * No Suunto 3 Fitness support
