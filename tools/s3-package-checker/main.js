/* eslint-disable camelcase, no-console, prefer-template */

const https = require('https');

const testServer = 'https://test-sportmode.sports-tracker.com';
const prodServer = 'https://sportmode.sports-tracker.com';
const chinaProdServer = 'https://sportmodes.suunto.cn';

/*
2.0.42 Spartans              5b16e39174d7f2064d7f93d96cadf6f0c36bebd3
2.1.54 S9 Baro RID           4063984b51a545ab11f0accd1e6a28cfde06c2c7
2.1.64 S9 Baro August 2018   4063984b51a545ab11f0accd1e6a28cfde06c2c7
2.2.18 S3 June update        a513a261c53dd94091286010886a6106bad0d100
2.3.22 S3 August 2018        1b69e1fa7035be01d3ac1aaafec33c8d7f71b048
2.3.22 S9 Inbox              1b69e1fa7035be01d3ac1aaafec33c8d7f71b048
2.4.X  S9 RID                TBA
*/

const spartans2_0_42 = '5b16e39174d7.json';
const s9barorid_2_1_54 = '4063984b51a5.json';
const s3june_2_2_18 = 'a513a261c53d.json';
const s3august_2_3_18 = '1b69e1fa7035.json';
const develop_2_4_8 = 'fc55229a58c5.json';

const servers = [testServer, prodServer, chinaProdServer];
const releasePackages =
    [spartans2_0_42, s9barorid_2_1_54, s3june_2_2_18, s3august_2_3_18, develop_2_4_8];

function check(server, releasePackage) {
  https.get(server + '/' + releasePackage, (resp) => {
    let data = '';

    // A chunk of data has been received.
    resp.on('data', (chunk) => {
      data += chunk;
    });

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      console.log('Server: ' + server + ' Package: ' + releasePackage);
      console.log(data);
    });
  }).on('error', (err) => {
    console.log('Error: ' + err.message);
  });
}

servers.forEach((server) => {
  releasePackages.forEach((releasePackage) => {
    check(server, releasePackage);
  });
});
