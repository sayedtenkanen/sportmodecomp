const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const config = {
  context: `${__dirname}/src`,
  entry: './index.js',
  output: {
    path: `${__dirname}/dist`,
    filename: 'bundle.js',
    library: '[name]',
    libraryTarget: 'var',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [{
          loader: 'babel-loader',
          options: { presets: ['env'] },
        }],
      },
    ],
  },
  performance: {
    hints: false
  },
  plugins: [
    new UglifyJsPlugin(),
  ],
};
module.exports = config;
