import { exec } from 'child_process';
import fs from 'fs';

const args = process.argv.slice(2);
const outputFile = args[0];

if (!outputFile) {
  process.exit(1);
}

const hashLength = 12;

exec(`cd ../../suunto-information-model && git rev-parse --verify HEAD --short=${hashLength}`, (error, stdout, stderr) => {
  if (error) {
    process.exit(2);
  } else if (stderr) {
    process.exit(3);
  }

  const hash = stdout.trim();

  if (!hash || hash.length !== hashLength) {
    process.exit(3);
  }

  fs.writeFile(outputFile, JSON.stringify({ hash }), (writeError) => {
    if (writeError) {
      process.exit(4);
    }
  });
});
