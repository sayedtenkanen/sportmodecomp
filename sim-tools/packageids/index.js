/* eslint no-bitwise: ["error", { "allow": ["&"] }] */
import fs from 'fs';

const simPath = '../../suunto-information-model/Specifications/CustomModes/CustomModePackages';

const collectActivityIds = (outputFile, packageIds) => {
  fs.readdir(simPath, (error, files) => {
    function readId(file) {
      return new Promise((resolve, reject) => {
        fs.readFile(`${simPath}/${file}`, 'utf8', (fileError, content) => {
          if (fileError) {
            reject(fileError);
          } else {
            const obj = JSON.parse(content);
            const nameId = obj.CustomModeGroups[0].NameID; // i.e. 'TXT_BASIC_MODE'
            const customModePackageId = obj.CustomModeGroups[0].CustomModePackageID & 0xFFFF;
            const fitnessConflict = 56800;
            if (packageIds.indexOf(customModePackageId) >= 0) {
              if (customModePackageId !== fitnessConflict) {
                reject(new Error(`Same ID defined twice ${customModePackageId}`));
              } else {
                // ObstacleRacingFitness.json and HandballFitness.json
                // already have a known conflict, must filter that away
                resolve(null);
              }
            } else {
              packageIds.push(customModePackageId);
              resolve(nameId && customModePackageId ?
                {
                  nameId,
                  customModePackageId,
                } :
                null);
            }
          }
        });
      });
    }

    // There are two types files, i.e. "AerobicBasic.json" and
    // "AerobicBasicPackage.json". Filter latter out since former
    // has all the required information.
    const promises = files
      .filter(file => !file.includes('Package'))
      .map(file => readId(file));

    Promise.all(promises).then((values) => {
      const filteredValues = values.filter(value => value != null);
      fs.writeFile(outputFile, JSON.stringify(filteredValues), (writeError) => {
        if (writeError) {
          process.exit(2);
        }
      });
    }).catch(() => {
      process.exit(3);
    });
  });
};

const args = process.argv.slice(2);
const outputFile = args[0];

if (!outputFile) {
  process.exit(1);
}

const packageIds = [];

collectActivityIds(outputFile, packageIds);
