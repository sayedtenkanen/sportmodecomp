import fs from 'fs';

const simPath = '../../suunto-information-model/Specifications/CustomModes/CustomModePackages';

const collectActivityIds = (outputFile) => {
  fs.readdir(simPath, (error, files) => {
    function readId(file) {
      return new Promise((resolve, reject) => {
        fs.readFile(`${simPath}/${file}`, 'utf8', (fileError, content) => {
          if (fileError) {
            reject(fileError);
          } else {
            const obj = JSON.parse(content);
            const compatibility = obj.Compatibility;
            const activityId = obj.CustomModeGroups[0].ActivityID;
            resolve(compatibility && activityId ?
              {
                activityId,
                compatibility,
              } :
              null);
          }
        });
      });
    }

    // There are two types files, i.e. "AerobicBasic.json" and
    // "AerobicBasicPackage.json". Filter latter out since former
    // has all the required information.
    const promises = files
      .filter(file => !file.includes('Package'))
      .map(file => readId(file));

    Promise.all(promises).then((values) => {
      fs.writeFile(outputFile, JSON.stringify(values), (writeError) => {
        if (writeError) {
          process.exit(2);
        }
      });
    });
  });
};

const args = process.argv.slice(2);
const outputFile = args[0];

if (!outputFile) {
  process.exit(1);
}

collectActivityIds(outputFile);
